(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ocp_js.Html
open Js_utils
open Bootstrap_helpers.Menu
open Data_types
open StringCompat


let txt s = txt (Lang.s_ s)
let link2 ?(disabled=false) classes lnk s  =
  Link2(classes, Common.a_link lnk, txt s, disabled)
let x__________x = Separator []


type dropdown = {
  dd_score : int ;
  dd_name : string ;
  mutable dd_items : dropdown_item list ;
}

and dropdown_item = {
  ddi_score : int ;
  ddi_disabled : bool ;
  ddi_link : string ;
  ddi_name : string ;
}

let menu_encoding =
  let open Json_encoding in
  let item_encoding =
    conv
      (fun { ddi_score ; ddi_link ; ddi_name ; ddi_disabled } ->
         (ddi_score, ddi_link, ddi_name, ddi_disabled))
      (fun (ddi_score, ddi_link, ddi_name, ddi_disabled) ->
         { ddi_score ; ddi_link ; ddi_name ; ddi_disabled } )
      (obj4
         (dft "score" int 10)
         (req "link" string)
         (req "title" string)
         (dft "disabled" bool false)) in
  let menu_encoding =
    let entry =
      conv
        (function { dd_score ; dd_name ; dd_items } ->
           (dd_score, dd_name, dd_items) )
        (fun (dd_score, dd_name, dd_items) -> { dd_score ; dd_name ; dd_items } )
        (obj3
           (dft "score" int 10)
           (req "menu" string)
           (req "submenu" (list item_encoding))) in
    (list entry) in
  (obj1 (req "top-menu" menu_encoding))

let dd_compare dd1 dd2 = compare dd1.dd_score dd2.dd_score
let ddi_compare ddi1 ddi2 = compare ddi1.ddi_score ddi2.ddi_score

let add_item dd ?(score=10) ?(disabled=false) ddi_link ddi_name =
  let ddi = {
    ddi_score = score;
    ddi_name ;
    ddi_link ;
    ddi_disabled = disabled ;
  } in
  dd.dd_items <- dd.dd_items @ [ddi]

let create menu_list _services =
  let menu_list = List.stable_sort dd_compare menu_list in
  (* compare by score *)
  if
    try
      match StringMap.find "Timings" Common.options with
      | "true" | "y" | "Y" | "yes" -> true
      | _ -> false
    with _ -> false
  then begin
    let menu_charts =
      List.find (fun d -> d.dd_name = "Charts") menu_list in
    add_item menu_charts  "/timingsmins"
      "API Server Stats (last minutes)";
    add_item menu_charts "/timingshours"
      "API Server Stats (last hours)";
    add_item menu_charts "/timingsdays"
      "API Server Stats (last days)";
  end ;

  let current_lang = match Jslang.get() with
    | None -> "en"
    | Some lang -> lang
  in
  log "current_lang = %S" current_lang;
  let menu =
    [
      link2 ["active"] "/" "Home";
    ] @
    List.map (fun dd ->
        Dropdown ([], [txt dd.dd_name],
                  List.map (fun ddi ->
                      if ddi.ddi_name = "" then
                        x__________x
                      else
                      if ddi.ddi_disabled then
                        link2
                          ~disabled:true
                          ["disabled"]
                          ddi.ddi_link
                          ddi.ddi_name
                      else
                        link2 []
                          ddi.ddi_link
                          ddi.ddi_name
                    ) (List.stable_sort ddi_compare dd.dd_items),
                  false)
      ) menu_list
    @
    [
      Dropdown([],  [span ~a:[ a_class ["glyphicon"; "glyphicon-cog"] ] []; txt " "],
               ((Header ([], "Language")) ::
                (List.map (fun (name, lang) ->
                     let args = List.filter (fun (v,_) -> v <> "lang") (Jsloc.args ()) in
                     let args = ("lang",lang):: args in
                     Link([], Common.link ~args (Jsloc.path_string ()),
                          txt name, lang = current_lang)
                   ) Infos.www.www_languages)) @
               [ x__________x ] @
               ((Header ([], "Theme")) ::
                (List.map (fun (name, theme) ->
                     Action([], (fun () ->
                         Common.change_theme theme;
                         Cookie.set "theme" theme),
                          txt name))
                    Infos.www.www_themes)) @
               [ x__________x ] @
               ((Generic ([], [ span ~a:[ a_id "login-button-div"] [ ] ] )) ::
                [ ]),
               false
              )
    ]
  in

  match Manip.by_id "top-menu" with
  | None -> ()
  | Some menu_ul ->
    let menu = List.map bootstrap_menu menu in
    Manip.replaceChildren menu_ul menu;
    ()

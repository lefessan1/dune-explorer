//Provides: ml_z_abs const
//Requires: bigInt
function ml_z_abs(z1) {
  return bigInt(z1).abs();
}

//Provides: ml_z_neg const
//Requires: bigInt
function ml_z_neg(z1) {
  return bigInt(z1).negate();
}

//Provides: ml_z_add const
//Requires: bigInt
function ml_z_add(z1, z2) {
  return bigInt(z1).add(bigInt(z2));
}

//Provides: ml_z_div const
//Requires: bigInt
function ml_z_div(z1, z2) {
  return bigInt(z1).divide(bigInt(z2)); // FIXME
}

//Provides: ml_z_rem const
//Requires: bigInt
function ml_z_rem(z1, z2) {
  return bigInt(z1).mod(bigInt(z2)); // FIXME
}

//Provides: ml_z_div_rem const
//Requires: bigInt, ml_z_div, ml_z_rem, caml_obj_block, caml_array_set
function ml_z_div_rem(z1, z2) {
  var div = ml_z_div(z1, z2);
  var rem = ml_z_rem(z1, z2);
  var res = caml_obj_block(0, 2);
  caml_array_set(res, 0, div);
  caml_array_set(res, 1, rem);
  return res;
}

//Provides: ml_z_equal const
//Requires: bigInt
function ml_z_equal(z1, z2) {
  return bigInt(z1).equals(bigInt(z2));
}

//Provides: ml_z_format const
//Requires: bigInt
//Requires: caml_to_js_string, caml_js_to_string, caml_failwith
function ml_z_format(fmt, z1) {
  fmt = caml_to_js_string(fmt);
  if (fmt == "%d") {
    return caml_js_to_string(bigInt(z1).toString());
  } else {
    caml_failwith("Unsupported format '" + fmt + "'");
  }
}

//Provides: ml_z_init const
function ml_z_init() {
}

//Provides: ml_z_install_frametable const
function ml_z_install_frametable() {
}

//Provides: ml_z_mul const
//Requires: bigInt
function ml_z_mul(z1, z2) {
  return bigInt(z1).multiply(bigInt(z2));
}

//Provides: ml_z_of_int const
//Requires: bigInt
function ml_z_of_int(i) {
  return bigInt(i);
}

//Provides: ml_z_of_int64 const
//Requires: bigInt, caml_int64_is_negative, caml_int64_neg, ml_z_neg
function ml_z_of_int64(i64) {
  if (caml_int64_is_negative(i64)) {
    i64 = caml_int64_neg(i64);
    return ml_z_neg(bigInt(i64[3]).shiftLeft(48).add(bigInt(i64[2]).shiftLeft(24)).add(bigInt(i64[1])));
  }
  else return bigInt(i64[3]).shiftLeft(48).add(bigInt(i64[2]).shiftLeft(24)).add(bigInt(i64[1]));
}

//Provides: ml_z_of_js_string_base const
//Requires: bigInt, caml_to_js_string, caml_failwith
function ml_z_of_js_string_base(base, s) {
  if (base == 0) { // https://github.com/ocaml/Zarith/blob/b8dbaf48a7927061df699ad7ce642bb4f1fe5308/caml_z.c#L600
    base = 10;

    if (s[0] == '0') {
      if (s.length == 1) {
        return bigInt(0);
      } else {
        var bc = s[1];
        if (bc == 'o' || bc == 'O') {
          base = 8;
        } else if (bc == 'x' || bc == 'X') {
          base = 16;
        } else if (bc == 'b' || bc == 'B') {
          base = 2;
        } else {
          caml_failwith("Z.of_substring_base: invalid digit");
        }

        s = s.substring(2);
      }
    }
  }
  return bigInt(s, base);
}

//Provides: ml_z_of_substring_base const
//Requires: caml_to_js_string, ml_z_of_js_string_base
function ml_z_of_substring_base(base, s, pos, len) {
  return ml_z_of_js_string_base(base, caml_to_js_string(s).substring(pos, pos + len));
}

//Provides: ml_z_shift_left const
//Requires: bigInt
function ml_z_shift_left(z1, amt) {
  return bigInt(z1).shiftLeft(amt);
}

//Provides: ml_z_shift_right const
//Requires: bigInt
function ml_z_shift_right(z1, amt) {
  return bigInt(z1).shiftRight(amt);
}

//Provides: ml_z_sub const
//Requires: bigInt
function ml_z_sub(z1, z2) {
  return bigInt(z1).subtract(bigInt(z2));
}

//Provides: ml_z_succ const
//Requires: bigInt
function ml_z_succ(z1) {
  return bigInt(z1).next();
}

// Requires: bigInt, caml_new_string
// Provides: ml_z_to_bits
function ml_z_to_bits(z) {
  var zbase = bigInt(255);
  var res = "";
  var big = bigInt(z);
  while (!big.isZero()) {
    res += String.fromCharCode(big.and(zbase));
    big = big.shiftRight(8);
  }
  return caml_new_string(res);
}

//Provides: ml_z_to_int const
//Requires: bigInt
function ml_z_to_int(z1) {
  return bigInt(z1).toJSNumber();
}

//Provides: ml_z_logand const
//Requires: bigInt
function ml_z_logand(z1, z2) {
  return bigInt(z1).and(bigInt(z2));
}

//Provides: ml_z_logor const
//Requires: bigInt
function ml_z_logor(z1, z2) {
  return bigInt(z1).or(bigInt(z2));
}

//Provides: ml_z_logxor const
//Requires: bigInt
function ml_z_logxor(z1, z2) {
  return bigInt(z1).xor(bigInt(z2));
}

//Provides: ml_z_lognot const
//Requires: bigInt
function ml_z_lognot(z1) {
  return bigInt(z1).lognot();
}


//Provides: ml_z_to_int64 const
//Requires: bigInt, ml_z_logand, ml_z_shift_right
function ml_z_to_int64(z) {
  return [255, ml_z_logand(bigInt(z), bigInt(16777215)).toJSNumber(), ml_z_logand(ml_z_shift_right(bigInt(z), 24), bigInt(16777215)).toJSNumber(), ml_z_logand(ml_z_shift_right(bigInt(z), 48), bigInt(65535)).toJsNumber()];
}

//Provides: ml_z_numbits const
//Requires: bigInt
function ml_z_numbits(z1) {
  z1 = bigInt(z1).abs();
  var n = 0;
  var upperBound = bigInt.one;
  while (upperBound.leq(z1)) {
    n += 1;
    upperBound = upperBound.multiply(2);
  }
  return n; // 2^{n-1} <= |x| < 2^n
}

// Requires: bigInt
// Provides: ml_z_of_bits
function ml_z_of_bits(arg) {
  var zbase = bigInt(256);
  var acc = bigInt(0);
  var pow = bigInt(1);
  for(var i=0; i<arg.l; i++) {
    var base_bump = bigInt(arg.c.charCodeAt(i));
    var bump = base_bump.multiply(pow);
    acc = acc.add(bump);
    pow = pow.multiply(zbase);
  }
  return acc;
}

//Provides: ml_z_pow const
//Requires: bigInt
function ml_z_pow(z1, i1) {
  return bigInt(z1).pow(bigInt(i1));
}

//Provides: ml_z_pred const
//Requires: bigInt
function ml_z_pred(z1) {
  return bigInt(z1).prev();
}

//Provides: ml_z_compare const
//Requires: bigInt
function ml_z_compare(z1, z2) {
  return bigInt(z1).compare(bigInt(z2));
}

//Provides: ml_z_sign const
//Requires: bigInt
function ml_z_sign(z1) {
  return bigInt(z1).compare(bigInt.zero);
}

//Provides: ml_z_gcd const
//Requires: bigInt
function ml_z_gcd(z1, z2) {
  return bigInt.gcd(z1, z2);
}

//Provides: ml_z_powm const
//Requires: bigInt
function ml_z_powm(z1, z2, z3) {
  return bigInt(z1).modPow(bigInt(z2), bigInt(z3));
}

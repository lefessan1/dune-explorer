(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Data_types
open Db_intf

let string_of_cal t =
  CalendarLib.Printer.Calendar.sprint Date.format t
let cal_of_string s =
  CalendarLib.Printer.Calendar.from_fstring Date.format s

let date_of_cal t =
  Date.from_string (CalendarLib.Printer.Calendar.sprint Date.format t)
let cal_of_date s =
  CalendarLib.Printer.Calendar.from_fstring Date.format (Date.to_string s)

let date_of_cal_dune t =
  Dune_types.Date.from_string
    (CalendarLib.Printer.Calendar.sprint Dune_types.Date.format t)
let cal_of_date_dune s =
  CalendarLib.Printer.Calendar.from_fstring Dune_types.Date.format
    (Dune_types.Date.to_string s)

let rows_to_option = function
  | [] -> None
  | [x] -> Some x
  | _ -> assert false

let protocol_not_found = "Can't recover this protocol"

let option_to_protocol = function
  | None -> protocol_not_found
  | Some p -> p

let hash_selector_of_hash selector =
  match String.get selector 0 with
  | 'b' | 'B' -> Block selector
  | 'd' | 'K' -> Account selector
  | 't' -> Account (Crypto.tz_to_dn selector)
  | _ -> Empty

let mem_filter filter filters = List.mem filter filters

let anon_types filters =
  mem_filter "Nonce" filters,
  mem_filter "Activation" filters,
  mem_filter "Double_baking_evidence" filters,
  mem_filter "Double_endorsement_evidence" filters

let manager_types filters =
  mem_filter "Transaction" filters,
  mem_filter "Origination" filters,
  mem_filter "Delegation" filters,
  mem_filter "Reveal" filters

let filters_flag filters =
  let empty = filters = [] in
  mem_filter "Nonce" filters || empty,
  mem_filter "Activation" filters || empty,
  mem_filter "Transaction" filters || empty,
  mem_filter "Origination" filters || empty,
  mem_filter "Delegation" filters || empty,
  mem_filter "Endorsement" filters || empty,
  mem_filter "Proposal" filters || empty,
  mem_filter "Ballot" filters || empty,
  mem_filter "Double_baking_evidence" filters || empty,
  mem_filter "Double_endorsement_evidence" filters || empty,
  mem_filter "Reveal" filters || empty

let omap f = function None -> None | Some x -> Some (f x)
let noop = function None -> assert false | Some x -> x

let to_name_opt ?alias = function
  | None -> None
  | Some dn -> Some {dn; alias}

let convert_opt f = function
  | None -> None
  | Some x -> Some (f x)

let test_opt f = function
  | None -> None, true
  | Some x -> Some (f x), false

let test_opti = function
  | None -> None, true
  | Some x -> Some x, false

let block_of_tuple_noop
    (hash, predecessor_hash, fitness, baker, timestamp, protocol, test_protocol,
     network, test_network, test_network_expiration,
     level, _level_position, priority,
     cycle, _cycle_position,
     _voting_period, _voting_period_position,
     commited_nonce_hash, pow_nonce,
     distance_level, nb_operations,
     validation_pass, proto, data, signature,
     volume, fees, _voting_period_kind) =
  let block_protocol = { proto_name = protocol; proto_hash = protocol } in
  let test_protocol = { proto_name = test_protocol; proto_hash = test_protocol } in
  let level = Int64.to_int level in
  let cycle = Int64.to_int cycle in
  let timestamp = date_of_cal timestamp in
  { hash; predecessor_hash; fitness; baker = Alias.to_name baker;
    timestamp;
    validation_pass = Int64.to_int validation_pass;
    operations = [] ;
    protocol = block_protocol ;
    nb_operations = Int64.to_int nb_operations;
    test_protocol; network; test_network;
    test_network_expiration; priority = Int32.to_int priority; level;
    commited_nonce_hash; pow_nonce;
    proto = Int64.to_int proto;
    data;
    signature;
    volume;
    fees;
    distance_level = Int64.to_int distance_level;
    cycle }

let block_of_tuple_ignore_op
    (hash, predecessor_hash, fitness, baker, timestamp,
     protocol, test_protocol,
     network, test_network, test_network_expiration,
     level, level_position, priority,
     cycle, cycle_position,
     voting_period, voting_period_position,
     commited_nonce_hash, pow_nonce,
     distance_level, nb_operations,
     validation_pass, proto, data, signature,
     volume, fees, voting_period_kind,
     _op_hash, _op_type) =
  block_of_tuple_noop
    (hash, predecessor_hash, fitness, baker, timestamp,
     protocol, test_protocol,
     network, test_network, test_network_expiration,
     level, level_position, priority,
     cycle, cycle_position,
     voting_period, voting_period_position,
     commited_nonce_hash, pow_nonce,
     distance_level, nb_operations,
     validation_pass, proto, data, signature,
     volume, fees, voting_period_kind)

let op_hash_to_bop_hash op_hash = (op_hash, "", "")

let block_op_get_op_hash
    (_hash, _predecessor, _fitness, _baker, _timestamp,
     _protocol, _test_protocol,
     _network, _test_network, _test_network_expiration,
     _level, _level_position, _priority,
     _cycle, _cycle_position,
     _voting_period, _voting_period_position,
     _commited_nonce_hash, _pow_nonce,
     _distance_level, _nb_operations,
     _validation_pass, _proto, _data, _signature,
     _volume, _fees, _voting_period_kind,
     op_hash, _op_type) = op_hash

let unoption_op_hash = function
  | (None,_,_) -> None
  | (Some a,b,c) -> Some (a,b,c)

let block_op_get_hash
    (hash, _predecessor, _fitness, _baker, _timestamp,
     _protocol, _test_protocol,
     _network, _test_network, _test_network_expiration,
     _level, _level_position, _priority,
     _cycle, _cycle_position,
     _voting_period, _voting_period_position,
     _commited_nonce_hash, _pow_nonce,
     _distance_level, _nb_operations,
     _validation_pass, _proto, _data, _signature,
     _volume, _fees, _voting_periond_kind,
     _op_hash, _op_type) = hash

let block_with_pred_fitness
    (hash, predecessor, fitness, baker, timestamp,
     protocol, test_protocol,
     network, test_network, test_network_expiration,
     level, level_position, priority,
     cycle, cycle_position,
     voting_period, voting_period_position,
     commited_nonce_hash, pow_nonce,
     distance_level, nb_operations,
     validation_pass, proto, data, signature,
     volume, fees, voting_period_kind, pred_fitness) =
  block_of_tuple_noop (
    hash, predecessor, fitness, baker, timestamp,
    protocol, test_protocol,
    network, test_network, test_network_expiration,
    level, level_position, priority,
    cycle, cycle_position,
    voting_period, voting_period_position,
    commited_nonce_hash, pow_nonce,
    distance_level, nb_operations,
    validation_pass, proto, data, signature,
    volume, fees, voting_period_kind), pred_fitness


let block_op_get_ops x =
  List.map (fun x -> op_hash_to_bop_hash (noop x))
    (noop (block_op_get_op_hash x))

let block_of_tuple_with_ops x =
  { (block_of_tuple_ignore_op x) with
    operations = block_op_get_ops x }

let errors_from_db = function
  | None -> None
  | Some l -> let rec aux = function
      | (err_id :: err_kind :: err_infos :: t) ->
        {err_id = Misc.unopt "" err_id;
         err_kind = Misc.unopt "" err_kind;
         err_info = Misc.unopt "{}" err_infos} :: (aux t)
      | _ -> [] in
    Some (aux l)

let or_script_from_db sc_code sc_code_hash sc_storage =
  match sc_code, sc_code_hash, sc_storage with
  | None, None, (Some "{}" | None) -> None
  | sc_code, sc_code_hash, Some sc_storage ->
    Some {sc_code; sc_code_hash; sc_storage}
  | sc_code, sc_code_hash, None ->
    Some {sc_code; sc_code_hash; sc_storage = "{}"}

let activation_from_db
    (act_pkh, act_secret, act_balance, act_op_level, act_timestamp) =
  let act_pkh = Alias.to_name act_pkh in
  let act_op_level = Int64.to_int act_op_level in
  let act_timestamp = string_of_cal act_timestamp in
  let act = { act_pkh; act_secret; act_balance; act_op_level;
              act_timestamp } in
  Activation act

let dee_from_db
    (_, endorse1_block_hash, endorse1_block_level, endorse2_block_hash,
     endorse2_block_level, accused, denouncer, lost_deposit, lost_rewards,
     lost_fees, gain_rewards, op_level, _op_cycle, _op_block_hash,
     _distance_level, _network, _tsp) =
  let endorse1 = {
    endorse_src = Alias.to_name "" ;
    endorse_op_level = Int64.to_int op_level ;
    endorse_block_level = Int64.to_int endorse1_block_level ;
    endorse_block_hash = endorse1_block_hash ;
    endorse_slot = [];
    endorse_priority = -1; endorse_timestamp = ""
  } in
  let endorse2 = {
    endorse_src = Alias.to_name "" ;
    endorse_op_level = Int64.to_int op_level ;
    endorse_block_level = Int64.to_int endorse2_block_level ;
    endorse_block_hash = endorse2_block_hash ;
    endorse_slot = [];
    endorse_priority = -1; endorse_timestamp = ""
  } in
  let dee = {
    double_endorsement1 = endorse1 ;
    double_endorsement2 = endorse2 ;
    double_endorsement_accused = Alias.to_name accused;
    double_endorsement_denouncer = Alias.to_name denouncer;
    double_endorsement_lost_deposit = lost_deposit;
    double_endorsement_lost_rewards = lost_rewards;
    double_endorsement_lost_fees = lost_fees;
    double_endorsement_gain_rewards = gain_rewards
  } in
  Double_endorsement_evidence dee

let dbe_from_db
    (signature, hash, _block_hash, _network, accused,
     denouncer, lost_deposit, lost_rewards, lost_fees, gain_rewards,
     _h1_id , h1_level, h1_proto, h1_predecessor, h1_timestamp, h1_validation_pass,
     h1_operations_hash, h1_fitness, h1_context, h1_priority,
     h1_commited_nonce_hash, h1_pow_nonce, h1_signature,
     _h2_id , h2_level, h2_proto, h2_predecessor, h2_timestamp, h2_validation_pass,
     h2_operations_hash, h2_fitness, h2_context, h2_priority,
     h2_commited_nonce_hash, h2_pow_nonce, h2_signature) =
  let h1 = {
    hd_level = Int64.to_int h1_level ;
    hd_proto = Int64.to_int h1_proto ;
    hd_predecessor = h1_predecessor ;
    hd_timestamp = date_of_cal h1_timestamp ;
    hd_validation_pass = Int64.to_int h1_validation_pass ;
    hd_operations_hash = h1_operations_hash ;
    hd_fitness = h1_fitness ;
    hd_context = h1_context ;
    hd_priority = Int32.to_int h1_priority ;
    hd_seed_nonce_hash = h1_commited_nonce_hash ;
    hd_proof_of_work_nonce = h1_pow_nonce ;
    hd_signature = h1_signature;
    hd_hash = None;
    hd_network = None} in
  let h2 = {
    hd_level = Int64.to_int h2_level ;
    hd_proto = Int64.to_int h2_proto ;
    hd_predecessor = h2_predecessor ;
    hd_timestamp = date_of_cal h2_timestamp ;
    hd_validation_pass = Int64.to_int h2_validation_pass ;
    hd_operations_hash = h2_operations_hash ;
    hd_fitness = h2_fitness ;
    hd_context = h2_context ;
    hd_priority = Int32.to_int h2_priority ;
    hd_seed_nonce_hash = h2_commited_nonce_hash ;
    hd_proof_of_work_nonce = h2_pow_nonce ;
    hd_signature = h2_signature;
    hd_hash = None;
    hd_network = None} in
  let double_baking_main =
    if h1_signature = signature then 0
    else if h2_signature = signature then 1
    else (-1) in
  let double_baking_accused =
    match accused with None  -> Alias.to_name "unknown" | Some dn1 -> Alias.to_name dn1 in
  let double_baking_denouncer =
    match denouncer with None -> Alias.to_name "unknown" | Some dn1 -> Alias.to_name dn1 in
  (* let unopt = function None -> 0L | Some v -> v in *)
  let double_baking_lost_deposit = Misc.unopt Int64.zero lost_deposit in
  let double_baking_lost_rewards = Misc.unopt Int64.zero lost_rewards in
  let double_baking_lost_fees = Misc.unopt Int64.zero lost_fees in
  let double_baking_gain_rewards = Misc.unopt Int64.zero gain_rewards in
  let dbe =
    { double_baking_header1 = h1 ; double_baking_header2 = h2;
      double_baking_main ;
      double_baking_accused ;
      double_baking_denouncer ;
      double_baking_lost_deposit ;
      double_baking_lost_rewards ;
      double_baking_lost_fees ;
      double_baking_gain_rewards } in
  Printf.eprintf "%s\n%!" hash;
  Double_baking_evidence dbe

let nonce_from_db (seed_level, seed_nonce) =
  let seed_level = Int64.to_int seed_level in
  Seed_nonce_revelation { seed_level; seed_nonce }

let endorsement_from_db
    (endorse_src, endorse_block_level, endorse_block_hash,
     endorse_slot, endorse_op_level, endorse_priority, endorse_timestamp) =
  let endorse_block_level = Int64.to_int endorse_block_level in
  let endorse_op_level = Int64.to_int endorse_op_level in
  let endorse_priority = Int32.to_int endorse_priority in
  let endorse_slot = Misc.unopt_list Int32.to_int endorse_slot in
  let endorse_timestamp = string_of_cal endorse_timestamp in
  Sourced (Endorsement {
      endorse_src = Alias.to_name endorse_src; endorse_block_hash;
      endorse_block_level; endorse_slot; endorse_op_level; endorse_priority;
      endorse_timestamp })

let transaction_from_db_base
    (tr_src, tr_dst, tr_fee, tr_counter,
     tr_amount, tr_parameters, gas_limit, storage_limit,
     tr_failed, tr_internal, tr_burn, tr_op_level, tr_timestamp,
     tr_errors, tr_collect_fee_gas, tr_collect_pk) =
  let tr_src = Alias.to_name tr_src in
  let tr_dst = Alias.to_name tr_dst in
  let tr_counter = Int64.to_int32 tr_counter in
  let tr_gas_limit = match gas_limit with
    | None -> Z.zero
    | Some gas_limit -> Z.of_int64  gas_limit in
  let tr_storage_limit = match storage_limit with
    | None -> Z.zero
    | Some storage_limit -> Z.of_int64 storage_limit in
  let tr_parameters = match tr_parameters with
    | Some "" -> None
    | _ -> tr_parameters in
  let tr_op_level = Int64.to_int tr_op_level in
  let tr_timestamp = string_of_cal tr_timestamp in
  let tr_errors = errors_from_db tr_errors in {
    tr_src; tr_dst; tr_amount; tr_counter; tr_fee; tr_gas_limit; tr_storage_limit;
    tr_parameters; tr_failed; tr_internal; tr_burn; tr_op_level; tr_timestamp;
    tr_errors; tr_collect_fee_gas; tr_collect_pk }

let transaction_from_db rows =
  let source, l = List.fold_left
      (fun (src, acc) row ->
         let tr = transaction_from_db_base row in
         let src = match src with
           | None when not tr.tr_internal -> Some tr.tr_src.dn
           | _ -> src in
         src, Transaction tr :: acc) (None, []) rows in
  match source with
  | None -> "", []
  | Some source -> source, List.rev l

let origination_from_db_base
  (or_src, or_kt1, or_fee, or_counter, or_manager, or_delegate,
   sc_code, sc_storage, sc_code_hash, or_spendable,
   or_delegatable, or_balance, gas_limit, storage_limit,
   or_failed, or_internal, or_burn, or_op_level, or_timestamp, or_errors) =
  let or_delegate = match or_delegate with
    | None -> Alias.to_name or_src
    | Some del -> Alias.to_name del in
  let or_script = or_script_from_db sc_code sc_code_hash sc_storage in
  let or_src = Alias.to_name or_src in
  let or_kt1 = Alias.to_name or_kt1 in
  let or_counter = Int64.to_int32 or_counter in
  let or_manager = Alias.to_name or_manager in
  let or_gas_limit =
    match gas_limit with
    | None -> Z.zero
    | Some gas_limit -> Z.of_int64  gas_limit in
  let or_storage_limit =
    match storage_limit with
    | None -> Z.zero
    | Some storage_limit -> Z.of_int64 storage_limit in
  let or_op_level = Int64.to_int or_op_level in
  let or_timestamp = string_of_cal or_timestamp in
  let or_errors = errors_from_db or_errors in {
    or_src; or_kt1; or_manager; or_delegate; or_script; or_spendable;
    or_delegatable; or_balance; or_counter; or_fee; or_gas_limit; or_storage_limit;
    or_failed; or_internal; or_burn; or_op_level; or_timestamp; or_errors }

let origination_from_db rows =
  let source, l = List.fold_left
      (fun (src, acc) row ->
         let ori = origination_from_db_base row in
         let src = match src with
           | None when not ori.or_internal -> Some ori.or_src.dn
           | _ -> src in
         src, Origination ori :: acc) (None, []) rows in
  match source with
  | None -> "", []
  | Some source -> source, List.rev l

let delegation_from_db_base
    (del_src, del_fee, del_counter, del_delegate, gas_limit, storage_limit,
     del_failed, del_internal, del_op_level, del_timestamp, del_errors) =
  let del_delegate = match del_delegate with
    | None -> Alias.to_name ""
    | Some dn -> Alias.to_name dn in
  let del_src = Alias.to_name del_src in
  let del_counter = Int64.to_int32 del_counter in
  let del_gas_limit =
    match gas_limit with
    | None -> Z.zero
    | Some gas_limit -> Z.of_int64  gas_limit in
  let del_storage_limit =
    match storage_limit with
    | None -> Z.zero
    | Some storage_limit -> Z.of_int64 storage_limit in
  let del_op_level = Int64.to_int del_op_level in
  let del_timestamp = string_of_cal del_timestamp in
  let del_errors = errors_from_db del_errors in
  { del_src; del_delegate; del_counter; del_fee; del_gas_limit; del_storage_limit;
    del_failed; del_internal; del_op_level; del_timestamp; del_errors }

let delegation_from_db rows =
  let source, l = List.fold_left
      (fun (src, acc) row ->
         let del = delegation_from_db_base row in
         let src = match src with
           | None when not del.del_internal -> Some del.del_src.dn
           | _ -> src in
         src, Delegation del :: acc) (None, []) rows in
  match source with
  | None -> "", []
  | Some source -> source, List.rev l

let reveal_from_db_base
    (rvl_src, rvl_fee, rvl_counter, rvl_pubkey, gas_limit, storage_limit,
     rvl_failed, rvl_internal, rvl_op_level, rvl_timestamp, rvl_errors) =
  let rvl_pubkey = Misc.unopt "" rvl_pubkey in
  let rvl_src = Alias.to_name rvl_src in
  let rvl_counter = Int64.to_int32 rvl_counter in
  let rvl_gas_limit =
    match gas_limit with
    | None -> Z.zero
    | Some gas_limit -> Z.of_int64  gas_limit in
  let rvl_storage_limit =
    match storage_limit with
    | None -> Z.zero
    | Some storage_limit -> Z.of_int64 storage_limit in
  let rvl_op_level = Int64.to_int rvl_op_level in
  let rvl_timestamp = string_of_cal rvl_timestamp in
  let rvl_errors = errors_from_db rvl_errors in {
    rvl_src; rvl_pubkey; rvl_counter; rvl_fee; rvl_gas_limit; rvl_storage_limit;
    rvl_failed; rvl_internal; rvl_op_level; rvl_timestamp; rvl_errors }

let reveal_from_db rows =
  let source, l = List.fold_left
      (fun (src, acc) row ->
         let rvl = reveal_from_db_base row in
         let src = match src with
           | None when not rvl.rvl_internal -> Some rvl.rvl_src.dn
           | _ -> src in
         src, Reveal rvl :: acc) (None, []) rows in
  match source with
  | None -> "", []
  | Some source -> source, List.rev l

let op_type_from_db_row row =
  let ( _op_hash, _op_block_hash, _op_network_hash, op_type, _op_level,
        (* Nonce *)
        _, _s_level, _s_nonce,
        (* Activation *)
        _, _a_pkh, _a_secret,
        (* Transaction*)
        _, _tr_src, _tr_dst, _tr_fee, _tr_counter, _tr_amount,
        _tr_parameters, _tr_gas_limit, _tr_storage_limit, _tr_failed,
        _tr_internal, _tr_burn, _tr_collect_fee_gas, _tr_collect_pk,
        (* Origination *)
        _, _or_src, _or_kt1, _or_fee, _or_counter, _or_manager, _or_delegate,
        _sc_code, _sc_storage, _sc_code_hash, _or_spendable, _or_delegatable,
        _or_gas_limit, _or_storage_limit,
        _or_balance, _or_failed, _or_internal, _or_burn,
        (* Delegation *)
        _, _del_src, _del_pubkey, _del_fee, _del_counter, _del_delegate,
        _del_gas_limit, _del_storage_limit, _del_failed, _del_internal,
        (* Endorsement *)
        _, _endorse_src, _, _endorse_block_hash, _endorse_slot,
        _endorse_block_level,
        (* Reveal *)
        _, _rvl_src, _rvl_fee, _rvl_counter, _rvl_pubkey,
        _rvl_gas_limit, _rvl_storage_limit, _rvl_failed, _rvl_internal) = row in
  op_type

let op_hash_from_db_row row =
  let ( op_hash, _op_block_hash, _op_network_hash, _op_type, _op_level,
        (* Nonce *)
        _, _s_level, _s_nonce,
        (* Activation *)
        _, _a_pkh, _a_secret,
        (* Transaction*)
        _, _tr_src, _tr_dst, _tr_fee, _tr_counter, _tr_amount,
        _tr_parameters, _tr_gas_limit, _tr_storage_limit, _tr_failed,
        _tr_internal, _tr_burn, _tr_collect_fee_gas, _tr_collect_pk,
        (* Origination *)
        _, _or_src, _or_kt1, _or_fee, _or_counter, _or_manager, _or_delegate,
        _sc_code, _sc_storage, _sc_code_hash, _or_spendable, _or_delegatable,
        _or_gas_limit, _or_storage_limit,
        _or_balance, _or_failed, _or_internal, _or_burn,
        (* Delegation *)
        _, _del_src, _del_pubkey, _del_fee, _del_counter, _del_delegate,
        _del_gas_limit, _del_storage_limit, _del_failed, _del_internal,
        (* Endorsement *)
        _, _endorse_src, _, _endorse_block_hash, _endorse_slot,
        _endorse_block_level,
        (* Reveal *)
        _, _rvl_src, _rvl_fee, _rvl_counter, _rvl_pubkey,
        _rvl_gas_limit, _rvl_storage_limit, _rvl_failed, _rvl_internal) = row in
  op_hash

let op_block_hash_from_db_row row =
  let ( _op_hash, op_block_hash, _op_network_hash, _op_type, _op_level,
        (* Nonce *)
        _, _s_level, _s_nonce,
        (* Activation *)
        _, _a_pkh, _a_secret,
        (* Transaction*)
        _, _tr_src, _tr_dst, _tr_fee, _tr_counter, _tr_amount,
        _tr_parameters, _tr_gas_limit, _tr_storage_limit, _tr_failed,
        _tr_internal, _tr_burn, _tr_collect_fee_gas, _tr_collect_pk,
        (* Origination *)
        _, _or_src, _or_kt1, _or_fee, _or_counter, _or_manager, _or_delegate,
        _sc_code, _sc_storage, _sc_code_hash, _or_spendable, _or_delegatable,
        _or_gas_limit, _or_storage_limit,
        _or_balance, _or_failed, _or_internal, _or_burn,
        (* Delegation *)
        _, _del_src, _del_pubkey, _del_fee, _del_counter, _del_delegate,
        _del_gas_limit, _del_storage_limit, _del_failed, _del_internal,
        (* Endorsement *)
        _, _endorse_src, _, _endorse_block_hash, _endorse_slot,
        _endorse_block_level,
        (* Reveal *)
        _, _rvl_src, _rvl_fee, _rvl_counter, _rvl_pubkey,
        _rvl_gas_limit, _rvl_storage_limit, _rvl_failed, _rvl_internal) = row in
  op_block_hash

let op_network_from_db_row row =
  let ( _op_hash, _op_block_hash, op_network_hash, _op_type, _op_level,
        (* Nonce *)
        _, _s_level, _s_nonce,
        (* Activation *)
        _, _a_pkh, _a_secret,
        (* Transaction*)
        _, _tr_src, _tr_dst, _tr_fee, _tr_counter, _tr_amount,
        _tr_parameters, _tr_gas_limit, _tr_storage_limit, _tr_failed,
        _tr_internal, _tr_burn, _tr_collect_fee_gas, _tr_collect_pk,
        (* Origination *)
        _, _or_src, _or_kt1, _or_fee, _or_counter, _or_manager, _or_delegate,
        _sc_code, _sc_storage, _sc_code_hash, _or_spendable, _or_delegatable,
        _or_gas_limit, _or_storage_limit,
        _or_balance, _or_failed, _or_internal, _or_burn,
        (* Delegation *)
        _, _del_src, _del_pubkey, _del_fee, _del_counter, _del_delegate,
        _del_gas_limit, _del_storage_limit, _del_failed, _del_internal,
        (* Endorsement *)
        _, _endorse_src, _, _endorse_block_hash, _endorse_slot,
        _endorse_block_level,
        (* Reveal *)
        _, _rvl_src, _rvl_fee, _rvl_counter, _rvl_pubkey,
        _rvl_gas_limit, _rvl_storage_limit, _rvl_failed, _rvl_internal) = row in
  op_network_hash

let op_src_from_db_row row =
  let ( _op_hash, _op_block_hash, _op_network_hash, _op_type, _op_level,
        (* Nonce *)
        _, _s_level, _s_nonce,
        (* Activation *)
        _, _a_pkh, _a_secret,
        (* Transaction*)
        _, tr_src, _tr_dst, _tr_fee, _tr_counter, _tr_amount,
        _tr_parameters, _tr_gas_limit, _tr_storage_limit, _tr_failed,
        _tr_internal, _tr_burn, _tr_collect_fee_gas, _tr_collect_pk,
        (* Origination *)
        _, or_src, _or_kt1, _or_fee, _or_counter, _or_manager, _or_delegate,
        _sc_code, _sc_storage, _sc_code_hash, _or_spendable, _or_delegatable,
        _or_gas_limit, _or_storage_limit,
        _or_balance, _or_failed, _or_internal, _or_burn,
        (* Delegation *)
        _, del_src, _del_pubkey, _del_fee, _del_counter, _del_delegate,
        _del_gas_limit, _del_storage_limit, _del_failed, _del_internal,
        (* Endorsement *)
        _, _endorse_src, _, _endorse_block_hash, _endorse_slot,
        _endorse_block_level,
        (* Reveal *)
        _, rvl_src, _rvl_fee, _rvl_counter, _rvl_pubkey,
        _rvl_gas_limit, _rvl_storage_limit, _rvl_failed, _rvl_internal) = row in
  match tr_src, or_src, del_src, rvl_src with
  | Some src, _, _, _ -> src
  | None, Some src, _, _ -> src
  | None, None, Some src, _ -> src
  | None, None, None, Some src -> src
  | _ -> assert false

let op_fee_from_db_row row =
  let ( _op_hash, _op_block_hash, _op_network_hash, _op_type, _op_level,
        (* Nonce *)
        _, _s_level, _s_nonce,
        (* Activation *)
        _, _a_pkh, _a_secret,
        (* Transaction*)
        _, _tr_src, _tr_dst, tr_fee, _tr_counter, _tr_amount,
        _tr_parameters, _tr_gas_limit, _tr_storage_limit, _tr_failed,
        _tr_internal, _tr_burn, _tr_collect_fee_gas, _tr_collect_pk,
        (* Origination *)
        _, _or_src, _or_kt1, or_fee, _or_counter, _or_manager, _or_delegate,
        _sc_code, _sc_storage, _sc_code_hash, _or_spendable, _or_delegatable,
        _or_gas_limit, _or_storage_limit,
        _or_balance, _or_failed, _or_internal, _or_burn,
        (* Delegation *)
        _, _del_src, _del_pubkey, del_fee, _del_counter, _del_delegate,
        _del_gas_limit, _del_storage_limit, _del_failed, _del_internal,
        (* Endorsement *)
        _, _endorse_src, _, _endorse_block_hash, _endorse_slot,
        _endorse_block_level,
        (* Reveal *)
        _, _rvl_src, rvl_fee, _rvl_counter, _rvl_pubkey,
        _rvl_gas_limit, _rvl_storage_limit, _rvl_failed, _rvl_internal) = row in
  match tr_fee, or_fee, del_fee, rvl_fee with
  | Some fee, _, _, _ -> fee
  | None, Some fee, _, _ -> fee
  | None, None, Some fee, _ -> fee
  | None, None, None, Some fee -> fee
  | _ -> assert false

let op_counter_from_db_row row =
  let ( _op_hash, _op_block_hash, _op_network_hash, _op_type, _op_level,
        (* Nonce *)
        _, _s_level, _s_nonce,
        (* Activation *)
        _, _a_pkh, _a_secret,
        (* Transaction*)
        _, _tr_src, _tr_dst, _tr_fee, tr_counter, _tr_amount,
        _tr_parameters, _tr_gas_limit, _tr_storage_limit, _tr_failed,
        _tr_internal, _tr_burn, _tr_collect_fee_gas, _tr_collect_pk,
        (* Origination *)
        _, _or_src, _or_kt1, _or_fee, or_counter, _or_manager, _or_delegate,
        _sc_code, _sc_storage, _sc_code_hash, _or_spendable, _or_delegatable,
        _or_gas_limit, _or_storage_limit,
        _or_balance, _or_failed, _or_internal, _or_burn,
        (* Delegation *)
        _, _del_src, _del_pubkey, _del_fee, del_counter, _del_delegate,
        _del_gas_limit, _del_storage_limit, _del_failed, _del_internal,
        (* Endorsement *)
        _, _endorse_src, _, _endorse_block_hash, _endorse_slot,
        _endorse_block_level,
        (* Reveal *)
        _, _rvl_src, _rvl_fee, rvl_counter, _rvl_pubkey,
        _rvl_gas_limit, _rvl_storage_limit, _rvl_failed, _rvl_internal) = row in
  match tr_counter, or_counter, del_counter, rvl_counter with
  | Some counter, _, _, _ -> counter
  | None, Some counter, _, _ -> counter
  | None, None, Some counter, _ -> counter
  | None, None, None, Some counter -> counter
  | _ -> assert false

let op_gas_from_db_row row =
  let ( _op_hash, _op_block_hash, _op_network_hash, _op_type, _op_level,
        (* Nonce *)
        _, _s_level, _s_nonce,
        (* Activation *)
        _, _a_pkh, _a_secret,
        (* Transaction*)
        _, _tr_src, _tr_dst, _tr_fee, _tr_counter, _tr_amount,
        _tr_parameters, tr_gas_limit, _tr_storage_limit, _tr_failed,
        _tr_internal, _tr_burn, _tr_collect_fee_gas, _tr_collect_pk,
        (* Origination *)
        _, _or_src, _or_kt1, _or_fee, _or_counter, _or_manager, _or_delegate,
        _sc_code, _sc_storage, _sc_code_hash, _or_spendable, _or_delegatable,
        or_gas_limit, _or_storage_limit,
        _or_balance, _or_failed, _or_internal, _or_burn,
        (* Delegation *)
        _, _del_src, _del_pubkey, _del_fee, _del_counter, _del_delegate,
        del_gas_limit, _del_storage_limit, _del_failed, _del_internal,
        (* Endorsement *)
        _, _endorse_src, _, _endorse_block_hash, _endorse_slot,
        _endorse_block_level,
        (* Reveal *)
        _, _rvl_src, _rvl_fee, _rvl_counter, _rvl_pubkey,
        rvl_gas_limit, _rvl_storage_limit, _rvl_failed, _rvl_internal) = row in
  match tr_gas_limit, or_gas_limit, del_gas_limit, rvl_gas_limit with
  | Some gas_limit, _, _, _ -> Some gas_limit
  | None, Some gas_limit, _, _ -> Some gas_limit
  | None, None, Some gas_limit, _ -> Some gas_limit
  | None, None, None, Some gas_limit -> Some gas_limit
  | _ -> assert false

let op_storage_from_db_row row =
  let ( _op_hash, _op_block_hash, _op_network_hash, _op_type, _op_level,
        (* Nonce *)
        _, _s_level, _s_nonce,
        (* Activation *)
        _, _a_pkh, _a_secret,
        (* Transaction*)
        _, _tr_src, _tr_dst, _tr_fee, _tr_counter, _tr_amount,
        _tr_parameters, _tr_gas_limit, tr_storage_limit, _tr_failed,
        _tr_internal, _tr_burn, _tr_collect_fee_gas, _tr_collect_pk,
        (* Origination *)
        _, _or_src, _or_kt1, _or_fee, _or_counter, _or_manager, _or_delegate,
        _sc_code, _sc_storage, _sc_code_hash, _or_spendable, _or_delegatable,
        _or_gas_limit, or_storage_limit,
        _or_balance, _or_failed, _or_internal, _or_burn,
        (* Delegation *)
        _, _del_src, _del_pubkey, _del_fee, _del_counter, _del_delegate,
        _del_gas_limit, del_storage_limit, _del_failed, _del_internal,
        (* Endorsement *)
        _, _endorse_src, _, _endorse_block_hash, _endorse_slot,
        _endorse_block_level,
        (* Reveal *)
        _, _rvl_src, _rvl_fee, _rvl_counter, _rvl_pubkey,
        _rvl_gas_limit, rvl_storage_limit, _rvl_failed, _rvl_internal) = row in
  match tr_storage_limit, or_storage_limit, del_storage_limit, rvl_storage_limit with
  | Some storage_limit, _, _, _ -> Some storage_limit
  | None, Some storage_limit, _, _ -> Some storage_limit
  | None, None, Some storage_limit, _ -> Some storage_limit
  | None, None, None, Some storage_limit -> Some storage_limit
  | _ -> assert false

let operation_from_db rows =
  Printf.eprintf "operation_from_db %d\n%!" @@ List.length rows ;
  match rows with
  | [] -> None
  | hd :: _ ->
    let op_type = op_type_from_db_row hd in
    match op_type with
    | Some "Manager" ->
      Printf.eprintf "Manager %d\n%!" @@ List.length rows ;
      let op_hash =
        match op_hash_from_db_row hd with
        | Some op_hash -> op_hash
        | _ -> assert false in
      let op_block_hash =
        match op_block_hash_from_db_row hd with
        | Some op_block_hash -> op_block_hash
        | _ -> Utils.pending_block_hash
      in
      let op_network_hash =
        match op_network_from_db_row hd with
        | Some op_network_hash -> op_network_hash
        | _ -> Utils.pending_block_hash in
      let src = op_src_from_db_row hd in
      let ops =
        List.fold_left (fun acc row -> match row with
            | ( _op_hash, _op_block_hash, _op_network_hash,
                _op_type, op_level,
                (* Nonce *)
                _, _s_level, _s_nonce,
                (* Activation *)
                _, _a_pkh, _a_secret,
                (* Transaction*)
                _, tr_src, tr_dst, tr_fee, tr_counter, tr_amount,
                tr_parameters, tr_gas_limit, tr_storage_limit,
                tr_failed, tr_internal, tr_burn, tr_collect_fee_gas, tr_collect_pk,
                (* Origination *)
                _, or_src, or_kt1, or_fee, or_counter, or_manager,
                or_delegate, sc_code, sc_storage, sc_code_hash, or_spendable,
                or_delegatable, or_balance, or_gas_limit, or_storage_limit,
                or_failed, or_internal, or_burn,
                (* Delegation *)
                _, del_src, del_pubkey, del_fee, del_counter,
                del_delegate, del_gas_limit, del_storage_limit,
                del_failed, del_internal,
                (* Endorsement *)
                _, _endorse_src, _, _endorse_block_hash, _endorse_slot,
                _endorse_block_level,
                (* Reveal *)
                _, rvl_src, rvl_fee, rvl_counter, rvl_pubkey, rvl_gas_limit,
                rvl_storage_limit, rvl_failed, rvl_internal
              ) ->
              let acc_tr =
                match tr_src, tr_dst, tr_fee, tr_gas_limit,
                      tr_storage_limit, tr_counter, tr_amount,
                      tr_parameters, tr_failed, tr_internal, tr_burn,
                      op_level with
                | Some tr_src, Some tr_dst, Some tr_fee,
                  Some gas_limit, Some storage_limit, Some tr_counter,
                  Some tr_amount, tr_parameters, Some tr_failed,
                  Some tr_internal, Some tr_burn, Some tr_op_level ->
                  let tr_parameters = match tr_parameters with
                    | Some "" -> None
                    | _ -> tr_parameters in
                  let tr_gas_limit = Z.of_int64 gas_limit in
                  let tr_storage_limit = Z.of_int64 storage_limit in
                  let tr_counter = Int64.to_int32 tr_counter in
                  Transaction { tr_src; tr_dst; tr_amount; tr_parameters ;
                                tr_counter ; tr_fee ;
                                tr_gas_limit ; tr_storage_limit ;
                                tr_failed ; tr_internal ; tr_burn;
                                tr_op_level = Int64.to_int tr_op_level;
                                tr_timestamp = ""; tr_errors = None;
                                tr_collect_fee_gas; tr_collect_pk} :: acc
                | _ -> acc in
              let acc_ori =
                match or_src, or_kt1, or_fee, or_gas_limit, or_storage_limit,
                      or_counter, or_manager,
                      or_delegate, sc_code, sc_storage, or_spendable,
                      or_delegatable, or_balance, or_failed,
                      or_internal, or_burn, op_level with
                | Some or_src, Some or_kt1, Some or_fee, Some gas_limit,
                  Some storage_limit, Some or_counter, Some or_manager,
                  or_delegate, sc_code, sc_storage,
                  Some or_spendable,
                  Some or_delegatable, Some or_balance,
                  Some or_failed, Some or_internal, Some or_burn,
                  Some or_op_level ->
                  let or_delegate =
                    match or_delegate with None -> src | Some del -> del in
                  let or_script = or_script_from_db sc_code sc_code_hash sc_storage in
                  let or_gas_limit = Z.of_int64 gas_limit in
                  let or_storage_limit = Z.of_int64 storage_limit in
                  let or_counter = Int64.to_int32 or_counter in
                  let origi = {
                    or_src; or_kt1; or_manager; or_delegate; or_script;
                    or_spendable; or_delegatable; or_balance ;
                    or_counter ; or_fee ; or_gas_limit ; or_storage_limit ;
                    or_failed ; or_internal ; or_burn;
                    or_op_level = Int64.to_int or_op_level;
                    or_timestamp = ""; or_errors = None} in
                  Origination origi :: acc_tr
                | _ -> acc_tr in
              let acc_del =
                match del_src, del_pubkey, del_fee, del_gas_limit,
                      del_storage_limit, del_counter,
                      del_delegate, del_failed, del_internal, op_level with
                | Some del_src, Some _del_pubkey, Some del_fee,
                  Some gas_limit, Some storage_limit,
                  Some del_counter, del_delegate,
                  Some del_failed, Some del_internal, Some del_op_level ->
                  let del_delegate = Misc.unopt (Alias.to_name "") del_delegate in
                  let del_gas_limit = Z.of_int64 gas_limit in
                  let del_storage_limit = Z.of_int64 storage_limit in
                  let del_counter = Int64.to_int32 del_counter in
                  Delegation {
                    del_src; del_delegate; del_counter; del_fee; del_gas_limit;
                    del_storage_limit; del_failed; del_internal;
                    del_op_level = Int64.to_int del_op_level;
                    del_timestamp = ""; del_errors = None } :: acc_ori
                | _ -> acc_ori in
              let acc =
                match rvl_src, rvl_fee, rvl_gas_limit, rvl_storage_limit,
                      rvl_counter, rvl_pubkey, rvl_failed, rvl_internal, op_level with
                | Some rvl_src, Some rvl_fee, Some gas_limit,
                  Some storage_limit, Some rvl_counter,
                  rvl_pubkey, Some rvl_failed, Some rvl_internal, Some rvl_op_level ->
                  let rvl_pubkey = match rvl_pubkey with None -> ""| Some s -> s in
                  let rvl_gas_limit = Z.of_int64 gas_limit in
                  let rvl_storage_limit = Z.of_int64 storage_limit in
                  let rvl_counter = Int64.to_int32 rvl_counter in
                  Reveal { rvl_src; rvl_pubkey; rvl_counter; rvl_fee; rvl_gas_limit;
                           rvl_storage_limit; rvl_failed; rvl_internal;
                           rvl_op_level = Int64.to_int rvl_op_level;
                           rvl_timestamp = ""; rvl_errors = None } :: acc_del
                | _ -> acc_del in
              acc)
          [] rows in
      Some
        { op_hash; op_block_hash;
          op_network_hash;
          op_type = Sourced (Manager ("manager", src, ops)) }

    | Some "Anonymous" ->
      let op_hash =
        match op_hash_from_db_row hd with
        | Some op_hash -> op_hash
        | _ -> assert false in
      let op_block_hash =
        match op_block_hash_from_db_row hd with
        | Some op_block_hash -> op_block_hash
        | _ -> Utils.pending_block_hash
      in
      let op_network_hash =
        match op_network_from_db_row hd with
        | Some op_network_hash -> op_network_hash
        | _ -> Utils.pending_block_hash in
      let ops =
        List.fold_left (fun acc row -> match row with
            | ( _op_hash, _op_block_hash, _op_network_hash,
                _op_type, op_level,
                (* Nonce *)
                _, s_level, s_nonce,
                (* Activation *)
                _, a_pkh, a_secret,
                (* Transaction*)
                _, _tr_src, _tr_dst, _tr_fee, _tr_counter, _tr_amount,
                _tr_parameters, _tr_gas, _tr_st, _tr_failed, _tr_internal,
                _tr_burn, _tr_collect_fee_gas, _tr_collect_pk,
                (* Origination *)
                _, _or_src, _or_kt1, _or_fee, _or_counter, _or_manager,
                _or_delegate, _sc_code, _sc_storage, _sc_code_hash, _or_spendable,
                _or_delegatable, _or_balance, _or_gas, _or_st, _or_failed,
                _or_internal, _or_burn,
                (* Delegation *)
                _, _del_src, _del_pubkey, _del_fee, _del_counter,
                _del_delegate, _del_gas, _del_st, _del_failed, _del_internal,
                (* Endorsement *)
                _, _endorse_src, _, _endorse_block_hash, _endorse_slot,
                _endorse_block_level,
                (* Reveal *)
                _, _rvl_src, _rvl_fee, _rvl_counter, _rvl_pubkey,
                _rvl_gas, _rvl_st, _rvl_failed, _rvl_internal) ->
              begin match s_level, s_nonce, a_pkh, a_secret with
                | Some seed_level, Some seed_nonce, None, None ->
                  let seed_level = Int64.to_int seed_level in
                  let seed = { seed_level; seed_nonce; } in
                  Seed_nonce_revelation seed :: acc
                | None, None, Some act_pkh, Some act_secret ->
                  let act = { act_pkh; act_secret; act_balance = None;
                              act_op_level = Misc.unoptf 0 Int64.to_int op_level;
                              act_timestamp = ""} in
                  Activation act :: acc
                | _ -> acc
              end
            ) [] rows in
      Some
        { op_hash; op_block_hash;
          op_network_hash;
          op_type = Anonymous ops }


    | Some "Endorsement" ->
      begin match rows with
        | ( Some op_hash, Some op_block_hash, Some op_network_hash, _op_type,
            Some op_level,
            (* Nonce *)
            _, _s_level, _s_nonce,
            (* Activation *)
            _, _a_pkh, _a_secret,
            (* Transaction*)
            _, _tr_src, _tr_dst, _tr_fee, _tr_counter, _tr_amount, _tr_parameters,
            _tr_gas_limit, _tr_storage_limit, _tr_failed, _tr_internal, _tr_burn,
            _tr_collect_fee_gas, _tr_collect_pk,
            (* Origination *)
            _, _or_src, _or_kt1, _or_fee, _or_counter, _or_manager, _or_delegate,
            _sc_code, _sc_storage, _sc_code_hash, _or_spendable, _or_delegatable,
            _or_balance, _or_gas_limit, _or_storage_limit, _or_failed, _or_internal,
            _or_burn,
            (* Delegation *)
            _, _del_src, _del_pubkey, _del_fee, _del_counter, _del_delegate,
            _del_gas_limit, _del_storage_limit, _del_failed, _del_internal,
            (* Endorsement *)
            _, endorse_src, _, endorse_block_hash, endorse_slot,
            endorse_block_level,
            (* Reveal *)
            _, _rvl_src, _rvl_fee, _rvl_counter, _rvl_pubkey,
            _rvl_gas_limit, _rvl_storage_limit, _rvl_failed, _rvl_internal) :: [] ->
          begin match endorse_src, endorse_block_hash,
                      endorse_slot, endorse_block_level with
          | Some endorse_src, Some endorse_block_hash,
            Some endorse_slot, Some endorse_block_level ->
            let endorse_slot = Utils.unopt_i64_slot endorse_slot in
            let endorse_block_level = Int64.to_int endorse_block_level in
            let endorse =
              { endorse_src; endorse_block_hash; endorse_slot; endorse_block_level;
                endorse_op_level = Int64.to_int op_level; endorse_priority = -1;
                endorse_timestamp = "" } in
            Some {
              op_hash; op_block_hash;
              op_network_hash;
              op_type = Sourced (Endorsement endorse) }
          | _ -> None
          end
        | _ -> None
      end

    | Some "Dictator" | Some "Amendement" | _ -> None

let operation_from_db_list rows =
  Printf.eprintf "operation_from_db_list %d\n%!" @@ List.length rows ;
  let list_agg_by_hash =
    List.fold_left (fun acc row ->
        match row with
        | ( Some op_hash, Some _op_block_hash, _1, _2, _op_level,
            (* Nonce *)
            _3, _s_level, _s_nonce,
            (* Activation *)
            _4, _a_pkh, _a_secret,
            (* Transaction*)
            _5, _tr_src, _tr_dst, _tr_fee, _tr_counter, _tr_amount,
            _tr_parameters, _tr_gas_limit, _tr_storage_limit, _tr_failed,
            _tr_internal, _tr_burn, _tr_collect_fee_gas, _tr_collect_pk,
            (* Origination *)
            _6, _or_src, _or_kt1, _or_fee, _or_counter, _or_manager,
            _or_delegate,
            _sc_code, _sc_storage, _sc_code_hash, _or_spendable, _or_delegatable,
            _or_balance, _or_gas_limit, _or_storage_limit, _or_failed,
            _or_internal, _or_burn,
            (* Delegation *)
            _7, _del_src, _del_pubkey, _del_fee, _del_counter, _del_delegate,
            _del_gas_limit, _del_storage_limit, _del_failed, _del_internal,
            (* Endorsement *)
            _8, _endorse_src, _9, _endorse_block_hash, _endorse_slot,
            _endorse_block_level,
            (* Reveal *)
            _10, _rvl_src, _rvl_fee, _rvl_counter, _rvl_pubkey,
            _rvl_gas_limit, _rvl_storage_limit, _rvl_failed, _rvl_internal
          ) ->
          let row =
            (Some op_hash, Some _op_block_hash, _1, _2, _op_level,
             (* Nonce *)
             _3, _s_level, _s_nonce,
             (* Activation *)
             _4, to_name_opt _a_pkh, _a_secret,
             (* Transaction*)
             _5, to_name_opt _tr_src, to_name_opt _tr_dst,
             _tr_fee, _tr_counter, _tr_amount,
             _tr_parameters, _tr_gas_limit, _tr_storage_limit, _tr_failed,
             _tr_internal, _tr_burn, _tr_collect_fee_gas, _tr_collect_pk,
             (* Origination *)
             _6, to_name_opt _or_src, to_name_opt _or_kt1,
             _or_fee, _or_counter, to_name_opt _or_manager,
             to_name_opt _or_delegate,
             _sc_code, _sc_storage, _sc_code_hash, _or_spendable, _or_delegatable,
             _or_balance, _or_gas_limit, _or_storage_limit, _or_failed, _or_internal,
             _or_burn,
             (* Delegation *)
             _7, to_name_opt _del_src, _del_pubkey, _del_fee, _del_counter,
             to_name_opt _del_delegate,
             _del_gas_limit, _del_storage_limit, _del_failed, _del_internal,
             (* Endorsement *)
             _8, to_name_opt _endorse_src, _9, _endorse_block_hash, _endorse_slot,
             _endorse_block_level,
             (* Reveal *)
             _10, to_name_opt _rvl_src, _rvl_fee, _rvl_counter, _rvl_pubkey,
             _rvl_gas_limit, _rvl_storage_limit, _rvl_failed, _rvl_internal
            ) in
          begin try
              let old_list = List.assoc op_hash acc in
              (op_hash, row :: old_list) :: (List.remove_assoc op_hash acc)
            with Not_found ->
              (op_hash, [ row ]) ::  acc
          end
        | _ -> acc)
      [] rows in
  List.fold_left (fun acc (_hash, rows) ->
      match operation_from_db rows with
      | None -> acc
      | Some op -> op :: acc)
    [] list_agg_by_hash

let transaction_from_db_list rows =
  List.map
    (fun (op_hash, op_block_hash, op_network_hash,
          src, dst, fee, counter, amount, parameters, gas_limit, storage_limit,
          failed, internal, burn, op_level, timestamp, errors, collect_fee_gas,
          collect_pk) ->
      let tr = transaction_from_db_base
          (src, dst, fee, counter, amount, parameters, gas_limit, storage_limit,
           failed, internal, burn, op_level, timestamp, errors, collect_fee_gas,
           collect_pk) in
      let op_type = Sourced (Manager ("manager", tr.tr_src, [Transaction tr])) in
      {op_hash; op_block_hash; op_network_hash; op_type}) rows

let token_from_db_list rows =
  List.map
    (fun (op_hash, op_block_hash, op_network_hash, op_level, timestamp,
          kind, caller, ts_src, ts_dst, ts_amount, ts_flag) ->
      let op_level = Int64.to_int op_level in
      let timestamp = string_of_cal timestamp in
      let ts_src = Alias.to_name ts_src in
      let ts_dst = Alias.to_name ts_dst in
      let ts = {ts_src; ts_dst; ts_amount = Z.of_string ts_amount;
                ts_flag; ts_fee = 0L} in
      let op, caller = match kind, caller with
        | "transfer", _ -> TS_Transfer ts, ts_src
        | "transferFrom", Some caller -> TS_TransferFrom ts, Alias.to_name caller
        | "approve", _ -> TS_Approve ts, ts_src
        | _ -> TS_Unknown, {dn=""; alias = None} in
      { op_hash; op_block_hash; op_network_hash;
        op_type = Tokened (op_level, timestamp, caller , op) }) rows

let delegation_from_db_list rows =
  List.map
    (fun (op_hash, op_block_hash, op_network_hash,
          src, fee, counter, delegate, gas_limit, storage_limit,
          failed, internal, op_level, timestamp, errors) ->
      let del = delegation_from_db_base
          (src, fee, counter, delegate, gas_limit, storage_limit, failed, internal,
           op_level, timestamp, errors) in
      let op_type = Sourced (Manager ("manager", del.del_src, [Delegation del])) in
      {op_hash; op_block_hash; op_network_hash; op_type}) rows

let reveal_from_db_list rows =
  List.map
    (fun (op_hash, op_block_hash, op_network_hash,
          src, fee, counter, pubkey, gas_limit, storage_limit,
          failed, internal, op_level, timestamp, errors) ->
      let rvl = reveal_from_db_base
          (src, fee, counter, pubkey, gas_limit, storage_limit, failed, internal,
           op_level, timestamp, errors) in
      let op_type = Sourced (Manager ("manager", rvl.rvl_src, [Reveal rvl])) in
      {op_hash; op_block_hash; op_network_hash; op_type}) rows

let origination_from_db_list rows =
  List.map
    (fun (op_hash, op_block_hash, op_network_hash,
          src, kt1, fee, counter, manager, delegate, code, storage_type, code_hash,
          spendable, delegatable, balance, gas_limit, storage_limit,
          failed, internal, burn, op_level, timestamp, errors) ->
      let ori = origination_from_db_base
          (src, kt1, fee, counter, manager, delegate, code, storage_type, code_hash,
           spendable, delegatable, balance, gas_limit, storage_limit, failed,
           internal, burn, op_level, timestamp, errors) in
      let op_type = Sourced (Manager ("manager", ori.or_src, [Origination ori])) in
      {op_hash; op_block_hash; op_network_hash; op_type}) rows

let activation_from_db_list rows =
  List.map
    (fun (op_hash, op_block_hash, op_network_hash,
          act_pkh, act_secret, act_balance, act_op_level, act_timestamp ) ->
      let act = activation_from_db
          (act_pkh, act_secret, act_balance, act_op_level, act_timestamp) in
      let op_type = Anonymous [ act ] in
      {op_hash; op_block_hash; op_network_hash; op_type}) rows

let endorsement_from_db_list rows =
  List.map
    (fun ( op_hash, op_block_hash, op_network_hash, endorse_src,
           endorse_block_hash, endorse_slot, endorse_block_level,
           endorse_op_level, endorse_priority, endorse_timestamp ) ->
      let endorse_slot = Misc.unopt_list Int32.to_int endorse_slot in
      let endorse_block_level = Int64.to_int endorse_block_level in
      let endorse_op_level = Int64.to_int endorse_op_level in
      let endorse_priority = Int32.to_int endorse_priority in
      let endorse_src = Alias.to_name endorse_src in
      let endorse_timestamp = string_of_cal endorse_timestamp in
      let endorse =
        { endorse_src; endorse_block_hash; endorse_slot; endorse_block_level;
          endorse_op_level; endorse_priority; endorse_timestamp} in
      { op_hash; op_block_hash; op_network_hash;
        op_type = Sourced (Endorsement endorse) }) rows


let bakings_from_db_list rows =
  List.rev @@
  List.fold_left
    (fun acc bk -> match bk with
       | (bk_block_hash, bk_baker_hash, bk_level, bk_cycle, bk_priority,
          Some bk_missed_priority, bk_distance_level, bk_fees, Some bk_bktime,
          Some bk_baked, bk_tsp) ->
         let bk_level = Int64.to_int bk_level in
         let bk_cycle = Int64.to_int bk_cycle in
         let bk_priority = Int32.to_int bk_priority in
         let bk_distance_level = Int64.to_int bk_distance_level in
         let bk_baker_hash = Alias.to_name bk_baker_hash in
         let bk_tsp = string_of_cal bk_tsp in
         let bk_missed_priority =
           if bk_baked then Some (Int32.to_int bk_missed_priority)
           else None in
         {bk_block_hash; bk_baker_hash; bk_level; bk_cycle ; bk_priority;
          bk_missed_priority; bk_distance_level; bk_fees;
          bk_bktime = int_of_float bk_bktime; bk_baked; bk_tsp} :: acc
       | _ -> acc ) [] rows

let bakings_endorsement_from_db_list rows =
  List.rev @@ List.fold_left (fun acc row -> match row with
      | (Some ebk_level, Some ebk_lr_nslot, ebk_block, ebk_source, ebk_cycle,
         ebk_priority, ebk_dist, ebk_slots, ebk_tsp) ->
        let ebk_level = Int64.to_int ebk_level in
        let ebk_cycle = convert_opt Int64.to_int ebk_cycle in
        let ebk_dist = convert_opt Int32.to_int ebk_dist in
        let ebk_source = convert_opt Alias.to_name ebk_source in
        let ebk_priority = convert_opt Int32.to_int ebk_priority in
        let ebk_slots = convert_opt (Misc.unopt_list Int32.to_int) ebk_slots in
        let ebk_tsp = convert_opt string_of_cal ebk_tsp in
        let ebk_lr_nslot = Int32.to_int ebk_lr_nslot in
        {ebk_block; ebk_source; ebk_level; ebk_cycle; ebk_priority;
         ebk_dist; ebk_slots; ebk_lr_nslot; ebk_tsp} :: acc
      | _ -> acc) [] rows

let cycle_bakings_from_db_list current_cycle rows =
  List.rev @@ List.fold_left
    (fun acc cbk -> match cbk with
       | (cbk_cycle, Some cnt_all, Some cnt_steal, cnt_miss, Some cbk_priority,
          dun_fee, cbk_bktime, block_reward, block_security_deposit) ->
         let cbk_cycle = Int64.to_int cbk_cycle in
         let dun_reward = Int64.mul block_reward cnt_all in
         let dun_deposit = Int64.mul block_security_deposit cnt_all in
         let cbk_priority, cbk_bktime = if cnt_all = 0L then None, None else
             Some cbk_priority,
             Some Int64.(to_int @@ div (of_float cbk_bktime) cnt_all) in
         {cbk_cycle; cbk_depth = current_cycle - cbk_cycle;
          cbk_count = {cnt_all; cnt_miss; cnt_steal};
          cbk_dun = {dun_fee; dun_reward; dun_deposit};
          cbk_priority; cbk_bktime} :: acc
       | _ -> acc ) [] rows

let cycle_endorsements_from_db_list current_cycle rows =
  List.rev @@ List.fold_left
    (fun acc ced -> match ced with
       | (ced_cycle, Some cnt_all, cnt_miss, Some ced_priority, Some dun_reward,
          endorsement_security_deposit) ->
         let ced_cycle = Int64.to_int ced_cycle in
         let dun_deposit = Int64.mul cnt_all endorsement_security_deposit in
         {ced_cycle; ced_depth = current_cycle - ced_cycle;
          ced_slots = {cnt_all; cnt_miss; cnt_steal=0L};
          ced_dun = {dun_fee=0L; dun_reward; dun_deposit};
          ced_priority} :: acc
       | _ -> acc) [] rows


let rights_from_db_list rows =
  List.rev @@
  List.fold_left
    (fun acc row -> match row with
       | ( Some pr_level, Some pr_bakers, Some pr_endorsers,
           Some pr_bakers_priority, baked_priority, baker ) ->
         let r_level = Int64.to_int pr_level in
         let r_bakers = List.rev @@
           List.fold_left (fun acc s ->
               match s with Some s -> (Alias.to_name s) :: acc | None -> acc) [] pr_bakers in
         let r_endorsers = List.rev @@
           List.fold_left (fun acc s ->
               match s with Some s -> (Alias.to_name s) :: acc | None -> acc) [] pr_endorsers in
         let r_bakers_priority = List.rev @@
           List.fold_left (fun acc s ->
               match s with Some s -> Int32.to_int s :: acc | None -> acc)
           [] pr_bakers_priority in
       let r_baked = match baked_priority, baker with
         | Some prio, Some baker -> Some (Alias.to_name baker, Int32.to_int prio)
         | _ -> None in
       { r_level; r_bakers; r_endorsers ; r_bakers_priority; r_baked } :: acc
       | _ -> acc)
    [] rows

let baker_rights_from_db_list rows =
  List.rev @@ List.fold_left
    (fun acc x -> match x with
       | ( pr_level, pr_cycle, Some pr_prio, Some pr_depth ) ->
         let br_level = Int64.to_int pr_level in
         let br_cycle = Int64.to_int pr_cycle in
         let br_priority = Int32.to_int pr_prio in
         let br_depth = Int64.to_int pr_depth in
         {br_level; br_cycle; br_priority; br_depth} :: acc
       | _ -> acc) [] rows

let endorser_rights_from_db_list rows =
  List.rev @@ List.fold_left
    (fun acc x -> match x with
       | ( pr_level, pr_cycle, Some pr_nslot, Some pr_depth) ->
         let er_level = Int64.to_int pr_level in
         let er_cycle = Int64.to_int pr_cycle in
         let er_nslot = Int32.to_int pr_nslot in
         let er_depth = Int64.to_int pr_depth in
         {er_level; er_cycle; er_nslot; er_depth} :: acc
       | _ -> acc) [] rows

let service_from_db
    (srv_name, dn1, logos, srv_url, srv_descr, srv_kind, tsp, srv_page,
     srv_delegations_page, srv_account_page, addresses, aliases,
     srv_display_delegation_page, srv_display_account_page) =
  let srv_dn1 = if dn1 = "" then None else Some dn1 in
  let srv_logo = Misc.unopt "" (Misc.unopt None (List.nth_opt logos 0)) in
  let srv_logo2 = Misc.unopt None (List.nth_opt logos 1) in
  let srv_logo_payout = Misc.unopt None (List.nth_opt logos 2) in
  let srv_sponsored = Misc.unoptf None
      (fun tsp -> Some (Date.to_string @@ date_of_cal tsp)) tsp in
  let srv_aliases = match addresses, aliases with
    | Some addresses, Some aliases ->
      Some (
        List.map2 (fun dn alias ->
            let dn = Misc.unopt "" dn in {dn; alias}) addresses aliases)
    | _ -> None in
  let srv_descr = if srv_descr = Some "" then None else srv_descr in
  let srv_page = if srv_page = Some "" then None else srv_page in
  {srv_kind; srv_dn1; srv_name; srv_url; srv_logo; srv_logo2; srv_logo_payout;
   srv_descr; srv_sponsored; srv_page; srv_delegations_page; srv_account_page;
   srv_aliases; srv_display_delegation_page; srv_display_account_page}

let constants_from_db
    (level_start, _level_end, _protocol, _block_hash, _distance_level,
     proof_of_work_nonce_size, nonce_length, max_revelations_per_block,
     max_operation_data_length, preserved_cycles, blocks_per_cycle,
     blocks_per_commitment, blocks_per_roll_snapshot,
     blocks_per_voting_period, time_between_blocks, endorsers_per_block,
     hard_gas_limit_per_operation, hard_gas_limit_per_block,
     proof_of_work_threshold, tokens_per_roll, michelson_maximum_type_size,
     seed_nonce_revelation_tip, origination_burn, block_security_deposit,
     endorsement_security_deposit, block_reward, endorsement_reward,
     cost_per_byte, hard_storage_limit_per_operation, max_proposals_per_delegate,
     test_chain_duration, hard_gas_limit_to_pay_fees, max_operation_ttl,
     protocol_revision) =
  let proof_of_work_nonce_size = Int32.to_int proof_of_work_nonce_size in
  let nonce_length = Int32.to_int nonce_length in
  let max_revelations_per_block = Int32.to_int max_revelations_per_block in
  let max_operation_data_length = Int32.to_int max_operation_data_length in
  let preserved_cycles = Int32.to_int preserved_cycles in
  let blocks_per_cycle = Int32.to_int blocks_per_cycle in
  let blocks_per_commitment = Int32.to_int blocks_per_commitment in
  let blocks_per_roll_snapshot = Int32.to_int blocks_per_roll_snapshot in
  let blocks_per_voting_period = Int32.to_int blocks_per_voting_period in
  let endorsers_per_block = Int32.to_int endorsers_per_block in
  let michelson_maximum_type_size = Int32.to_int michelson_maximum_type_size in
  let max_proposals_per_delegate = convert_opt Int32.to_int max_proposals_per_delegate in
  let time_between_blocks = List.rev @@ List.fold_left (fun acc i -> match i with
      | None -> acc | Some i -> (Int32.to_int i) :: acc) [] time_between_blocks in
  let max_operation_ttl = Int32.to_int max_operation_ttl in
  let protocol_revision = convert_opt Int32.to_int protocol_revision in
  (Int64.to_int level_start,
   { Dune_types.proof_of_work_nonce_size; nonce_length; max_revelations_per_block;
     max_operation_data_length; preserved_cycles; blocks_per_cycle;
     blocks_per_commitment; blocks_per_roll_snapshot;
     blocks_per_voting_period; time_between_blocks; endorsers_per_block;
     hard_gas_limit_per_operation; hard_gas_limit_per_block;
     proof_of_work_threshold; tokens_per_roll; michelson_maximum_type_size;
     seed_nonce_revelation_tip; origination_burn; block_security_deposit;
     endorsement_security_deposit; block_reward; endorsement_reward;
     cost_per_byte; hard_storage_limit_per_operation;
     max_proposals_per_delegate; test_chain_duration;
     hard_gas_limit_to_pay_fees; max_operation_ttl;
     protocol_revision})

let merge_constants_params ?constants params level f =
  let open Dune_types in
  match constants, params, level with
  | Some constants, Some params, Some level ->
    constants level (fun cst ->
        let choose p c = match p with None -> c | Some p -> p in
        let choose_opt p c = match p with None -> c | Some p -> Some p in
        let constants = {
          proof_of_work_nonce_size =
            choose params.proof_of_work_nonce_size_opt cst.proof_of_work_nonce_size;
          nonce_length =
            choose params.nonce_length_opt cst.nonce_length;
          max_revelations_per_block =
            choose params.max_revelations_per_block_opt cst.max_revelations_per_block;
          max_operation_data_length =
            choose params.max_operation_data_length_opt cst.max_operation_data_length;
          preserved_cycles =
            choose params.preserved_cycles_opt cst.preserved_cycles;
          blocks_per_cycle =
            choose params.blocks_per_cycle_opt cst.blocks_per_cycle;
          blocks_per_commitment =
            choose params.blocks_per_commitment_opt cst.blocks_per_commitment;
          blocks_per_roll_snapshot =
            choose params.blocks_per_roll_snapshot_opt cst.blocks_per_roll_snapshot;
          blocks_per_voting_period =
            choose params.blocks_per_voting_period_opt cst.blocks_per_voting_period;
          time_between_blocks =
            choose params.time_between_blocks_opt cst.time_between_blocks;
          endorsers_per_block =
            choose params.endorsers_per_block_opt cst.endorsers_per_block;
          hard_gas_limit_per_operation =
            choose params.hard_gas_limit_per_operation_opt cst.hard_gas_limit_per_operation;
          hard_gas_limit_per_block =
            choose params.hard_gas_limit_per_block_opt cst.hard_gas_limit_per_block;
          proof_of_work_threshold =
            choose params.proof_of_work_threshold_opt cst.proof_of_work_threshold;
          tokens_per_roll =
            choose params.tokens_per_roll_opt cst.tokens_per_roll;
          michelson_maximum_type_size =
            choose params.michelson_maximum_type_size_opt cst.michelson_maximum_type_size;
          seed_nonce_revelation_tip =
            choose params.seed_nonce_revelation_tip_opt cst.seed_nonce_revelation_tip;
          origination_burn = (match params.origination_size_opt with
              | None -> cst.origination_burn
              | Some p -> Int64.(mul (of_int p) 1_000L));
          block_security_deposit =
            choose params.block_security_deposit_opt cst.block_security_deposit;
          endorsement_security_deposit =
            choose params.endorsement_security_deposit_opt cst.endorsement_security_deposit;
          block_reward =
            choose params.block_reward_opt cst.block_reward;
          endorsement_reward =
            choose params.endorsement_reward_opt cst.endorsement_reward;
          cost_per_byte =
            choose params.cost_per_byte_opt cst.cost_per_byte;
          hard_storage_limit_per_operation =
            choose params.hard_storage_limit_per_operation_opt cst.hard_storage_limit_per_operation;
          max_proposals_per_delegate =
            choose_opt params.max_proposals_per_delegate_opt cst.max_proposals_per_delegate;
          test_chain_duration = cst.test_chain_duration;
          hard_gas_limit_to_pay_fees =
            choose params.hard_gas_limit_to_pay_fees_opt cst.hard_gas_limit_to_pay_fees;
          max_operation_ttl =
            choose params.max_operation_ttl_opt cst.max_operation_ttl;
          protocol_revision = params.protocol_revision_opt } in
        f constants)
  | _ -> ()

let batch_operation rows mk_op mk_ops =
  let grouped =
    List.rev @@ List.fold_left
      (fun acc x ->
         let hash, block, network, op = mk_op x in
         match List.assoc_opt hash acc with
         | Some (block, network, ops) ->
           (hash, (block, network, op :: ops)) :: (List.remove_assoc hash acc)
         | None -> (hash, (block, network, [ op ] )) :: acc)
      [] rows in
  List.map (fun (op_hash, (op_block_hash, op_network_hash, ops)) ->
      { op_hash; op_block_hash; op_network_hash;
        op_type = mk_ops (List.rev ops) }) grouped

let manager_batch_operation rows mk_op =
  let grouped =
    List.rev @@ List.fold_left
      (fun acc x ->
         let hash, block, network, src, op = mk_op x in
         match List.assoc_opt hash acc with
         | Some (block, network, src, ops) ->
           (hash, (block, network, src, op :: ops)) :: (List.remove_assoc hash acc)
         | None -> (hash, (block, network, src, [ op ] )) :: acc)
      [] rows in
  List.map (fun (op_hash, (op_block_hash, op_network_hash, src, ops)) ->
      { op_hash; op_block_hash; op_network_hash;
        op_type = Sourced (Manager ("manager", Alias.to_name src, List.rev ops)) }) grouped

(* pending operations *)
let pending_info_from_db pe_hash pe_branch pe_status pe_tsp pe_errors =
  let pe_tsp = string_of_cal pe_tsp in
  let pe_errors = errors_from_db pe_errors in
  {pe_hash; pe_branch; pe_status; pe_tsp; pe_errors}
let pending_manager_from_db pe_src pe_counter pe_fee pe_gas_limit pe_storage_limit =
  let pe_src = Alias.to_name pe_src in
  let pe_gas_limit = Misc.convopt Z.of_int64 pe_gas_limit in
  let pe_storage_limit = Misc.convopt Z.of_int64 pe_storage_limit in
  {pe_src; pe_counter; pe_fee; pe_gas_limit; pe_storage_limit}
let pending_seed_nonce_revelation_from_db
    (hash, branch, status, tsp, errors, pe_seed_level, pe_seed_nonce) =
  let pe_seed_level = Int64.to_int pe_seed_level in
  PSeed_nonce_revelation
    {pe_seed_info = pending_info_from_db hash branch status tsp errors;
     pe_seed_level; pe_seed_nonce}
let pending_activation_from_db
    (hash, branch, status, tsp, errors, pe_act_pkh, pe_act_secret) =
  let pe_act_pkh = Alias.to_name pe_act_pkh in
  PActivation
    {pe_act_info = pending_info_from_db hash branch status tsp errors;
     pe_act_pkh; pe_act_secret}
let pending_endorsement_from_db
    (hash, branch, status, tsp, errors, pe_end_level, pe_end_prio) =
  let pe_end_level = Int64.to_int pe_end_level in
  let pe_end_prio = Misc.convopt Int32.to_int pe_end_prio in
  PEndorsement
    {pe_end_info = pending_info_from_db hash branch status tsp errors;
     pe_end_level; pe_end_prio}
let pending_transaction_from_db
    (hash, branch, status, tsp, errors, src, counter, fee, gas_limit, storage_limit,
     pe_tr_dst, pe_tr_amount, pe_tr_parameters, pe_tr_collect_fee_gas,
     pe_tr_collect_pk) =
  let pe_tr_dst = Alias.to_name pe_tr_dst in
  PTransaction
    {pe_tr_info = pending_info_from_db hash branch status tsp errors;
     pe_tr_man = pending_manager_from_db src counter fee gas_limit storage_limit;
     pe_tr_dst; pe_tr_amount; pe_tr_parameters; pe_tr_collect_fee_gas;
     pe_tr_collect_pk}
let pending_origination_from_db
    (hash, branch, status, tsp, errors, src, counter, fee, gas_limit, storage_limit,
     pe_or_kt1, pe_or_manager, pe_or_delegate, sc_code, sc_storage, sc_code_hash,
     pe_or_spendable, pe_or_delegatable, pe_or_balance) =
  let pe_or_script = or_script_from_db sc_code sc_code_hash sc_storage in
  let pe_or_kt1 = Alias.to_name pe_or_kt1 in
  let pe_or_manager = Alias.to_name pe_or_manager in
  let pe_or_delegate = Misc.convopt Alias.to_name pe_or_delegate in
  POrigination
    {pe_or_info = pending_info_from_db hash branch status tsp errors;
     pe_or_man = pending_manager_from_db src counter fee gas_limit storage_limit;
     pe_or_kt1; pe_or_manager; pe_or_delegate; pe_or_script; pe_or_spendable;
     pe_or_delegatable; pe_or_balance}
let pending_delegation_from_db
    (hash, branch, status, tsp, errors, src, counter, fee, gas_limit, storage_limit,
     pe_del_delegate) =
  let pe_del_delegate = Alias.to_name pe_del_delegate in
  PDelegation
    {pe_del_info = pending_info_from_db hash branch status tsp errors;
     pe_del_man = pending_manager_from_db src counter fee gas_limit storage_limit;
     pe_del_delegate}
let pending_reveal_from_db
    (hash, branch, status, tsp, errors, src, counter, fee, gas_limit, storage_limit,
     pe_rvl_pubkey) =
  PReveal
    {pe_rvl_info = pending_info_from_db hash branch status tsp errors;
     pe_rvl_man = pending_manager_from_db src counter fee gas_limit storage_limit;
     pe_rvl_pubkey}

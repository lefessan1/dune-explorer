(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Dune_types

let debug fmt = Utils.debug !Debug_constants.debug_writer fmt

let dbh = PGOCaml.connect ~database:DunscanConfig.database ()

let () = EzPG.upgrade_database dbh ~upgrades: Pg_update.upgrades

let pg_lock_r = ref false
let pg_lock f =
  if !pg_lock_r
  then f ()
  else
    begin
      pg_lock_r := true;
      PGSQL(dbh) "BEGIN";
      f ();
      PGSQL(dbh) "COMMIT";
      pg_lock_r := false
    end

let head () =
  PGSQL(dbh) "SELECT * FROM block \
              WHERE distance_level = 0 ORDER BY level DESC LIMIT 1"
  |> Pg_helper.rows_to_option
  |> Pg_helper.omap Pg_helper.block_of_tuple_noop

let register_cycle_limits ?(force=false) cycle level_start level_end =
  match PGSQL(dbh) "SELECT cycle FROM cycle_limits WHERE cycle = $cycle" with
  | [] ->
    PGSQL(dbh)
      "INSERT INTO cycle_limits(cycle, level_start, level_end) \
       VALUES($cycle, $level_start, $level_end)"
  | _ when force ->
    PGSQL(dbh)
      "UPDATE cycle_limits SET level_start = $level_start, level_end = $level_end \
       WHERE cycle = $cycle"
  | _ -> ()

let cycle_from_level level =
  match
    PGSQL(dbh) "SELECT cycle FROM cycle_limits \
                WHERE $level BETWEEN level_start AND level_end" with
  | cycle :: _ -> cycle
  | _ -> assert false

let is_block_registered hash =
  let row = PGSQL(dbh) "SELECT hash FROM block WHERE hash = $hash" in
  match row with
  | [ _hash ] -> true
  | _ -> false

let register_dune_user hash =
  let insert hash contract =
    PGSQL(dbh) "INSERT INTO dune_user (hash,contract) \
                VALUES ($hash,$contract) ON CONFLICT (hash) DO NOTHING";
    PGSQL(dbh) "INSERT INTO operation_count_user(dn) VALUES($hash) \
                ON CONFLICT DO NOTHING";
    PGSQL(dbh) "INSERT INTO account_info(hash, manager) VALUES($hash, $hash) \
                ON CONFLICT DO NOTHING" in
  let update hash edpk contract =
    PGSQL(dbh) "INSERT INTO dune_user (hash,contract,edpk) \
                VALUES ($hash,$contract,$edpk) ON CONFLICT (hash) \
                DO UPDATE SET hash=$hash,contract=$contract,edpk=$edpk" in
  debug "[Writer] Registering user %s\n%!" hash ;
  if hash = "" || String.length hash = 1 then insert hash false (* HACK *)
  else
    begin match String.sub hash 0 2 with
      | "tz" | "dn" -> insert hash false
      | "TZ" | "KT" -> insert hash true
      | "ed" -> update (Crypto.pk_to_dn1 hash) hash false
      | _ -> debug "[Writer] invalid user hash %S\n%!" hash end

let register_cycle_count_baker cycle hash =
  if PGSQL(dbh) "SELECT cycle, dn FROM cycle_count_baker WHERE cycle = $cycle \
                 AND dn = $hash" = [] then
    PGSQL(dbh) "INSERT INTO cycle_count_baker(cycle, dn) VALUES($cycle, $hash)"

let update_originated_contract ?origination kt1 delegate manager spendable delegatable =
  PGSQL(dbh)
    "UPDATE account_info SET \
     delegate = $?delegate, manager = $manager, spendable = $spendable, \
     delegatable = $delegatable, origination = $?origination \
     WHERE hash = $kt1"

let register_header header =
  let shell = header.header_shell in
  let level = Int64.of_int shell.shell_level in
  let proto = Int64.of_int shell.shell_proto in
  let predecessor = shell.shell_predecessor in
  let timestamp = Pg_helper.cal_of_date_dune shell.shell_timestamp in
  let validation_pass = Int64.of_int shell.shell_validation_pass in
  let operations_hash = shell.shell_operations_hash in
  let fitness = shell.shell_fitness in
  let context = shell.shell_context in
  let priority = Int32.of_int header.header_priority in
  let seed = header.header_seed_nonce_hash in
  let nonce = header.header_proof_of_work_nonce in
  let signature = header.header_signature in
  let row =
    PGSQL(dbh)
      "SELECT * FROM header WHERE \
       level = $level AND \
       priority = $priority AND \
       fitness = $fitness AND \
       context = $context AND \
       signature = $signature" in
  match row with
  | [] ->
    PGSQL(dbh)
      "INSERT INTO header \
       (level, proto, predecessor, timestamp, validation_pass, \
       operations_hash, fitness, context, priority, \
       commited_nonce_hash, pow_nonce, signature) \
       VALUES \
       ($level, $proto, $predecessor, $timestamp, $validation_pass, \
       $operations_hash, $fitness, $context, $priority, \
       $seed, $nonce, $signature) \
       ON CONFLICT DO NOTHING"
  | _ -> ()

let register_constants ?(distance=(-1)) ?protocol ?block_hash level
    { proof_of_work_nonce_size; nonce_length; max_revelations_per_block;
      max_operation_data_length; preserved_cycles; blocks_per_cycle;
      blocks_per_commitment; blocks_per_roll_snapshot;
      blocks_per_voting_period; time_between_blocks; endorsers_per_block;
      hard_gas_limit_per_operation; hard_gas_limit_per_block;
      proof_of_work_threshold; tokens_per_roll; michelson_maximum_type_size;
      seed_nonce_revelation_tip; origination_burn; block_security_deposit;
      endorsement_security_deposit; block_reward; endorsement_reward;
      cost_per_byte; hard_storage_limit_per_operation;
      max_proposals_per_delegate; test_chain_duration;
      hard_gas_limit_to_pay_fees; max_operation_ttl; protocol_revision } =
  match block_hash with
  | Some block_hash ->
    let distance = Int32.of_int distance in
    let level = Int64.of_int level in
    let proof_of_work_nonce_size = Int32.of_int proof_of_work_nonce_size in
    let nonce_length = Int32.of_int nonce_length in
    let max_revelations_per_block = Int32.of_int max_revelations_per_block in
    let max_operation_data_length = Int32.of_int max_operation_data_length in
    let preserved_cycles = Int32.of_int preserved_cycles in
    let blocks_per_cycle = Int32.of_int blocks_per_cycle in
    let blocks_per_commitment = Int32.of_int blocks_per_commitment in
    let blocks_per_roll_snapshot = Int32.of_int blocks_per_roll_snapshot in
    let blocks_per_voting_period = Int32.of_int blocks_per_voting_period in
    let endorsers_per_block = Int32.of_int endorsers_per_block in
    let michelson_maximum_type_size = Int32.of_int michelson_maximum_type_size in
    let max_proposals_per_delegate = Pg_helper.convert_opt Int32.of_int max_proposals_per_delegate in
    let time_between_blocks = List.map (fun i -> Some (Int32.of_int i)) time_between_blocks in
    let max_operation_ttl = Int32.of_int max_operation_ttl in
    let protocol_revision = Pg_helper.convert_opt Int32.of_int protocol_revision in
    PGSQL(dbh)
      "INSERT INTO protocol_constants ( \
       level_start, protocol, block_hash, distance_level, \
       proof_of_work_nonce_size, nonce_length, \
       max_revelations_per_block, max_operation_data_length, preserved_cycles, \
       blocks_per_cycle, blocks_per_commitment, blocks_per_roll_snapshot, \
       blocks_per_voting_period, time_between_blocks, endorsers_per_block, \
       hard_gas_limit_per_operation, hard_gas_limit_per_block, proof_of_work_threshold, \
       tokens_per_roll, michelson_maximum_type_size, seed_nonce_revelation_tip, \
       origination_burn, block_security_deposit, endorsement_security_deposit, \
       block_reward, endorsement_reward, cost_per_byte, hard_storage_limit_per_operation, \
       max_proposals_per_delegate, test_chain_duration, hard_gas_limit_to_pay_fees, \
       max_operation_ttl, protocol_revision) \
       VALUES($level, $?protocol, $block_hash, $distance, \
       $proof_of_work_nonce_size, $nonce_length, \
       $max_revelations_per_block, $max_operation_data_length, \
       $preserved_cycles, $blocks_per_cycle, $blocks_per_commitment, \
       $blocks_per_roll_snapshot, $blocks_per_voting_period, \
       $time_between_blocks, $endorsers_per_block, \
       $hard_gas_limit_per_operation, $hard_gas_limit_per_block, \
       $proof_of_work_threshold, $tokens_per_roll, \
       $michelson_maximum_type_size, $seed_nonce_revelation_tip, \
       $origination_burn, $block_security_deposit, \
       $endorsement_security_deposit, $block_reward, $endorsement_reward, \
       $cost_per_byte, $hard_storage_limit_per_operation, \
       $?max_proposals_per_delegate, $?test_chain_duration, \
       $hard_gas_limit_to_pay_fees, $max_operation_ttl, $?protocol_revision) \
       ON CONFLICT DO NOTHING"
  | _ -> ()

let register_protocol ?(distance=(-1L)) ?constants ?block_hash level proto =
  let new_proto =
    match PGSQL(dbh) "SELECT hash FROM protocol WHERE hash = $proto" with
    | [] -> true
    | _ -> false in
  if new_proto then (
    PGSQL(dbh) "INSERT INTO protocol (hash, name) VALUES ($proto, $proto)";
    (match PGSQL(dbh) "SELECT level_start FROM protocol_constants \
                       WHERE distance_level = 0 \
                       ORDER BY level_start DESC LIMIT 1" with
    | [] -> ()
    | previous_level_start :: _ ->
      let level = Int64.of_int level in
      PGSQL(dbh) "UPDATE protocol_constants SET level_end = $level - 1::bigint \
                  WHERE level_start = $previous_level_start AND distance_level = 0");
    match constants with
    | None -> ()
    | Some constants ->
      constants level (fun constants ->
          let distance = Int64.to_int distance in
          register_constants ~distance ~protocol:proto ?block_hash level constants))

(* blocks *)

let register_block ?constants block level distance operation_count volume fees =
  let hash = block.node_hash in
  let header = block.node_header in
  let shell = header.header_shell in
  let predecessor_hash = shell.shell_predecessor in
  let fitness = shell.shell_fitness in
  let baker = block.node_metadata.meta_header.header_meta_baker in
  let timestamp = Pg_helper.cal_of_date_dune shell.shell_timestamp in
  let validation_pass = shell.shell_validation_pass in
  let protocol = block.node_protocol in
  let test_protocol = block.node_protocol in
  let network = block.node_chain_id in
  let test_network, test_network_expiration =
    match block.node_metadata.meta_test_chain_status with
    | CSNot_running -> "Not running", "Not running"
    | CSForking { protocol ; expiration } ->
      Printf.sprintf "Forking %S" protocol,
      expiration
    | CSRunning { chain_id ; genesis ; protocol ; expiration } ->
      Printf.sprintf "Running [%S][%S][%S]" chain_id genesis protocol,
      expiration in
  let priority  = Int32.of_int header.header_priority  in
  let commited_nonce_hash  = header.header_seed_nonce_hash in
  let pow_nonce = header.header_proof_of_work_nonce in
  let proto = shell.shell_proto in
  let data = "--" in            (* TODO remove data field *)
  let signature = header.header_signature in
  let protocol_hash = protocol in
  let test_protocol_hash = protocol in
  let validation_pass = Int64.of_int validation_pass in
  let proto = Int64.of_int proto in
  let lvl_level = Int64.of_int level.node_lvl_level in
  let lvl_level_position = Int64.of_int level.node_lvl_level_position in
  let lvl_cycle = Int64.of_int level.node_lvl_cycle in
  let lvl_cycle_position = Int64.of_int level.node_lvl_cycle_position in
  let lvl_voting_period = Int64.of_int level.node_lvl_voting_period in
  let lvl_voting_period_position = Int64.of_int level.node_lvl_voting_period_position in
  let voting_period_kind =
    Dune_utils.string_of_voting_period_kind
      block.node_metadata.meta_header.header_meta_voting_period_kind in
  (match lvl_cycle_position, constants with
   | (0L, Some constants) ->
     constants level.node_lvl_level (fun cst ->
         let blocks_per_cycle = Int64.of_int cst.blocks_per_cycle in
         let level_end = Int64.(pred (add lvl_level blocks_per_cycle)) in
         register_cycle_limits ~force:true lvl_cycle lvl_level level_end)
   | _ -> ());
  register_dune_user baker ;
  register_cycle_count_baker lvl_cycle baker;
  register_protocol ~distance ?constants ~block_hash:hash level.node_lvl_level protocol ;
  register_protocol ~distance ~block_hash:hash level.node_lvl_level test_protocol;
  PGSQL(dbh)
    "INSERT INTO block \
     (hash, predecessor, fitness, baker, timestamp, validation_pass, \
     proto, data, signature, \
     protocol, test_protocol, \
     network, test_network, test_network_expiration, level, level_position, \
     priority, cycle, cycle_position, voting_period, voting_period_position, \
     commited_nonce_hash, pow_nonce, distance_level, operation_count, \
     volume, fees, voting_period_kind) \
     VALUES \
     ($hash, $predecessor_hash, $fitness, $baker, $timestamp, $validation_pass, \
     $proto, $data, $signature, \
     $protocol_hash, \
     $test_protocol_hash, $network, $test_network, $test_network_expiration, \
     $lvl_level, $lvl_level_position, $priority, $lvl_cycle, \
     $lvl_cycle_position, $lvl_voting_period, $lvl_voting_period_position, \
     $commited_nonce_hash, $pow_nonce, $distance, $operation_count, \
     $volume, $fees, $voting_period_kind) \
     ON CONFLICT (hash) DO NOTHING";
  (* factor 2 is to take some space for endorsements and transactions of alternative
     chains that we don't read in operation_recent *)
  let endorsement_cut = match PGSQL(dbh) "SELECT MAX(id) FROM endorsement_last" with
    | [ Some i ] -> Int64.(sub i (mul 2L Pg_update.Constants.last_endorsements))
    | _ -> 0L in
  let transaction_cut = match PGSQL(dbh) "SELECT MAX(id) FROM transaction_last" with
    | [ Some i ] -> Int64.(sub i (mul 2L Pg_update.Constants.last_transactions))
    | _ -> 0L in
  let origination_cut = match PGSQL(dbh) "SELECT MAX(id) FROM origination_last" with
    | [ Some i ] -> Int64.(sub i (mul 2L Pg_update.Constants.last_originations))
    | _ -> 0L in
  let reveal_cut = match PGSQL(dbh) "SELECT MAX(id) FROM reveal_last" with
    | [ Some i ] -> Int64.(sub i (mul 2L Pg_update.Constants.last_reveals))
    | _ -> 0L in
  let delegation_cut = match PGSQL(dbh) "SELECT MAX(id) FROM delegation_last" with
    | [ Some i ] -> Int64.(sub i (mul 2L Pg_update.Constants.last_delegations))
    | _ -> 0L in
  let activation_cut = match PGSQL(dbh) "SELECT MAX(id) FROM activation_last" with
    | [ Some i ] -> Int64.(sub i (mul 2L Pg_update.Constants.last_activations))
    | _ -> 0L in
  PGSQL(dbh) "DELETE FROM endorsement_last WHERE id < $endorsement_cut";
  PGSQL(dbh) "DELETE FROM transaction_last WHERE id < $transaction_cut";
  PGSQL(dbh) "DELETE FROM origination_last WHERE id < $origination_cut";
  PGSQL(dbh) "DELETE FROM delegation_last WHERE id < $delegation_cut";
  PGSQL(dbh) "DELETE FROM activation_last WHERE id < $activation_cut";
  PGSQL(dbh) "DELETE FROM reveal_last WHERE id < $reveal_cut"

let register_genesis genesis =
  let dummy_level = {
    node_lvl_level = 0 ;
    node_lvl_level_position = 0 ;
    node_lvl_cycle = 0 ;
    node_lvl_cycle_position = 0 ;
    node_lvl_voting_period = 0 ;
    node_lvl_voting_period_position = 0 ;
    node_lvl_expected_commitment = false ;
  } in
  let orphan = { genesis with
                 node_hash = Utils.orphan_block_hash;
                 node_header = {
                   genesis.node_header with
                   header_shell = {
                     genesis.node_header.header_shell with
                     shell_predecessor = Utils.orphan_block_hash } } } in
  (* Special user for genesis and first block *)
  PGSQL(dbh) "INSERT INTO dune_user (hash,contract) \
              VALUES ('God','false')";
  register_block genesis dummy_level 0L 0L 0L 0L;
  register_block orphan dummy_level Int64.minus_one 0L 0L 0L

(* operations *)

let get_status_from_manager_metadata = function
  | None -> false (* Pending *)
  | Some meta ->
    match meta.manager_meta_operation_result with
    | None -> assert false
    | Some meta ->
      match meta.meta_op_status with
      | None -> assert false
      | Some status -> status <> "applied"

let get_burn = function
  | None -> 0L
  | Some meta -> match meta.manager_meta_operation_result with
    | None -> 0L
    | Some meta ->
      match meta.meta_op_balance_updates with
      | None -> 0L
      | Some l_bu ->
        Int64.neg @@
        List.fold_left (fun acc bu -> match bu with
            | Contract (_, change) -> Int64.add acc change
            | _ -> acc) 0L l_bu

let get_internal_burn = function
  | None -> 0L
  | Some meta ->
    List.fold_left (fun acc -> function
        | NTransaction transaction ->
          Int64.add acc (get_burn transaction.node_tr_metadata)
        | NOrigination origination ->
          Int64.add acc (get_burn origination.node_or_metadata)
        | _ -> acc)
      0L meta.manager_meta_internal_operation_results

let get_total_burn meta = Int64.add (get_burn meta) (get_internal_burn meta)

let format_error {node_err_id; node_err_kind; node_err_info} =
  [ Some node_err_id; Some node_err_kind; Some node_err_info ]

let format_errors l = List.flatten @@ List.map format_error l

let get_manager_errors = function
  | None -> None
  | Some m -> match m.manager_meta_operation_result with
    | None -> None
    | Some m -> Some (format_errors m.meta_op_errors)

let get_denounciation_info meta =
  match meta with
  | None -> "", "", 0L, 0L, 0L, 0L
  | Some metadata ->
    let balance_updates =
      match metadata.meta_op_balance_updates with None -> [] | Some l -> l in
    List.fold_left
      (fun (accused, denouncer, lost_deposit,
            lost_rewards, lost_fees, gain_rewards) b ->
        match b with
        (* | Debited _ | Credited _ *)
        | Contract _ ->
          accused, denouncer, lost_deposit,
          lost_rewards, lost_fees, gain_rewards
        | Rewards (dn1, _cycle, amount) ->
          if amount < 0L then (* accused *)
            (dn1, denouncer,
             lost_deposit, amount, lost_fees, gain_rewards)
          else      (* denouncer *)
            (accused, dn1,
             lost_deposit, lost_rewards, lost_fees, amount)
        | Fees (dn1, _cycle, fees) ->
          (dn1, denouncer,
           lost_deposit, lost_rewards, fees, gain_rewards)
        | Deposits (dn1, _cycle, amount) ->
          (dn1, denouncer,
           amount, lost_rewards, lost_fees, gain_rewards))
      ("","", 0L, 0L, 0L, 0L) balance_updates

let get_header_id header =
  let h1_level =
    Int64.of_int header.header_shell.shell_level in
  let h1_priority = Int32.of_int header.header_priority in
  let h1_fitness = header.header_shell.shell_fitness in
  let h1_context = header.header_shell.shell_context in
  let h1_signature = header.header_signature in
  match
    PGSQL(dbh)
      "SELECT id FROM header AS h \
       WHERE \
       h.level = $h1_level AND \
       h.priority = $h1_priority AND \
       h.fitness = $h1_fitness AND \
       h.context = $h1_context AND \
       h.signature = $h1_signature" with
  | [ id ] -> Int64.to_int32 id
  | _ -> debug "[Writer] Header not found\n%!"; assert false


let register_token_account ?source tsp account contract =
  let source = Misc.unopt account source in
  PGSQL(dbh)
    "INSERT INTO token_balances(account, contract, source, last_modified) \
     VALUES($account, $contract, $source, $tsp) \
     ON CONFLICT DO NOTHING"

let format_big_map_diff accounts = function
  | None -> [], [], []
  | Some meta -> match meta.manager_meta_operation_result with
    | None -> [], [], []
    | Some meta -> match meta.meta_op_big_map_diff with
      | None -> [], [], []
      | Some l ->
        List.fold_left (fun (acc1, acc2, acc3) (_key_hash, key, value) ->
            match key, value with
            | Bytes h, Some (Prim ("Pair", [Int balance; Seq l] , _)) ->
              let account = Crypto.hex_to_account h in
              let m1, m2, m3 = List.fold_left (fun (acc01, acc02, acc03) x -> match x with
                  | Prim ("Elt", [Bytes h2; Int v2], _) ->
                    let account2 = Crypto.hex_to_account h2 in
                    if List.mem account2 accounts then
                      Some account2 :: acc01, Some account :: acc02, Some (Z.to_string v2) :: acc03
                    else
                      acc01, acc02, acc03
                  | _ -> acc01, acc02, acc03) ([], [], []) l in
              Some account :: m1 @ acc1, Some account :: m2 @ acc2,
              Some (Z.to_string balance) :: m3 @ acc3
            | _ -> acc1, acc2, acc3) ([], [], []) l

let register_token_operation hash tsp level block_hash network transaction =
  let open Data_types in
  let caller = transaction.node_tr_src in
  let contract = transaction.node_tr_dst in
  let level = Int64.of_int level in
  match transaction.node_tr_parameters with
  | None -> ()
  | Some parameters ->
    let token_op = Token_standard.decode caller parameters in
    let kind = Token_standard.token_op_to_str token_op in
    match token_op with
    | TS_Unknown | TS_Kyc _ -> ()
    | TS_Transfer {ts_dst; ts_amount; ts_flag; _} ->
      debug "[Writer] transfer %s\n%!" hash;
      let dst = ts_dst.dn in
      let ts_amount = Z.to_string ts_amount in
      let bl_accounts, bl_sources, balances =
        format_big_map_diff [dst; caller] transaction.node_tr_metadata in
      register_token_account tsp caller contract;
      register_token_account tsp dst contract;
      PGSQL(dbh)
        "INSERT INTO token_operations( \
         transaction, contract, kind, source, destination, amount, flag, \
         balances, balance_accounts, balance_sources, \
         op_level, op_block_hash, timestamp, network) \
         VALUES($hash, $contract, $kind, $caller, $dst, $ts_amount, $ts_flag, \
         $balances, $bl_accounts, $bl_sources, \
         $level, $block_hash, $tsp, $network) ON CONFLICT DO NOTHING"
    | TS_TransferFrom {ts_src; ts_dst; ts_amount; ts_flag; _} ->
      debug "[Writer] transferFrom %s\n%!" hash;
      let src = ts_src.dn in
      let dst = ts_dst.dn in
      let ts_amount = Z.to_string ts_amount in
      let bl_accounts, bl_sources, balances =
        format_big_map_diff [dst; caller] transaction.node_tr_metadata in
      register_token_account tsp src contract;
      register_token_account tsp dst contract;
      register_token_account ~source:src tsp caller contract;
      PGSQL(dbh)
        "INSERT INTO token_operations(\
         transaction, contract, kind, source, destination, amount, flag, \
         balances, balance_accounts, balance_sources, \
         op_level, op_block_hash, timestamp, network, caller) \
         VALUES($hash, $contract, $kind, $src, $dst, $ts_amount, $ts_flag, \
         $balances, $bl_accounts, $bl_sources, \
         $level, $block_hash, $tsp, $network, $caller) ON CONFLICT DO NOTHING"
    | TS_Approve {ts_dst; ts_amount; ts_flag; _} ->
      debug "[Writer] approve %s\n%!" hash;
      let dst = ts_dst.dn in
      let ts_amount = Z.to_string ts_amount in
      let bl_accounts, bl_sources, balances =
        format_big_map_diff [dst; caller] transaction.node_tr_metadata in
      register_token_account tsp caller contract;
      register_token_account ~source:caller tsp dst contract;
      PGSQL(dbh)
        "INSERT INTO token_operations(\
         transaction, contract, kind, source, destination, amount, flag, \
         balances, balance_accounts, balance_sources, \
         op_level, op_block_hash, timestamp, network) \
         VALUES($hash, $contract, $kind, $caller, $dst, $ts_amount, $ts_flag, \
         $balances, $bl_accounts, $bl_sources, \
         $level, $block_hash, $tsp, $network) ON CONFLICT DO NOTHING"

let block_info block =
  Int64.of_int block.node_header.header_shell.shell_level,
  block.node_hash, block.node_chain_id,
  Pg_helper.cal_of_date_dune block.node_header.header_shell.shell_timestamp,
  block.node_metadata.meta_header.header_meta_baker,
  block.node_header.header_priority

let register_seed_nonce_revelation ?block hash tsp snr =
  let level = Int64.of_int snr.node_seed_level in
  let nonce = snr.node_seed_nonce in
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, baker, _ = block_info block in
    PGSQL(dbh)
      "INSERT INTO seed_nonce_revelation_all \
       (hash, level, nonce, timestamp_op, op_level, op_block_hash, \
       distance_level, network, timestamp_block, baker) \
       VALUES($hash, $level, $nonce, $tsp, $op_level, \
       $op_block_hash, -1, $network, $timestamp_block, $baker)"
  | None -> ()

let register_double_baking_evidence ?block hash dbe =
  register_header dbe.node_double_bh1 ;
  register_header dbe.node_double_bh2 ;
  let h1 = get_header_id dbe.node_double_bh1 in
  let h2 = get_header_id dbe.node_double_bh2 in
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let op_cycle = cycle_from_level op_level in
    let accused, denouncer, lost_deposit,
        lost_rewards, lost_fees, gain_rewards =
      get_denounciation_info dbe.node_double_bh_metadata in
    PGSQL(dbh)
      "INSERT INTO double_baking_evidence_all \
       (hash, header1, header2, accused, denouncer, lost_deposit, lost_rewards, \
       lost_fees, gain_rewards, op_level, op_cycle, op_block_hash, \
       distance_level, network, timestamp) \
       VALUES($hash, $h1, $h2, $accused, $denouncer, $lost_deposit, $lost_rewards, \
       $lost_fees, $gain_rewards, $op_level, $op_cycle, \
       $op_block_hash, -1, $network, $timestamp_block)"
  | None -> ()

let register_double_endorsement_evidence ?block hash dee =
  let endorse1 = dee.node_double_endorsement1 in
  let endorse2 = dee.node_double_endorsement2 in
  let block1 = endorse1.ndee_branch in (* TODO *)
  let level1 = Int64.of_int endorse1.ndee_level in
  let block2 = endorse2.ndee_branch in
  let level2 = Int64.of_int endorse2.ndee_level in
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let op_cycle = cycle_from_level op_level in
    let accused, denouncer, lost_deposit,
        lost_rewards, lost_fees, gain_rewards =
      get_denounciation_info dee.node_double_endorsement_metadata in
    PGSQL(dbh)
      "INSERT INTO double_endorsement_evidence_all \
       (hash, block_hash1, level1, block_hash2, level2, accused, denouncer, \
       lost_deposit, lost_rewards, lost_fees, gain_rewards, op_level, \
       op_cycle, op_block_hash, distance_level, network, timestamp) \
       VALUES($hash, $block1, $level1, $block2, $level2, $accused, $denouncer, \
       $lost_deposit, $lost_rewards, $lost_fees, $gain_rewards, $op_level, \
       $op_cycle, $op_block_hash, -1, $network, $timestamp_block)"
  | None -> ()

let register_activation ?block hash tsp act =
  let pkh = act.node_act_pkh in
  let secret = act.node_act_secret in
  register_dune_user pkh;
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let balance = match act.node_act_metadata with
      | None -> None
      | Some meta -> match meta.meta_op_balance_updates with
        | None -> Some 0L
        | Some [ bu ] ->  begin match bu with
            | Contract (acc, amount) when acc = pkh -> Some amount
            | _ -> debug "[Writer] Weird balance_updates type for activation %S\n%!" hash;
              Some 0L
          end
        | Some l ->
          debug "[Writer] Unexpected balance_updates length for activation %S - %S\n%!"
            hash @@ String.concat " " (List.map Dune_utils.string_of_balance_update l);
          None in
    PGSQL(dbh)
      "INSERT INTO activation_last \
       (hash, pkh, secret, balance, timestamp_op, op_level, op_block_hash, \
       distance_level, network, timestamp_block) \
       VALUES($hash, $pkh, $secret, $?balance, $tsp, $op_level, \
       $op_block_hash, -1, $network, $timestamp_block)";
    PGSQL(dbh)
      "INSERT INTO activation_all \
       (hash, pkh, secret, balance, timestamp_op, op_level, op_block_hash, \
       distance_level, network, timestamp_block) \
       VALUES($hash, $pkh, $secret, $?balance, $tsp, $op_level, \
       $op_block_hash, -1, $network, $timestamp_block)"
  | None -> ()

let register_endorsement ?block ?branch hash endo =
  match block, branch with
  | Some block, Some branch ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let op_cycle = cycle_from_level op_level in
    let level = Int64.of_int endo.node_endorse_block_level in
    let priority =
      match PGSQL(dbh) "SELECT priority FROM block WHERE hash = $branch" with
      | [ p ] -> p
      | _ -> assert false in
    let src, slots =
      match endo.node_endorse_metadata with
      | None -> "", []
      | Some metadata ->
        Misc.unopt "" metadata.meta_op_delegate,
        Misc.unoptf [] (List.map (fun s -> Some (Int32.of_int s)))
          metadata.meta_op_slots in
    register_dune_user src;
    register_cycle_count_baker op_cycle src;
    PGSQL(dbh)
      "INSERT INTO endorsement_last \
       (hash, source, block_hash, block_level, slots, priority, \
       op_block_hash, op_level, op_cycle, distance_level, network, timestamp) \
       VALUES \
       ($hash, $src, $branch, $level, $slots, $priority, \
       $op_block_hash, $op_level, $op_cycle, -1, $network, $timestamp_block) \
       ON CONFLICT DO NOTHING";
    PGSQL(dbh)
      "INSERT INTO endorsement_all \
       (hash, source, block_hash, block_level, slots, priority, \
       op_block_hash, op_level, op_cycle, distance_level, network, timestamp) \
       VALUES \
       ($hash, $src, $branch, $level, $slots, $priority, \
       $op_block_hash, $op_level, $op_cycle, -1, $network, $timestamp_block) \
       ON CONFLICT DO NOTHING"
  | _ -> ()

let register_activate_protocol ?constants ?block ?(internal=false) hash acp =
  let source = acp.node_acp_src in
  register_dune_user source;
  let counter = Z.to_int64 acp.node_acp_counter in
  let fee = acp.node_acp_fee in
  let gas_limit = Z.to_int64 acp.node_acp_gas_limit in
  let storage_limit = Z.to_int64 acp.node_acp_storage_limit in
  let level = Int32.of_int acp.node_acp_level in
  let protocol = acp.node_acp_protocol in
  let parameters = match acp.node_acp_parameters with
    | None -> None
    | Some p -> Some (
        EzEncoding.construct Dune_encoding_min.Protocol_param.encoding p) in
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let op_level_int = Some (Int64.to_int op_level) in
    let failed = get_status_from_manager_metadata acp.node_acp_metadata in
    let errors = get_manager_errors acp.node_acp_metadata in
    Pg_helper.merge_constants_params ?constants
      acp.node_acp_parameters op_level_int (fun constants ->
          register_constants ?protocol ~block_hash:op_block_hash acp.node_acp_level constants);
    PGSQL(dbh)
      "INSERT INTO activate_protocol \
       (hash, source, fee, counter,  gas_limit, storage_limit, \
       level, protocol, parameters, failed, internal, \
       op_level, op_block_hash, distance_level, \
       network, timestamp_block, errors) \
       VALUES($hash, $source, $fee, $counter, $gas_limit, \
       $storage_limit, $level, $?protocol, $?parameters, $failed, $internal, \
       $op_level, $op_block_hash, -1, $network, \
       $timestamp_block, $?errors)"
  | None -> ()

let register_manage_accounts ?block ?(internal=false) hash mac =
  let source = mac.node_mac_src in
  register_dune_user source;
  let counter = Z.to_int64 mac.node_mac_counter in
  let fee = mac.node_mac_fee in
  let gas_limit = Z.to_int64 mac.node_mac_gas_limit in
  let storage_limit = Z.to_int64 mac.node_mac_storage_limit in
  let bytes = Hex.show mac.node_mac_bytes in
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let failed = get_status_from_manager_metadata mac.node_mac_metadata in
    let errors = get_manager_errors mac.node_mac_metadata in
    PGSQL(dbh)
      "INSERT INTO manage_accounts \
       (hash, source, fee, counter,  gas_limit, storage_limit, \
       bytes, failed, internal, op_level, op_block_hash, distance_level, \
       network, timestamp_block, errors) \
       VALUES($hash, $source, $fee, $counter, $gas_limit, \
       $storage_limit, $bytes, $failed, $internal, \
       $op_level, $op_block_hash, -1, $network, \
       $timestamp_block, $?errors)"
  | None -> ()

let register_delegation ?block ?(internal=false) hash tsp dlg =
  let source = dlg.node_del_src in
  let counter = Z.to_int64 dlg.node_del_counter in
  let fee = dlg.node_del_fee in
  let gas_limit = Z.to_int64 dlg.node_del_gas_limit in
  let storage_limit = Z.to_int64 dlg.node_del_storage_limit in
  debug "[Writer]  delegation %s\n%!" hash;
  register_dune_user source;
  let delegate = dlg.node_del_delegate in
  register_dune_user delegate;
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let failed = get_status_from_manager_metadata dlg.node_del_metadata in
    let errors = get_manager_errors dlg.node_del_metadata in
    PGSQL(dbh)
      "INSERT INTO delegation_last \
       (hash, source, fee, counter, delegate, gas_limit, storage_limit, \
       failed, internal, timestamp_op, op_level, op_block_hash, distance_level, \
       network, timestamp_block, errors) \
       VALUES($hash, $source, $fee, $counter, $delegate, $gas_limit, \
       $storage_limit, $failed, $internal, $tsp, \
       $op_level, $op_block_hash, -1, $network, $timestamp_block, $?errors)";
    PGSQL(dbh)
      "INSERT INTO delegation_all \
       (hash, source, fee, counter, delegate, gas_limit, storage_limit, \
       failed, internal, timestamp_op, op_level, op_block_hash, distance_level, \
       network, timestamp_block, errors) \
       VALUES($hash, $source, $fee, $counter, $delegate, $gas_limit, \
       $storage_limit, $failed, $internal, $tsp, \
       $op_level, $op_block_hash, -1, $network, \
       $timestamp_block, $?errors)"
  | None -> ()

let register_reveal ?block ?(internal=false) hash tsp rvl =
  let source = rvl.node_rvl_src in
  let counter = Z.to_int64 rvl.node_rvl_counter in
  let fee = rvl.node_rvl_fee in
  let gas_limit = Z.to_int64 rvl.node_rvl_gas_limit in
  let storage_limit = Z.to_int64 rvl.node_rvl_storage_limit in
  debug "[Writer]  reveal %s\n%!" hash;
  register_dune_user source;
  let pubkey = rvl.node_rvl_pubkey in
  register_dune_user pubkey;
  match block with
  | Some block ->
    let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
    let failed = get_status_from_manager_metadata rvl.node_rvl_metadata in
    let errors = get_manager_errors rvl.node_rvl_metadata in
    PGSQL(dbh)
      "INSERT INTO reveal_last \
       (hash, source, fee, counter, pubkey, gas_limit, storage_limit, \
       failed, internal, timestamp_op, op_level, op_block_hash, distance_level, \
       network, timestamp_block, errors) \
       VALUES($hash, $source, $fee, $counter, $pubkey, $gas_limit, \
       $storage_limit, $failed, $internal, $tsp, \
       $op_level, $op_block_hash, -1, $network, $timestamp_block, $?errors)";
    PGSQL(dbh)
      "INSERT INTO reveal_all \
       (hash, source, fee, counter, pubkey, gas_limit, storage_limit, \
       failed, internal, timestamp_op, op_level, op_block_hash, distance_level, \
       network, timestamp_block, errors) \
       VALUES($hash, $source, $fee, $counter, $pubkey, $gas_limit, \
       $storage_limit, $failed, $internal, $tsp, \
       $op_level, $op_block_hash, -1, $network, $timestamp_block, $?errors)"
  | None -> ()

let rec register_transaction ?block ?(internal=false) hash tsp tr =
  debug "[Writer]  transaction %s\n%!" hash;
  let source = tr.node_tr_src in
  let counter = Z.to_int64 tr.node_tr_counter in
  let fee = tr.node_tr_fee in
  let gas_limit = Z.to_int64 tr.node_tr_gas_limit in
  let storage_limit = Z.to_int64 tr.node_tr_storage_limit in
  register_dune_user source;
  let dst = tr.node_tr_dst in
  register_dune_user dst;
  let amount = tr.node_tr_amount in
  let parameters = tr.node_tr_parameters in
  let collect_fee_gas = Misc.convopt Z.to_int64 tr.node_tr_collect_fee_gas in
  let collect_pk = tr.node_tr_collect_pk in
  begin match block with
    | Some block ->
      let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
      let failed = get_status_from_manager_metadata tr.node_tr_metadata in
      let errors = get_manager_errors tr.node_tr_metadata in
      let burn = if internal then 0L else get_total_burn tr.node_tr_metadata in
      if not failed then
        register_token_operation hash tsp (Int64.to_int op_level)
          op_block_hash network tr;
      PGSQL(dbh)
        "INSERT INTO transaction_all \
         (hash, source, destination, fee, counter, amount, parameters, \
         gas_limit, storage_limit, failed, internal, timestamp_op, burn_dun, \
         op_level, op_block_hash, distance_level, network, timestamp_block, \
         errors, collect_fee_gas, collect_pk) \
         VALUES \
         ($hash, $source, $dst, $fee, $counter, $amount, $?parameters, \
         $gas_limit, $storage_limit, $failed, $internal, $tsp, $burn, \
         $op_level, $op_block_hash, -1, $network, $timestamp_block, \
         $?errors, $?collect_fee_gas, $?collect_pk)";
      PGSQL(dbh)
        "INSERT INTO transaction_last \
         (hash, source, destination, fee, counter, amount, parameters, \
         gas_limit, storage_limit, failed, internal, timestamp_op, burn_dun, \
         op_level, op_block_hash, distance_level, network, timestamp_block, \
         errors, collect_fee_gas, collect_pk) \
         VALUES \
         ($hash, $source, $dst, $fee, $counter, $amount, $?parameters, \
         $gas_limit, $storage_limit, $failed, $internal, $tsp, $burn, \
         $op_level, $op_block_hash, -1, $network, $timestamp_block, \
         $?errors, $?collect_fee_gas, $?collect_pk)"
    | None -> ()
  end;
  match tr.node_tr_metadata with
  | None -> ()
  | Some metadata ->
    register_operation_type ?block ~internal:true hash tsp
      metadata.manager_meta_internal_operation_results

and register_origination ?block ?(internal=false) hash tsp ori =
  debug "[Writer]  origination %s\n%!" hash;
  let source = ori.node_or_src in
  let counter = Z.to_int64 ori.node_or_counter in
  let fee = ori.node_or_fee in
  let gas_limit = Z.to_int64 ori.node_or_gas_limit in
  let storage_limit = Z.to_int64 ori.node_or_storage_limit in
  register_dune_user source;
  let manager = ori.node_or_manager in
  register_dune_user manager;
  let delegate = match ori.node_or_delegate with
    | None -> "" | Some delegate -> delegate in
  register_dune_user delegate ;
  let balance = ori.node_or_balance in
  let spendable = match ori.node_or_spendable with
    | Some spendable -> spendable
    | None -> match ori.node_or_script with
      | Some _ -> false
      | _ -> true in
  let sc_code, sc_storage, sc_code_hash = match ori.node_or_script with
    | None -> None, None, None
    | Some sc -> sc.sc_code, Some sc.sc_storage, sc.sc_code_hash in
  begin match block with
    | Some block ->
      let op_level, op_block_hash, network, timestamp_block, _, _ = block_info block in
      let kt1 = match ori.node_or_metadata with
        | None -> Crypto.op_to_KT1 hash
        | Some meta ->
          match meta.manager_meta_operation_result with
          | None -> assert false
          | Some meta ->
            match meta.meta_op_originated_contracts with
            | None | Some [] -> Crypto.op_to_KT1 hash
            | Some (hd :: _ ) -> hd in
      debug "[Writer] kt1 %S originated by %S\n%!" kt1 source ;
      let delegatable = match ori.node_or_delegatable with
        | Some delegatable -> delegatable
        | None -> if String.length kt1 < 2 then false
          else match String.sub kt1 0 2 with
            | "KT" -> true
            | _ -> false in
      register_dune_user kt1;
      update_originated_contract ~origination:hash kt1
        ori.node_or_delegate manager spendable delegatable;
      let failed = get_status_from_manager_metadata ori.node_or_metadata in
      let errors = get_manager_errors ori.node_or_metadata in
      let burn = if internal then 0L else get_total_burn ori.node_or_metadata in
      PGSQL(dbh)
        "INSERT INTO origination_last \
         (hash, source, kt1, fee, counter, manager, delegate, script_code, \
         script_storage_type, script_code_hash, spendable, delegatable, balance, gas_limit, \
         storage_limit, failed, internal, burn_dun, timestamp_op, \
         op_level, op_block_hash, distance_level, network, timestamp_block, errors) \
         VALUES($hash, $source, $kt1, $fee, $counter, $manager, $delegate, \
         $?sc_code, $?sc_storage, $?sc_code_hash, $spendable, $delegatable, $balance, $gas_limit, \
         $storage_limit, $failed, $internal, $burn, $tsp, \
         $op_level, $op_block_hash, -1, $network, $timestamp_block, $?errors)";
      PGSQL(dbh)
        "INSERT INTO origination_all \
         (hash, source, kt1, fee, counter, manager, delegate, script_code, \
         script_storage_type, script_code_hash, spendable, delegatable, balance, gas_limit, \
         storage_limit, failed, internal, burn_dun, timestamp_op, \
         op_level, op_block_hash, distance_level, network, timestamp_block, errors) \
         VALUES($hash, $source, $kt1, $fee, $counter, $manager, $delegate, \
         $?sc_code, $?sc_storage, $?sc_code_hash, $spendable, $delegatable, $balance, $gas_limit, \
         $storage_limit, $failed, $internal, $burn, $tsp, \
         $op_level, $op_block_hash, -1, $network, $timestamp_block, $?errors)"
    | None -> ()
  end;
  match ori.node_or_metadata with
  | None -> ()
  | Some metadata ->
    register_operation_type ?block ~internal:true hash tsp
      metadata.manager_meta_internal_operation_results

and register_operation_type ?constants ?block ?branch ?internal hash tsp operations =
  let len = List.length operations in
  List.iteri (fun i op ->
      debug "[Writer] register_operation_type %S %d / %d\n%!" hash (i + 1) len ;
      match op with
      | NSeed_nonce_revelation snr -> register_seed_nonce_revelation ?block hash tsp snr
      | NDouble_baking_evidence dbe -> register_double_baking_evidence ?block hash dbe
      | NDouble_endorsement_evidence dee -> register_double_endorsement_evidence ?block hash dee
      | NActivation act -> register_activation ?block hash tsp act
      | NEndorsement endo -> register_endorsement ?block ?branch hash endo
      | NActivate_protocol acp -> register_activate_protocol ?constants ?block ?internal hash acp
      | NManage_accounts mac -> register_manage_accounts ?block ?internal hash mac
      | NDelegation dlg -> register_delegation ?block ?internal hash tsp dlg
      | NReveal rvl -> register_reveal ?block ?internal hash tsp rvl
      | NTransaction tr -> register_transaction ?block ?internal hash tsp tr
      | NOrigination ori -> register_origination ?block ?internal hash tsp ori
      | _ -> ())
    operations

let register_block_operation block op =
  let block_hash = block.node_hash in
  let network = block.node_chain_id in
  let tsp = Pg_helper.cal_of_date_dune block.node_header.header_shell.shell_timestamp in
  let op_hash = op.node_op_hash in
  let op_type = Dune_utils.string_of_op_contents (List.hd op.node_op_contents) in
  let op_anon_type =
    Utils.opt_list @@ Dune_utils.string_of_anonymous_op_contents op.node_op_contents in
  let op_manager_type =
    Utils.opt_list @@ Dune_utils.string_of_manager_op_contents op.node_op_contents in
  debug "[Writer] register_block_operation %S %S\n%!" block_hash op_hash;
  PGSQL(dbh)
    "INSERT INTO operation (hash, op_type, op_anon_type, op_manager_type, timestamp, block_hash, network) \
     VALUES ($op_hash, $op_type, $op_anon_type, $op_manager_type, $tsp, $block_hash, $network)"

let register_operations ?constants block ops =
  let tsp = Pg_helper.cal_of_date_dune block.node_header.header_shell.shell_timestamp in
  let len = List.length ops in
  List.iteri (fun i op ->
      debug "[Writer] register_operation %S %d / %d\n%!" block.node_hash (i + 1) len;
      register_block_operation block op;
      register_operation_type ?constants ~block ~branch:op.node_op_branch op.node_op_hash
        tsp op.node_op_contents) ops

let string_operation (op : node_operation_type option) : string =
  match op with
  | None -> "Header"
  | Some op -> match op with
      NTransaction _ -> "Transaction"
    | NOrigination _ -> "Origination"
    | NReveal _ -> "Reveal"
    | NDelegation _ -> "Delegation"
    | NSeed_nonce_revelation _ -> "Seed nonce revelation"
    | NActivation _ -> "Activation"
    | NDouble_endorsement_evidence _ -> "Double endorsement evidence"
    | NDouble_baking_evidence _ -> "Double baking evidence"
    | NEndorsement _ -> "Endorsement"
    | NProposals _ -> "Proposals"
    | NBallot _ -> "Ballot"
    | NActivate -> "Activate"
    | NActivate_testnet -> "Activate testnet"
    | NActivate_protocol _ -> "Activate protocol"
    | NManage_accounts _ -> "Manage accounts"

(* Balance updates *)

let bu_to_bu_info ?op may_burn bu_internal bu_level bu_date bu bu_block_hash =
  let open Data_types in
  let bu_op_type = string_operation op in
  match bu with
    Contract (bu_account, bu_diff) ->
    Some
      {bu_account;
       bu_block_hash;
       bu_diff;
       bu_date;
       bu_update_type = "Contract";
       bu_op_type;
       bu_internal;
       bu_level = Int32.to_int bu_level;
       bu_frozen = false;
       bu_burn=may_burn; bu_cycle = None} (* Contracts in originations/transactions may burn dun *)
  | Rewards (bu_account,_,bu_diff)->
    Some
      {bu_account;
       bu_block_hash;
       bu_diff;
       bu_date;
       bu_update_type = "Reward";
       bu_op_type;
       bu_internal;
       bu_level = Int32.to_int bu_level;
       bu_frozen = true;
       bu_burn=false; bu_cycle = None}

  | Fees (bu_account,_,bu_diff)->
    Some
      {bu_account;
       bu_block_hash;
       bu_diff;
       bu_date;
       bu_update_type = "Fee";
       bu_op_type;
       bu_internal;
       bu_level = Int32.to_int bu_level;
       bu_frozen = true;
       bu_burn=false; bu_cycle = None}

  | Deposits (bu_account,_,bu_diff) ->
    Some
      {bu_account;
       bu_block_hash;
       bu_diff;
       bu_date;
       bu_update_type = "Deposit";
       bu_op_type;
       bu_internal;
       bu_level = Int32.to_int bu_level;
       bu_frozen = true;
       bu_burn=false; bu_cycle = None}

let get_op_bal_update = function
  | Some {meta_op_status;meta_op_balance_updates = Some l;_} ->
    begin match meta_op_status with Some "applied" | None -> l | _ -> [] end
  | _ -> []

let rec get_man_bal_update ?op header_bu_infos man_mtdt is_orig internal date level block_hash =
  let b_u = match man_mtdt.manager_meta_balance_updates with
      Some b_u -> b_u
    | None -> []
  in
  let bu_info_internal : Data_types.balance_update_info list =
    List.flatten
      (List.map
         (get_balance_update_info ~header_bu_infos date level true block_hash)
         man_mtdt.manager_meta_internal_operation_results) in
  let all_bu =
    b_u
    @ (get_op_bal_update man_mtdt.manager_meta_operation_result) in

  bu_info_internal @
  (
    List.fold_left
      (fun acc bu ->
         let bu_info =
           match op with
             None -> bu_to_bu_info is_orig internal level date bu block_hash
           | Some op -> bu_to_bu_info ~op is_orig internal level date bu block_hash in

         match bu_info with
           None -> acc
         | Some bu -> bu :: acc) [] all_bu)

and get_balance_update_info
    ?(header_bu_infos=[])
    (date : Data_types.Date.t)
    (level : int32)
    (internal:bool)
    (block_hash : block_hash)
    (op : node_operation_type) =
  let open Data_types in
  match op with
    NTransaction {node_tr_metadata = Some man_mtdt; _ }
  | NReveal {node_rvl_metadata = Some man_mtdt; _ }
  | NDelegation {node_del_metadata = Some man_mtdt; _ }
  | NActivate_protocol {node_acp_metadata = Some man_mtdt; _}
  | NManage_accounts {node_mac_metadata = Some man_mtdt; _} ->
    get_man_bal_update ~op header_bu_infos man_mtdt false internal date level block_hash

  | NOrigination {node_or_metadata = Some man_mtdt; node_or_manager = manager; _} ->
     (* Burns dun *)
    let (bu_info : balance_update_info list) =
      get_man_bal_update ~op header_bu_infos man_mtdt true internal date level block_hash in
    (* Burned dun appear as contracts. For usual operations, contracts go by pairs
         (one is debited, the other credited).
         Burnt dun are the only unpaired contracts.*)
    let () =
      List.iter
        (fun (info : balance_update_info) ->
           if not info.bu_burn || not (String.equal info.bu_account manager)
           then () (* It has either been treated, or is not a burn contract *)
           else
             let opp_val = Int64.neg info.bu_diff in
             let is_not_burn : bool =
               List.exists
                 (fun (i:balance_update_info) ->
                    if Int64.equal opp_val i.bu_diff
                    then let () = i.bu_burn <- false in true
                    else false
                 )
                 bu_info
             in
             info.bu_burn <- not is_not_burn
        )
        (bu_info@header_bu_infos)
    in bu_info

  | NSeed_nonce_revelation {node_seed_metadata = op_mtdt ; _ }
  | NActivation {node_act_metadata = op_mtdt ; _ }
  | NDouble_endorsement_evidence {node_double_endorsement_metadata = op_mtdt ; _ }
  | NDouble_baking_evidence {node_double_bh_metadata = op_mtdt ; _ }
  | NEndorsement {node_endorse_metadata = op_mtdt ; _ }
  | NProposals {node_prop_metadata = op_mtdt ; _ }
  | NBallot {node_ballot_metadata = op_mtdt ; _ } ->
    List.fold_left
      (fun acc bu ->
         match bu_to_bu_info ~op false internal level date bu block_hash with
           None -> acc
         | Some bu -> bu :: acc)
      []
      (get_op_bal_update op_mtdt)
  | _ -> []

let insert_bu_info bu =
  let open Data_types in
  let (hash : string) = bu.bu_account in
  let (diff : int64) = bu.bu_diff in
  let (date : CalendarLib.Calendar.t) = Pg_helper.cal_of_date bu.bu_date in
  let (update_type : string) = bu.bu_update_type in
  let (internal : bool) = bu.bu_internal in
  let (level : int32) = Int32.of_int bu.bu_level in
  let (frozen: bool) = bu.bu_frozen in
  let (burn : bool) = bu.bu_burn in
  let (block_hash: block_hash) = bu.bu_block_hash in
  let (op_type : string) = bu.bu_op_type in
  PGSQL(dbh)
    "INSERT INTO balance_updates \
     (hash, block_hash, diff, date, update_type, operation_type, internal, level, frozen, burn) \
     VALUES \
     ($hash, $block_hash, $diff, $date, $update_type, $op_type, $internal, $level, $frozen, $burn)"

let bu_infos_from_block block =
  let date = (Utils.dune_to_data_date block.node_header.header_shell.shell_timestamp) in
  let ops = List.flatten block.node_operations in
  let level = Int32.of_int block.node_header.header_shell.shell_level in
  let block_hash = block.node_hash in
  let header_bu_infos =
    match block.node_metadata.meta_header.header_meta_balance_updates with
      None -> []
    | Some l ->
      List.fold_left
        (fun acc bu ->
           match bu_to_bu_info true true level date bu block_hash with
             None -> acc
           | Some bu_info -> bu_info :: acc) [] l in
    List.fold_left
      (fun acc op ->
         List.flatten (
             List.map
               (get_balance_update_info ~header_bu_infos date level false block_hash)
               op.node_op_contents) @ acc)
      header_bu_infos
      ops

let register_balance_updates_info balance_update_infos =
  List.iter insert_bu_info balance_update_infos

module StrMap =
    Map.Make(
    struct
      type t = string
      let compare =
        String.compare end)

let update_new_cycle cycle =
  (* New cycle for balance history *)
  PGSQL(dbh)
    "INSERT INTO balance_from_balance_updates (hash,spendable_balance,frozen,rewards,fees,deposits,cycle) \
     SELECT old.hash,old.spendable_balance,old.frozen,old.rewards,old.fees,old.deposits,$cycle \
     FROM balance_snapshot AS old WHERE old.modified=true ON CONFLICT(hash,cycle) DO NOTHING";
  PGSQL(dbh)
    "UPDATE balance_snapshot SET modified=false";
  (* There are too much balance updates for the db to be efficient.
     Every 5 cycle, sanitize the table balance_updates. *)
  let cycle = Int64.of_int32 cycle in
  let cycle_limit = Int64.of_int (5 + 1) in
  if cycle < cycle_limit then () (* Do nothing *)
  else (* sanitize *)
    match PGSQL(dbh) "SELECT level_start, level_end FROM cycle_limits \
                      WHERE cycle = $cycle - ($cycle_limit + 1::bigint)" with
    | (level_start, level_end) :: _ ->
      let level_start, level_end = Int64.(to_int32 level_start, to_int32 level_end) in
      PGSQL(dbh)
        "DELETE FROM balance_updates \
         WHERE level BETWEEN $level_start AND $level_end"
    | _ -> ()

let update_balance_from_balance_updates
    ?(force_init=false) cycle hash total_diff =
  let open Data_types in
    let old_balances =
      PGSQL(dbh)
        "SELECT spendable_balance,frozen,rewards,fees,deposits \
         FROM balance_snapshot WHERE hash=$hash" in

    match old_balances with
    | [] ->
      let s_diff = total_diff.b_spendable
      and f_diff = total_diff.b_frozen
      and rew_diff = total_diff.b_rewards
      and fee_diff = total_diff.b_fees
      and dep_diff = total_diff.b_deposits in
      begin
        if force_init
        then
          PGSQL(dbh)
            "INSERT INTO balance_from_balance_updates \
             (hash,spendable_balance,frozen,rewards,fees,deposits,cycle) \
             VALUES \
             ($hash,$s_diff,$f_diff,$rew_diff, $fee_diff,$dep_diff,$cycle)"
        else
          PGSQL(dbh)
            "INSERT INTO balance_from_balance_updates \
             (hash,spendable_balance,frozen,rewards,fees,deposits,cycle) \
             VALUES \
             ($hash,0,0,0,0,0,$cycle)"
      end;
      PGSQL(dbh)
        "INSERT INTO balance_snapshot \
         (hash,spendable_balance,frozen,rewards,fees,deposits,modified) \
         VALUES \
         ($hash,$s_diff,$f_diff,$rew_diff, $fee_diff,$dep_diff,true)"

    | (sb,frz,rew,fees,deps) :: _ ->
      let new_spd_bal =
        Int64.add sb total_diff.b_spendable
      in
      let new_frz_bal =
        Int64.add frz total_diff.b_frozen
      in
      let new_rw_bal =
        Int64.add rew total_diff.b_rewards
      in
      let new_fee_bal =
        Int64.add fees total_diff.b_fees
      in
      let new_dep_bal =
        Int64.add deps total_diff.b_deposits
      in
      PGSQL(dbh)
        "UPDATE balance_snapshot \
         SET \
         spendable_balance=$new_spd_bal, \
         frozen=$new_frz_bal, \
         rewards=$new_rw_bal, \
         fees=$new_fee_bal, \
         deposits=$new_dep_bal, \
         modified=true \
         WHERE \
         hash=$hash"

let register_balance_from_balance_updates level (bu_list : (account_hash * int64 * bool * string) list) =
  let update_map acc bu  =
    let open Data_types in
    let hash,diff,frz,update_type = bu in
    StrMap.update
      hash
      (fun old ->
         let diff_balance =
           {b_spendable = if frz then Int64.zero else diff;
            b_frozen = if frz then diff else Int64.zero;
            b_rewards = if update_type = "Reward" then diff else Int64.zero;
            b_fees = if update_type = "Fee" then diff else Int64.zero;
            b_deposits = if update_type = "Deposit" then diff else Int64.zero;
           }
         in
          match old with
            None -> Some diff_balance
          | Some bal ->

            let (+) = Int64.add in

            Some (
              {b_spendable = bal.b_spendable + diff_balance.b_spendable;
               b_frozen = bal.b_frozen + diff_balance.b_frozen;
               b_rewards =  bal.b_rewards + diff_balance.b_rewards;
               b_fees =  bal.b_fees + diff_balance.b_fees;
               b_deposits =  bal.b_deposits + diff_balance.b_deposits;
           }))
      acc in
  let bal_map = List.fold_left update_map StrMap.empty bu_list in
  let level = Int64.of_int32 level in
  let cycle, first = match
      PGSQL(dbh) "SELECT cycle, $level = level_start FROM cycle_limits \
                  WHERE $level BETWEEN level_start AND level_end" with
  | (cycle, Some first) :: _ -> Int64.to_int32 cycle, first
  | _ -> 0l, false in
  StrMap.iter (update_balance_from_balance_updates cycle) bal_map;
  if first then update_new_cycle cycle

let register_init_balance hash init date level =
  let open Data_types in
  let bu_info =
    {
      bu_account = hash;
      bu_block_hash = "";
      bu_diff = init;
      bu_date = date;
      bu_update_type = "Initialization";
      bu_op_type = "";
      bu_internal = true;
      bu_level = level;
      bu_frozen = false;
      bu_burn = false;
      bu_cycle = None}
  in
  insert_bu_info bu_info;
  update_balance_from_balance_updates
    ~force_init:true
    Int32.zero
    hash
    {b_spendable = init;
     b_frozen = Int64.zero;
     b_rewards = Int64.zero;
     b_fees = Int64.zero;
     b_deposits =Int64.zero}

let register_balances_updates block =
  register_balance_updates_info @@ bu_infos_from_block block

let compute_volume_fees ops =
  List.fold_left (fun (acc_vol, acc_fee) op ->
      List.fold_left (fun (acc_vol, acc_fee) op -> match op with
          | NTransaction tr ->
            let amount = tr.node_tr_amount in
            let fee = tr.node_tr_fee in
            let amount =
              if get_status_from_manager_metadata tr.node_tr_metadata then
                acc_vol
              else
                Int64.add amount acc_vol in
            amount, Int64.add fee acc_fee
          | NOrigination ori ->
            let balance = ori.node_or_balance in
            let fee = ori.node_or_fee in
            let balance =
              if get_status_from_manager_metadata ori.node_or_metadata then
                acc_vol
              else
                Int64.add balance acc_vol in
            balance, Int64.add fee acc_fee
          | NDelegation del ->
            let fee = del.node_del_fee in
            acc_vol, Int64.add fee acc_fee
          | NReveal rvl ->
            let fee = rvl.node_rvl_fee in
            acc_vol, Int64.add fee acc_fee
          | NActivate_protocol acp ->
            let fee = acp.node_acp_fee in
            acc_vol, Int64.add fee acc_fee
          | NManage_accounts mac ->
            let fee = mac.node_mac_fee in
            acc_vol, Int64.add fee acc_fee
          | _ -> acc_vol, acc_fee)
        (acc_vol, acc_fee) op.node_op_contents)
    (0L, 0L) ops

let register_all ?constants block lvl ops =
  let volume, fees = compute_volume_fees ops in
  let operation_count = Int64.of_int @@ List.length ops in
  pg_lock (fun () ->
      register_header block.node_header;
      register_block ?constants block lvl (-1L) operation_count volume fees;
      register_operations ?constants block ops;
      register_balances_updates block
    )

let update_count_info ?(force=false) hash level info =
  match PGSQL(dbh) "SELECT level FROM count_info WHERE info = $info" with
  | [ level0 ] ->
    if (info = "lowest" && level0 > level) ||
       (info = "highest" && level0 < level) || force then
      PGSQL(dbh)
        "UPDATE count_info SET hash = $hash, level = $level WHERE info = $info"
  | _ ->
    PGSQL(dbh) "INSERT INTO count_info (hash, level, info) \
                VALUES($hash, $level, $info)"

let update_counts ?(force=false) hash sign =
  let alt = if sign = 1L then 0L else 1L in
  match PGSQL(dbh)
          "SELECT bl.cycle, bl.level, bl.priority, bl.fees, bl.baker, \
           bake_time(bl.timestamp - pred.timestamp, bl.priority, \
           time_between_blocks), time_between_blocks[1] \
           FROM block AS bl INNER JOIN block AS pred ON \
           pred.hash = bl.predecessor \
           INNER JOIN protocol_constants AS cst ON \
           COALESCE(bl.level BETWEEN cst.level_start AND cst.level_end, \
           cst.level_start <= bl.level AND cst.level_end IS NULL) \
           WHERE bl.hash = $hash AND cst.distance_level = 0" with
  | [ cycle, level, priority, fees, baker, bktime, t0 ] ->
    debug "[Writer] [update_counts_main] \
           Update counts for block %s at level %Ld with factor %Ld\n%!" hash level sign;
    let bktime = match bktime, t0 with
      | Some bktime, _ -> bktime
      | _, Some t0 ->
        debug "[Writer] [update_counts_main] \
               Cannot compute bake time, setting it to time_between_blocks[0]\n%!";
        Int32.to_float t0
      | _ ->
        debug "[Writer] [update_counts_main] \
               Cannot get time constants, setting bake time to 60s\n%!";
        60. in
    let bktime = Int64.to_float sign *. bktime in
    begin match PGSQL(dbh) "SELECT cycle FROM cycle_count WHERE cycle = $cycle" with
      | [] -> PGSQL(dbh) "INSERT INTO cycle_count(cycle) VALUES($cycle)"
      | _ -> () end;
    PGSQL(dbh) (* cycle counts *)
      "WITH tr(nb) AS ( \
       SELECT COUNT( * ) FROM transaction_all WHERE op_block_hash = $hash), \
       dlg(nb) AS ( \
       SELECT COUNT( * ) FROM delegation_all WHERE op_block_hash = $hash), \
       ori(nb) AS ( \
       SELECT COUNT( * ) FROM origination_all WHERE op_block_hash = $hash), \
       act(nb) AS ( \
       SELECT COUNT( * ) FROM activation_all WHERE op_block_hash = $hash), \
       rvl(nb) AS ( \
       SELECT COUNT( * ) FROM reveal_all WHERE op_block_hash = $hash), \
       dbe(nb) AS ( \
       SELECT COUNT( * ) FROM double_baking_evidence_all WHERE op_block_hash = $hash), \
       dee(nb) AS ( \
       SELECT COUNT( * ) FROM double_endorsement_evidence_all WHERE op_block_hash = $hash), \
       nonce(nb) AS ( \
       SELECT COUNT( * ) FROM seed_nonce_revelation_all WHERE op_block_hash = $hash), \
       endo(nba, nb_op) AS ( \
       SELECT agg_prio(priority, array_length(slots, 1), true), count(*) \
       FROM endorsement_all WHERE op_block_hash = $hash) \
       UPDATE cycle_count SET \
       nb_transaction = nb_transaction + $sign * tr.nb, \
       nb_delegation = nb_delegation + $sign * dlg.nb, \
       nb_origination = nb_origination + $sign * ori.nb, \
       nb_activation = nb_activation + $sign * act.nb, \
       nb_reveal = nb_reveal + $sign * rvl.nb, \
       nb_dbe = nb_dbe + $sign * dbe.nb, \
       nb_dee = nb_dee + $sign * dee.nb, \
       nb_nonce = nb_nonce + $sign * nonce.nb, \
       nb_endorsement = array_lin(nb_endorsement, endo.nba, 1, $sign), \
       nb_endorsement_op = nb_endorsement_op + $sign * endo.nb_op,
       nb_prio = array_lin(nb_prio, init_array_prio($priority, 1), 1, $sign) \
       FROM tr, dlg, ori, act, rvl, dbe, dee, nonce, endo \
       WHERE cycle = $cycle";
    PGSQL(dbh) (* missed endorsement *)
      "WITH endo(dn, nb) AS ( \
       SELECT UNNEST(endorsers), UNNEST(slots) FROM level_rights \
       WHERE level = $level - 1::bigint) \
       UPDATE cycle_count_baker AS ccb SET \
       nb_miss_endorsement = nb_miss_endorsement + $sign::bigint * COALESCE(endo.nb, 0) \
       FROM endo WHERE endo.dn = ccb.dn AND cycle = $cycle";
    PGSQL(dbh) (* endorsement *)
      "WITH endo(dn, nba, nb) AS ( \
       SELECT source, agg_prio(priority, array_length(slots, 1), true), \
       SUM(array_length(slots, 1)) \
       FROM endorsement_all WHERE op_block_hash = $hash GROUP BY source) \
       UPDATE cycle_count_baker AS ccb SET \
       nb_endorsement = array_lin(nb_endorsement, endo.nba, 1, $sign), \
       nb_miss_endorsement = nb_miss_endorsement - $sign * endo.nb, \
       nb_alt_endorsement = nb_alt_endorsement + $alt \
       FROM endo WHERE endo.dn = ccb.dn AND cycle = $cycle";
    PGSQL(dbh) (* baking *)
      "UPDATE cycle_count_baker SET \
       nb_baking = array_lin(nb_baking, init_array_prio($priority, 1), 1, $sign), \
       fees = fees + $sign::bigint * $fees, \
       time = time + $bktime, \
       nb_alt_baking = nb_alt_baking + $alt \
       WHERE cycle = $cycle AND dn = $baker";
    PGSQL(dbh) (* missed baking *)
      "UPDATE cycle_count_baker SET nb_miss_baking = nb_miss_baking + $sign \
       FROM (SELECT UNNEST(bakers), UNNEST(bakers_priority) \
       FROM level_rights WHERE level = $level) AS lr(baker, prio) \
       WHERE prio < $priority AND dn = baker AND cycle = $cycle";
    PGSQL(dbh) (* transaction as source *)
      "WITH tr_src(dn, nb) AS ( \
       SELECT source, COUNT( * ) FROM transaction_all WHERE op_block_hash = $hash \
       GROUP BY source) \
       UPDATE operation_count_user AS ocu SET \
       nb_transaction_src = nb_transaction_src + $sign * coalesce(tr_src.nb,0) \
       FROM tr_src WHERE ocu.dn = tr_src.dn";
    PGSQL(dbh) (* transaction as destination without source *)
      "WITH tr_dst(dn, nb) AS ( \
       SELECT destination, COUNT( * ) FROM transaction_all \
       WHERE op_block_hash = $hash AND source <> destination \
       GROUP BY destination) \
       UPDATE operation_count_user AS ocu SET \
       nb_transaction_dst = nb_transaction_dst + $sign * coalesce(tr_dst.nb,0) \
       FROM tr_dst WHERE ocu.dn = tr_dst.dn";
    PGSQL(dbh) (* delegation as source *)
      "WITH dlg_src(dn, nb) AS ( \
       SELECT source, COUNT( * ) FROM delegation_all WHERE op_block_hash = $hash \
       GROUP BY source) \
       UPDATE operation_count_user AS ocu SET \
       nb_delegation_src = nb_delegation_src + $sign * coalesce(dlg_src.nb,0) \
       FROM dlg_src WHERE ocu.dn = dlg_src.dn";
    PGSQL(dbh) (* delegation as delegate without source *)
      "WITH dlg_dlg(dn, nb) AS ( \
       SELECT delegate, COUNT( * ) FROM delegation_all WHERE op_block_hash = $hash \
       AND delegate <> source GROUP BY delegate) \
       UPDATE operation_count_user AS ocu SET \
       nb_delegation_dlg = nb_delegation_dlg + $sign * coalesce(dlg_dlg.nb,0) \
       FROM dlg_dlg WHERE ocu.dn = dlg_dlg.dn";
    PGSQL(dbh) (* origination as source *)
      "WITH ori_src(dn, nb) AS ( \
       SELECT source, COUNT( * ) FROM origination_all WHERE op_block_hash = $hash \
       GROUP BY source) \
       UPDATE operation_count_user AS ocu SET \
       nb_origination_src = nb_origination_src + $sign * coalesce(ori_src.nb,0) \
       FROM ori_src WHERE ocu.dn = ori_src.dn";
    PGSQL(dbh) (* origination as manager without source *)
      "WITH ori_man(dn, nb) AS ( \
       SELECT manager, COUNT( * ) FROM origination_all WHERE op_block_hash = $hash \
       AND manager <> source GROUP BY manager) \
       UPDATE operation_count_user AS ocu SET \
       nb_origination_man = nb_origination_man + $sign * coalesce(ori_man.nb,0) \
       FROM ori_man WHERE ocu.dn = ori_man.dn";
    PGSQL(dbh) (* origination as kt1 *)
      "WITH ori_kt1(dn, nb) AS ( \
       SELECT kt1, COUNT( * ) FROM origination_all WHERE op_block_hash = $hash \
       GROUP BY kt1) \
       UPDATE operation_count_user AS ocu SET \
       nb_origination_kt1 = nb_origination_kt1 + $sign * coalesce(ori_kt1.nb,0) \
       FROM ori_kt1 WHERE ocu.dn = ori_kt1.dn";
    PGSQL(dbh) (* origination as delegate *)
      "WITH ori_dlg(dn, nb) AS ( \
       SELECT delegate, COUNT( * ) FROM origination_all WHERE op_block_hash = $hash \
       GROUP BY delegate) \
       UPDATE operation_count_user AS ocu SET \
       nb_origination_dlg = nb_origination_dlg + $sign * coalesce(ori_dlg.nb,0) \
       FROM ori_dlg WHERE ocu.dn = ori_dlg.dn";
    PGSQL(dbh) (* activation *)
      "WITH act(dn, nb) AS ( \
       SELECT pkh, COUNT( * ) FROM activation_all WHERE op_block_hash = $hash \
       GROUP BY pkh) \
       UPDATE operation_count_user AS ocu SET \
       nb_activation = nb_activation + $sign * coalesce(act.nb,0) \
       FROM act WHERE ocu.dn = act.dn";
    PGSQL(dbh) (* reveal *)
      "WITH rvl(dn, nb) AS ( \
       SELECT source, COUNT( * ) FROM reveal_all WHERE op_block_hash = $hash \
       GROUP BY source) \
       UPDATE operation_count_user AS ocu SET \
       nb_reveal = nb_reveal + $sign * coalesce(rvl.nb,0) \
       FROM rvl WHERE ocu.dn = rvl.dn";
    PGSQL(dbh) (* double baking evidence as denouncer/baker*)
      "WITH dbe_bk(dn, nb) AS ( \
       SELECT denouncer, COUNT( * ) FROM double_baking_evidence_all \
       WHERE op_block_hash = $hash \
       GROUP BY denouncer) \
       UPDATE operation_count_user AS ocu SET \
       nb_dbe_bk = nb_dbe_bk + $sign * coalesce(dbe_bk.nb,0) \
       FROM dbe_bk WHERE ocu.dn = dbe_bk.dn";
    PGSQL(dbh) (* double baking evidence as accused *)
      "WITH dbe_acc(dn, nb) AS ( \
       SELECT accused, COUNT( * ) FROM double_baking_evidence_all \
       WHERE op_block_hash = $hash AND accused <> denouncer \
       GROUP BY accused) \
       UPDATE operation_count_user AS ocu SET \
       nb_dbe_acc = nb_dbe_acc + $sign * coalesce(dbe_acc.nb,0) \
       FROM dbe_acc WHERE ocu.dn = dbe_acc.dn";
    PGSQL(dbh) (* double endorsement evidence as denouncer*)
      "WITH dee_dn(dn, nb) AS ( \
       SELECT denouncer, COUNT( * ) FROM double_endorsement_evidence_all \
       WHERE op_block_hash = $hash \
       GROUP BY denouncer) \
       UPDATE operation_count_user AS ocu SET \
       nb_dee_dn = nb_dee_dn + $sign * coalesce(dee_dn.nb,0) \
       FROM dee_dn WHERE ocu.dn = dee_dn.dn";
    PGSQL(dbh) (* double endorsement evidence as accused *)
      "WITH dee_acc(dn, nb) AS ( \
       SELECT accused, COUNT( * ) FROM double_endorsement_evidence_all \
       WHERE op_block_hash = $hash AND accused <> denouncer \
       GROUP BY accused) \
       UPDATE operation_count_user AS ocu SET \
       nb_dee_acc = nb_dee_acc + $sign * coalesce(dee_acc.nb,0) \
       FROM dee_acc WHERE ocu.dn = dee_acc.dn";
    PGSQL(dbh) (* nonce revelation *)
      "WITH nonce(dn, nb) AS ( \
       SELECT baker, COUNT( * ) FROM seed_nonce_revelation_all \
       WHERE op_block_hash = $hash \
       GROUP BY baker) \
       UPDATE operation_count_user AS ocu SET \
       nb_nonce = nb_nonce + $sign * coalesce(nonce.nb,0) \
       FROM nonce WHERE ocu.dn = nonce.dn";
    PGSQL(dbh) (* endorsement *)
      "WITH endo(dn, nb) AS ( \
       SELECT source, COUNT( * ) FROM endorsement_all \
       WHERE op_block_hash = $hash GROUP BY source ) \
       UPDATE operation_count_user AS ocu SET \
       nb_endorsement = nb_endorsement + $sign * coalesce(endo.nb,0) \
       FROM endo WHERE ocu.dn = endo.dn";
    update_count_info ~force hash level "highest";
  | _ -> debug "[Writer] [update_counts_main] block not registered\n%!"

let reset_balance_from_balance_updates account diff frz up_type level  =
  debug "[Writer] [reset_balance] %s : undoing %s balance_update : diff = %s\n%!"
    account (if frz then "frozen" else "contract") (Int64.to_string diff);
  let level = Int64.of_int32 level in
  let cycle_in_bfbu =
    match PGSQL(dbh) "SELECT cycle FROM cycle_limits \
                      WHERE $level BETWEEN level_start AND level_end" with
    | cycle :: _ -> Int32.(add (Int64.to_int32 cycle) 1l)
    | _ -> 0l in
  (* We add one because the index of balance_from_balance_updates has an offset of 1 *)
  if not frz then
    begin
      PGSQL(dbh)
        "UPDATE balance_from_balance_updates \
         SET spendable_balance=spendable_balance - $diff \
         WHERE hash=$account AND cycle>=$cycle_in_bfbu";
      PGSQL(dbh)
        "UPDATE balance_snapshot \
         SET spendable_balance=spendable_balance - $diff \
         WHERE hash=$account"
    end
  else
    let () =
      PGSQL(dbh)
        "UPDATE balance_from_balance_updates \
         SET frozen=frozen - $diff \
         WHERE hash=$account AND cycle>=$cycle_in_bfbu";
      PGSQL(dbh)
        "UPDATE balance_snapshot \
         SET frozen=frozen - $diff \
         WHERE hash=$account"
    in
    match up_type with
      "Reward" ->
      PGSQL(dbh)
        "UPDATE balance_from_balance_updates \
         SET rewards=rewards - $diff \
         WHERE hash=$account AND cycle>=$cycle_in_bfbu";
      PGSQL(dbh)
        "UPDATE balance_snapshot \
         SET rewards=rewards - $diff \
         WHERE hash=$account"
    | "Fee" ->
      PGSQL(dbh)
        "UPDATE balance_from_balance_updates \
         SET fees=fees - $diff \
         WHERE hash=$account AND cycle>=$cycle_in_bfbu";
      PGSQL(dbh)
        "UPDATE balance_snapshot \
         SET fees=fees - $diff \
         WHERE hash=$account"
    | "Deposit" ->
      PGSQL(dbh)
        "UPDATE balance_from_balance_updates \
         SET deposits=deposits - $diff \
         WHERE hash=$account AND cycle>=$cycle_in_bfbu";
      PGSQL(dbh)
        "UPDATE balance_snapshot \
         SET deposits=deposits - $diff \
         WHERE hash=$account"
    | s -> debug "[Writer] [update_balance_updates] %s undefined\n%!" s

let update_distance_level_alt level =
  PGSQL(dbh) "UPDATE block SET distance_level = -1 WHERE level > $level" ;
  PGSQL(dbh) "UPDATE endorsement_all SET distance_level = -1 WHERE op_level > $level" ;
  PGSQL(dbh) "UPDATE endorsement_last SET distance_level = -1 WHERE op_level > $level";
  PGSQL(dbh) "UPDATE transaction_all SET distance_level = -1 WHERE op_level > $level" ;
  PGSQL(dbh) "UPDATE transaction_last SET distance_level = -1 WHERE op_level > $level";
  PGSQL(dbh) "UPDATE origination_all SET distance_level = -1 WHERE op_level > $level" ;
  PGSQL(dbh) "UPDATE origination_last SET distance_level = -1 WHERE op_level > $level";
  PGSQL(dbh) "UPDATE delegation_all SET distance_level = -1 WHERE op_level > $level" ;
  PGSQL(dbh) "UPDATE delegation_last SET distance_level = -1 WHERE op_level > $level";
  PGSQL(dbh) "UPDATE activation_all SET distance_level = -1 WHERE op_level > $level" ;
  PGSQL(dbh) "UPDATE activation_last SET distance_level = -1 WHERE op_level > $level";
  PGSQL(dbh) "UPDATE reveal_all SET distance_level = -1 WHERE op_level > $level" ;
  PGSQL(dbh) "UPDATE reveal_last SET distance_level = -1 WHERE op_level > $level";
  PGSQL(dbh) "UPDATE seed_nonce_revelation_all SET distance_level = -1 WHERE op_level > $level" ;
  PGSQL(dbh) "UPDATE token_operations SET distance_level = -1 WHERE op_level > $level" ;
  let level32= Int64.to_int32 level in
  let bad_bal_updt =
    PGSQL(dbh) "SELECT hash,diff,frozen,update_type FROM balance_updates \
                WHERE level > $level32 AND distance_level = 0" in
  List.iter
    (fun (account,diff,frz,up_type) ->
       reset_balance_from_balance_updates account diff frz up_type level32)
    bad_bal_updt;

  PGSQL(dbh) "UPDATE balance_updates SET distance_level = -1 WHERE level > $level32"

let update_balances level block_hash =
  debug "[Writer] [update_balance_updates]\n%!";
  let level = Int32.of_int level in
  let good_bal_updts =
    PGSQL(dbh) "SELECT hash,diff,frozen,update_type FROM balance_updates \
                WHERE block_hash=$block_hash AND level=$level"
  in
  register_balance_from_balance_updates level good_bal_updts

let update_new_delegates block_hash =
  PGSQL(dbh)
    "WITH tmp(delegator, delegate) AS (\
     SELECT DISTINCT source, delegate FROM delegation_all \
     WHERE op_block_hash = $block_hash) \
     UPDATE account_info SET delegate = tmp.delegate FROM tmp \
     WHERE hash = delegator"

let update_former_delegates block_hash =
  PGSQL(dbh)
    "nullable-results"
    "WITH tmp(delegator, delegate, level) AS (\
     SELECT DISTINCT source, delegate, op_level FROM delegation_all \
     WHERE op_block_hash = $block_hash), \
     tmp2(delegator, delegate) AS (\
     SELECT dlg.source, dlg.delegate FROM tmp \
     LEFT JOIN delegation_all AS dlg ON dlg.source = tmp.delegator \
     WHERE op_level < level AND distance_level = 0 \
     ORDER BY op_level DESC LIMIT 1) \
     UPDATE account_info SET delegate = tmp2.delegate FROM tmp2 \
     WHERE hash = delegator"

let update_token_balances block_hash sign =
  if sign > 0 then
    PGSQL(dbh)
      "WITH tmp(account, contract, source, balance, tsp) AS ( \
       SELECT unnest(balance_accounts), contract, unnest(balance_sources), \
       unnest(balances), timestamp FROM token_operations \
       WHERE op_block_hash = $block_hash) \
       UPDATE token_balances  AS tb SET \
       balance = tmp.balance, last_modified = tmp.tsp, last_block = $block_hash \
       FROM tmp WHERE tb.account = tmp.account AND tb.contract = tmp.contract AND \
       tb.source = tmp.source"
  else
    let l =
      PGSQL(dbh)
        "SELECT unnest(balance_accounts), contract, unnest(balance_sources) \
         FROM token_operations \
         WHERE op_block_hash = $block_hash" in
    List.iter (function
        | Some account, contract, Some source ->
          PGSQL(dbh)
            "UPDATE token_balances SET balance = 0, last_modified = NULL, \
             last_block = NULL \
             WHERE contract = $contract AND account = $account AND source = $source";
          PGSQL(dbh)
            "WITH tmp(balance, tsp, block) AS ( \
             SELECT unnest(top.balances), top.timestamp, top.op_block_hash \
             FROM token_operations AS top \
             WHERE top.contract = $contract AND $account = any(top.balance_accounts) \
             AND top.balance_sources[array_position(top.balance_accounts, $account, 1)] = $source \
             ORDER BY op_level DESC LIMIT 1) \
             UPDATE token_balances \
             SET balance = tmp.balance, last_modified = tmp.tsp, \
             last_block = tmp.block \
             FROM tmp WHERE contract = $contract AND account = $account \
             AND source = $source"
        | _ -> ()) l

let update_constants hash distance_level =
  match PGSQL(dbh) "SELECT level_start, distance_level FROM protocol_constants \
                    WHERE block_hash = $hash" with
  | [] -> ()
  | (level_start, _d) :: _ ->
    debug "[Writer] [update_constants]\n%!";
    let distance_level = Int32.of_int distance_level in
    if distance_level = 0l then
      PGSQL(dbh)
        "WiTH last_constants(hash) AS ( \
         SELECT block_hash FROM protocol_constants WHERE distance_level = 0 \
         ORDER BY level_start DESC LIMIT 1) \
         UPDATE protocol_constants AS cst SET level_end = $level_start - 1::bigint \
         FROM last_constants AS lc \
         WHERE lc.hash = cst.block_hash"
    else
      PGSQL(dbh)
        "WiTH last_constants(hash) AS ( \
         SELECT block_hash FROM protocol_constants WHERE distance_level = 0 \
         AND block_hash <> $hash \
         ORDER BY level_start DESC LIMIT 1) \
         UPDATE protocol_constants AS cst SET level_end = NULL \
         FROM last_constants AS lc \
         WHERE lc.hash = cst.block_hash";
  PGSQL(dbh)
    "UPDATE protocol_constants SET distance_level = $distance_level \
     WHERE block_hash = $hash"

let reset_main_chain count start_level level64 =
  debug "[Writer] [reset_main_chain] %d %Ld\n%!" start_level level64 ;
  if count then
    begin match PGSQL(dbh) "SELECT hash, level FROM block WHERE distance_level = 0 \
                            ORDER BY level DESC LIMIT 1" with
    | [ hash, level ] ->
      let rec aux hash level =
        if level > level64 then (
          update_counts ~force:true hash (-1L) ;
          update_former_delegates hash ;
          update_token_balances hash (-1);
          update_constants hash (-1);
          match PGSQL(dbh) "SELECT predecessor FROM block \
                            WHERE hash = $hash" with
          | [ pred ] -> aux pred (Int64.pred level)
          | _ -> ()) in
      aux hash level
    | _ -> () end;
  update_distance_level_alt level64;
  let start_level64 = Int64.of_int start_level in
  let cycle =
    PGSQL(dbh) "SELECT cycle FROM block WHERE level = $start_level64 LIMIT 1" in
  begin match cycle with
    | [ cycle ] ->
      PGSQL(dbh) "INSERT INTO switch (cycle, switch_count, longest_alt_chain) \
                  VALUES ($cycle, 0, 0) ON CONFLICT DO NOTHING"
    | _ -> ()
  end ;
  let level = Int64.to_int level64 in
  let chain_depth = start_level - level in
  let chain_depth64 = Int64.of_int chain_depth in
  if chain_depth > 1 then
    begin match cycle with
      | [ cycle ] ->
        let row =
          PGSQL(dbh) "SELECT cycle, switch_count, longest_alt_chain FROM switch \
                      WHERE cycle = $cycle" in
        begin match row with
        | [] ->
          PGSQL(dbh) "INSERT INTO switch \
                      (cycle, switch_count, longest_alt_chain) \
                      VALUES \
                      ($cycle, 1, $chain_depth64)"
        | [ (_, switch_count, longest_alt_chain) ] ->
          let longest = max longest_alt_chain chain_depth64 in
          let switch_count = Int64.add switch_count 1L in
          PGSQL(dbh) "UPDATE switch SET \
                      switch_count = $switch_count, \
                      longest_alt_chain = $longest \
                      WHERE cycle = $cycle"
        | _ -> ()
        end
      | _ -> ()
    end

let update_distance_level_main count hash =
  PGSQL(dbh) "UPDATE block SET distance_level = 0 WHERE hash = $hash";
  PGSQL(dbh) "UPDATE endorsement_all SET distance_level = 0 WHERE op_block_hash = $hash";
  PGSQL(dbh) "UPDATE endorsement_last SET distance_level = 0 WHERE op_block_hash = $hash";
  PGSQL(dbh) "UPDATE transaction_all SET distance_level = 0 WHERE op_block_hash = $hash";
  PGSQL(dbh) "UPDATE transaction_last SET distance_level = 0 WHERE op_block_hash = $hash";
  PGSQL(dbh) "UPDATE origination_all SET distance_level = 0 WHERE op_block_hash = $hash";
  PGSQL(dbh) "UPDATE origination_last SET distance_level = 0 WHERE op_block_hash = $hash";
  PGSQL(dbh) "UPDATE delegation_all SET distance_level = 0 WHERE op_block_hash = $hash";
  PGSQL(dbh) "UPDATE delegation_last SET distance_level = 0 WHERE op_block_hash = $hash";
  PGSQL(dbh) "UPDATE activation_all SET distance_level = 0 WHERE op_block_hash = $hash";
  PGSQL(dbh) "UPDATE activation_last SET distance_level = 0 WHERE op_block_hash = $hash";
  PGSQL(dbh) "UPDATE reveal_all SET distance_level = 0 WHERE op_block_hash = $hash";
  PGSQL(dbh) "UPDATE reveal_last SET distance_level = 0 WHERE op_block_hash = $hash";
  PGSQL(dbh) "UPDATE balance_updates SET distance_level = 0 WHERE block_hash = $hash";
  PGSQL(dbh) "UPDATE seed_nonce_revelation_all SET distance_level = 0 WHERE op_block_hash = $hash";
  PGSQL(dbh) "UPDATE token_operations SET distance_level = 0 WHERE op_block_hash = $hash";
  if count then (
    update_constants hash 0;
    update_counts ~force:true hash 1L;
    update_token_balances hash 1;
    update_new_delegates hash)

let register_main_chain count block =
  pg_lock (fun () ->
      let hash = block.node_hash in
      let pred_hash = block.node_header.header_shell.shell_predecessor in
      let start_level = block.node_header.header_shell.shell_level in
      let rec register_aux hash pred_hash curr_level =
        debug "[Writer] [register_main_chain] %d\n%!" curr_level ;

        match  PGSQL(dbh) "SELECT predecessor, level, distance_level \
                           FROM block WHERE hash = $pred_hash" with
        | [ _, level, 0L ] ->
          reset_main_chain count start_level level ;
          update_balances curr_level hash;
          update_distance_level_main count hash
        | [ pred, level, _ ] ->
          register_aux pred_hash pred @@ Int64.to_int level ;
          update_balances curr_level hash;
          update_distance_level_main count hash
        | _ ->
          debug "[Writer] [main_chain] Can't recover main chain status for %s\n%!"
            pred_hash in
      register_aux hash pred_hash start_level;
    )

let peer_to_string peer =
  match peer with
  | None -> ""
  | Some s -> s

let last_connection =
  function
  | None -> "", ""
  | Some (point, date) -> peer_to_string (Some point), date


let get_country point =
  if point = "" then "", ""
  else begin
    try
      let ip =
        try
          Scanf.sscanf point "::ffff:%[0-9.]:%_d" (fun ip -> ip)
        with _ ->
          Scanf.sscanf point "%[0-9a-fA-F:]" (fun ip -> ip)
      in
      let gi = Geoip.init_exn Geoip.GEOIP_MEMORY_CACHE in
      let country_name = Misc.unopt "" @@ Geoip.country_name_by_name gi ip in
      let country_code = Misc.unopt "" @@ Geoip.country_code_by_name gi ip in
      Geoip.close gi ;
      country_name, country_code
    with _ ->
      debug "[Writer] Found unparseable ip: %s\n%!" point;
      "", ""
  end

let register_network_stats stats =
  pg_lock (fun () ->
      PGSQL(dbh) "UPDATE peers SET state= 'disconnected'" ;
      List.iter
        (fun { peer_id; country=_; score ; trusted ; conn_metadata ;
               state ; id_point ; stat ;
               last_failed_connection ; last_rejected_connection ;
               last_established_connection ; last_disconnection ;
               last_seen ; last_miss } ->
          let _conn_metadata = conn_metadata in (* TODO add to db *)
          let point_id = peer_to_string id_point in
          let state =
            match state with
            | Accepted -> "accepted"
            | Running -> "running"
            | Disconnected -> "disconnected" in
          let last_failed_connection_point, last_failed_connection_date =
            last_connection last_failed_connection in
          let last_rejected_connection_point, last_rejected_connection_date =
            last_connection last_rejected_connection in
          let last_established_connection_point, last_established_connection_date =
            last_connection last_established_connection in
          let last_disconnection_point, last_disconnection_date =
            last_connection last_disconnection in
          let last_seen_point, last_seen_date = last_connection last_seen in
          let last_miss_point, last_miss_date = last_connection last_miss in
          let total_sent = stat.total_sent in
          let total_recv = stat.total_recv in
          let current_inflow = Int64.of_int stat.current_inflow in
          let current_outflow = Int64.of_int  stat.current_outflow in
          match PGSQL(dbh) "SELECT peer_id, point_id, country_name, country_code \
                            FROM peers \
                            WHERE peer_id = $peer_id" with
          | [] -> (* No entry just insert a new row *)
            let country_name, country_code = get_country point_id  in
            PGSQL(dbh)
              "INSERT INTO peers \
               (peer_id, country_name, country_code, point_id, trusted, score, state, total_sent, \
               total_received, current_inflow, current_outflow, \
               last_failed_connection_point, last_failed_connection_date, \
               last_rejected_connection_point, last_rejected_connection_date, \
               last_established_connection_point,last_established_connection_date, \
               last_disconnection_point, last_disconnection_date, \
               last_seen_point, last_seen_date, \
               last_miss_point, last_miss_date) \
               VALUES \
               ($peer_id, $country_name, $country_code, $point_id, $trusted, $score, $state, \
               $total_sent, $total_recv, $current_inflow, $current_outflow, \
               $last_failed_connection_point, $last_failed_connection_date, \
               $last_rejected_connection_point, $last_rejected_connection_date, \
               $last_established_connection_point, $last_established_connection_date, \
               $last_disconnection_point, $last_disconnection_date, \
               $last_seen_point, $last_seen_date, \
               $last_miss_point, $last_miss_date)"
          | [ _, point_id_db, country_name, country_code ] -> (* Known peer, juste update info *)
            if point_id = "" then
              PGSQL(dbh)
                "UPDATE peers \
                 SET \
                 trusted = $trusted, \
                 score = $score, \
                 state = $state, \
                 total_sent = $total_sent, \
                 total_received = $total_recv, \
                 current_inflow = $current_inflow, \
                 current_outflow = $current_outflow, \
                 last_failed_connection_point = $last_failed_connection_point, \
                 last_failed_connection_date = $last_failed_connection_date, \
                 last_rejected_connection_point = $last_rejected_connection_point, \
                 last_rejected_connection_date = $last_rejected_connection_date, \
                 last_established_connection_point = $last_established_connection_point, \
                 last_established_connection_date = $last_established_connection_date, \
                 last_disconnection_point = $last_disconnection_point, \
                 last_disconnection_date = $last_disconnection_date, \
                 last_seen_point = $last_seen_point, \
                 last_seen_date = $last_seen_date, \
                 last_miss_point = $last_miss_point, \
                 last_miss_date = $last_miss_date WHERE peer_id = $peer_id"
            else
              let country_name, country_code =
                match point_id_db with
                | None -> country_name, country_code
                | Some point_id_db ->
                  if point_id_db = point_id then country_name, country_code
                  else get_country point_id in
              PGSQL(dbh)
                "UPDATE peers \
                 SET \
                 country_name = $country_name, \
                 country_code = $country_code, \
                 point_id = $point_id, \
                 trusted = $trusted, \
                 score = $score, \
                 state = $state, \
                 total_sent = $total_sent, \
                 total_received = $total_recv, \
                 current_inflow = $current_inflow, \
                 current_outflow = $current_outflow, \
                 last_failed_connection_point = $last_failed_connection_point, \
                 last_failed_connection_date = $last_failed_connection_date, \
                 last_rejected_connection_point = $last_rejected_connection_point, \
                 last_rejected_connection_date = $last_rejected_connection_date, \
                 last_established_connection_point = $last_established_connection_point, \
                 last_established_connection_date = $last_established_connection_date, \
                 last_disconnection_point = $last_disconnection_point, \
                 last_disconnection_date = $last_disconnection_date, \
                 last_seen_point = $last_seen_point, \
                 last_seen_date = $last_seen_date, \
                 last_miss_point = $last_miss_point, \
                 last_miss_date = $last_miss_date WHERE peer_id = $peer_id"
          | _ -> () )
        stats)

let block_hash level =
  debug "[Writer] block_hash/%i\n%!" level;
  let level = Int64.of_int level in
  match PGSQL(dbh)
          "SELECT hash FROM block WHERE level = $level \
           AND distance_level = 0" with
  | [ hash ] -> hash
  | _ -> assert false


let register_crawler_activity name delay =
  let timestamp = Unix.gettimeofday () in
  let delay = Int32.of_int delay in
  PGSQL(dbh) "INSERT INTO crawler_activity (name, timestamp, delay) \
              VALUES ($name, $timestamp, $delay) ON CONFLICT (name) \
              DO UPDATE SET timestamp = $timestamp, delay = $delay"

let update_alias ?(verbose=true) hash alias =
  let user_has_alias, old_alias =
    match PGSQL(dbh) "SELECT alias FROM user_alias WHERE dn = $hash" with
    | [ old_alias ] -> true, old_alias
    | _ -> false, "" in
  let alias_in_use_by =
    match alias with
    | None -> ""
    | Some alias ->
      let query_alias =
        PGSQL(dbh) "SELECT dn FROM user_alias WHERE alias = $alias" in
      match query_alias with
      | [ user ] -> user
      | _ -> "" in
  if alias_in_use_by <> "" then
    (if verbose then debug "[Writer] alias already in use by %s\n%!" alias_in_use_by)
  else
    match user_has_alias, alias with
    | false, Some alias when String.length alias > 0 ->
      PGSQL(dbh) "INSERT INTO user_alias (dn, alias) VALUES ($hash, $alias)";
      PGSQL(dbh) "UPDATE dune_user SET alias = $alias WHERE hash = $hash"
    | true, Some alias when String.length alias > 0 && alias <> old_alias ->
      PGSQL(dbh) "UPDATE user_alias SET alias = $alias WHERE dn = $hash";
      PGSQL(dbh) "UPDATE dune_user SET alias = $alias WHERE hash = $hash"
    | true, None | true, Some "" ->
      PGSQL(dbh) "DELETE FROM user_alias WHERE dn = $hash";
      PGSQL(dbh) "UPDATE dune_user SET alias = NULL WHERE hash = $hash"
    | _ -> ()

let reset_alias {Data_types.dn; alias} =
  match alias with
  | None -> (* remove alias (outdated) *)
    PGSQL(dbh) "DELETE FROM user_alias WHERE dn = $dn";
    PGSQL(dbh) "UPDATE dune_user SET alias = NULL WHERE hash = $dn"
  | Some alias ->
    match PGSQL(dbh) "SELECT dn FROM user_alias WHERE alias = $alias " with
    | [ hash ] when hash <> dn ->
      (* remove alias from old user and set it to new *)
      PGSQL(dbh) "DELETE FROM user_alias WHERE alias = $alias";
      PGSQL(dbh) "UPDATE dune_user SET alias = NULL WHERE alias = $alias";
      begin match PGSQL(dbh) "SELECT alias FROM user_alias WHERE dn = $dn" with
        | [] ->
          PGSQL(dbh) "INSERT INTO user_alias (dn, alias) VALUES ($dn, $alias)";
          PGSQL(dbh) "UPDATE dune_user SET alias = $alias WHERE hash = $dn"
        | _ ->
          PGSQL(dbh) "UPDATE user_alias SET alias = $alias WHERE dn = $dn";
          PGSQL(dbh) "UPDATE dune_user SET alias = $alias WHERE hash = $dn" end
    | [] ->
      (* add new alias for a user *)
      begin match PGSQL(dbh) "SELECT alias FROM user_alias WHERE dn = $dn" with
        | [] ->
          PGSQL(dbh) "INSERT INTO user_alias (dn, alias) VALUES ($dn, $alias)";
          PGSQL(dbh) "UPDATE dune_user SET alias = $alias WHERE hash = $dn"
        | _ ->
          PGSQL(dbh) "UPDATE user_alias SET alias = $alias WHERE dn = $dn";
          PGSQL(dbh) "UPDATE dune_user SET alias = $alias WHERE hash = $dn" end
    | _ -> (* user already exist with this alias *) ()


(* update highest info and counts if in alternative branch *)
let update_count_info_highest_alt () =
  let rec aux hash =
    match PGSQL(dbh) "SELECT distance_level, predecessor, level \
                      FROM block WHERE hash = $hash" with
    | [ -1L, pred, _ ] ->
      update_counts hash (-1L);
      aux pred
    | [ _, _, level ] ->
      PGSQL(dbh) "UPDATE count_info SET \
                  hash = $hash, level = $level WHERE info = 'highest'"
    | _ -> () in
  match PGSQL(dbh) "SELECT hash FROM count_info WHERE info = 'highest'" with
  | [ hash ] -> aux hash
  | _ -> ()

(* counts from a hash (head in None) down to a level *)
let reset_counts start_level end_hash =
  let rec aux hash level acc =
    Printf.printf "%Ld\n%!" level;
    if level >= start_level then (
      begin match PGSQL(dbh) "SELECT predecessor FROM block WHERE hash = $hash" with
        | [ pred ] -> aux pred (Int64.pred level) (hash :: acc)
        | _ -> assert false end )
    else (
      List.iter (fun hash -> update_counts hash 1L) acc;
      List.nth_opt acc 0)
  in
  match end_hash with
  | None ->
    begin match PGSQL(dbh) "SELECT hash, level FROM block WHERE distance_level = 0 \
                            ORDER BY level DESC LIMIT 1" with
    | [ hash, level ] ->
      let start_hash = aux hash level [] in
      begin match start_hash with
        | None -> ()
        | Some start_hash -> update_count_info start_hash start_level "lowest" end;
      update_count_info hash level "highest";
      List.iter (fun hash -> update_counts hash 0L)
        (PGSQL(dbh) "SELECT hash FROM block WHERE distance_level <> 0 \
                     AND level >= $start_level")
    | _ -> () end
  | Some end_hash ->
     begin match PGSQL(dbh) "SELECT level FROM block WHERE hash = $end_hash" with
    | [ level ] ->
      let start_hash = aux end_hash level [] in
      begin match start_hash with
        | None -> ()
        | Some start_hash -> update_count_info start_hash start_level "lowest" end;
      update_count_info end_hash level "highest";
      List.iter (fun hash -> update_counts hash 0L)
        (PGSQL(dbh) "SELECT hash FROM block WHERE distance_level <> 0 \
                     AND level >= $start_level AND level <= $level")
    | _ -> () end

(* extend count range if needed *)
let counts_downup start_level end_level =
  let start_level = Int64.of_int start_level in
  let end_level = Int64.of_int end_level in
  update_count_info_highest_alt ();
  let lowest_level, lowest_pred =
    match PGSQL(dbh) "SELECT ci.level, bl.predecessor FROM count_info AS ci \
                      INNER JOIN block AS bl ON bl.hash = ci.hash \
                      WHERE ci.info = 'lowest'" with
    | [ level, pred ] -> Some level, Some pred
    | _ ->
      PGSQL(dbh) "INSERT INTO count_info (hash, level, info) \
                  SELECT hash, $start_level, 'lowest' FROM block \
                  WHERE level = $start_level AND distance_level = 0";
      None, None in
  let highest_level =
    match PGSQL(dbh) "SELECT level FROM count_info WHERE info = 'highest'" with
    | [ level ] -> Some level
    | _ ->
      PGSQL(dbh) "INSERT INTO count_info (hash, level, info) \
                  SELECT hash, $end_level, 'highest' FROM block \
                  WHERE level = $end_level AND distance_level = 0";
      None in
  let get_end_hash end_level =
    if end_level = -1L then None
    else
      match PGSQL(dbh) "SELECT hash FROM block WHERE level = $end_level AND \
                        distance_level = 0" with
      | [ hash ] -> Some hash
      | _ -> None in
  let pairs =
    match lowest_level, lowest_pred, highest_level with
    | Some lowest_level, Some lowest_pred, Some highest_level ->
      let low_pair =
        if start_level < lowest_level then
          [start_level, Some lowest_pred]
        else [] in
      let high_pair =
        if end_level = -1L || end_level > highest_level then
          [Int64.succ highest_level, get_end_hash end_level]
        else [] in
      low_pair @ high_pair
    | _ ->  [start_level, get_end_hash end_level] in
  List.iter (fun (start_level, end_hash) -> reset_counts start_level end_hash) pairs

let register_service
    {Data_types.srv_kind; srv_dn1; srv_name; srv_url; srv_logo; srv_logo2; srv_logo_payout;
     srv_descr; srv_sponsored; srv_page; srv_delegations_page; srv_account_page;
     srv_aliases; srv_display_delegation_page; srv_display_account_page} =
  let dn1 = Misc.unopt "" srv_dn1 in
  let logos = [Some srv_logo; srv_logo2; srv_logo_payout] in
  let descr = Misc.unopt "" srv_descr in
  let page = Misc.unopt "" srv_page in
  match srv_sponsored, srv_aliases with
  | None, _ ->
    PGSQL(dbh)
      "INSERT INTO services (dn1, name, url, logos, description, kind, page, \
       delegations_page, account_page, display_delegation, display_account) \
       VALUES($dn1, $srv_name, $srv_url, $logos, $descr, $srv_kind, $page, \
       $srv_delegations_page, $srv_account_page, \
       $srv_display_delegation_page, $srv_display_account_page)"
  | Some tsp, aliases ->
    let end_tsp = Pg_helper.cal_of_string tsp in
    match aliases with
    | None ->
      PGSQL(dbh)
        "INSERT INTO services (dn1, name, url, logos, description, kind, sponsored, \
         page, delegations_page, account_page, display_delegation, display_account) \
         VALUES($dn1, $srv_name, $srv_url, $logos, $descr, $srv_kind, $end_tsp, \
         $page, $srv_delegations_page, $srv_account_page, \
         $srv_display_delegation_page, $srv_display_account_page)"
    | Some l ->
      let addresses = List.map (fun {Data_types.dn; _} -> Some dn) l in
      let aliases = List.map (fun {Data_types.alias; _} -> alias) l in
      PGSQL(dbh)
        "INSERT INTO services (dn1, name, url, logos, description, kind, sponsored, \
         page, delegations_page, account_page, addresses, aliases, display_delegation, \
         display_account) \
         VALUES($dn1, $srv_name, $srv_url, $logos, $descr, $srv_kind, $end_tsp, \
         $page, $srv_delegations_page, $srv_account_page, $addresses, $aliases, \
         $srv_display_delegation_page, $srv_display_account_page)"

let check_and_update_service service =
  let open Data_types in
  let name = service.srv_name in
  let kind = service.srv_kind in
  match PGSQL(dbh) "SELECT * FROM services WHERE name = $name AND kind = $kind" with
  | [] ->
    debug "[Writer] Registering new service %s\n%!" name;
    register_service service
  | [ srv ] ->
    let srv = Pg_helper.service_from_db srv in
    if srv <> service then (
      debug "[Writer] Updating service %s\n%!" name;
      PGSQL(dbh) "DELETE FROM services WHERE name = $name";
      register_service service)
  | _ ->
    debug "[Writer] Several services with same name: %s\n%!" name

let register_coingecko_exchange
    {Data_types.gk_last; gk_target; gk_tsp; gk_volume; gk_base; gk_market; gk_converted_last; _ } =
  let price_usd = gk_converted_last.Data_types.gk_usd in
  match PGSQL(dbh)
          "SELECT timestamp FROM coingecko_exchange WHERE \
           name = $gk_market AND base = $gk_base AND target = $gk_target" with
  | [ tsp ] -> if tsp <> gk_tsp then
      PGSQL(dbh)
        "UPDATE coingecko_exchange SET \
         timestamp = $gk_tsp, volume = $gk_volume, conversion = $gk_last, \
         price_usd = $price_usd \
         WHERE name = $gk_market AND base = $gk_base AND target = $gk_target"
  | _ ->
    PGSQL(dbh)
      "INSERT INTO coingecko_exchange (name, base, target, timestamp, volume, \
       conversion, price_usd) \
       VALUES($gk_market, $gk_base, $gk_target, $gk_tsp, $gk_volume, $gk_last, \
       $price_usd)"

let register_marketcap { Data_types.mc_id; name; symbol; rank; price_usd; price_btc;
                         volume_usd_24; market_cap_usd; available_supply;
                         total_supply; max_supply; percent_change_1;
                         percent_change_24; percent_change_7; last_updated } =
  let unopt = function None -> "" | Some value -> value in
  let volume_usd_24 = unopt volume_usd_24 in
  let market_cap_usd = unopt market_cap_usd in
  let available_supply = unopt available_supply in
  let total_supply = unopt total_supply in
  let max_supply = unopt max_supply in
  let percent_change_1 = unopt percent_change_1 in
  let percent_change_24 = unopt percent_change_24 in
  let percent_change_7 = unopt percent_change_7 in
  let last_updated = Int64.of_string last_updated in
  PGSQL(dbh) "INSERT into marketcap\
              (currency_id, name, symbol, rank, price_usd, price_btc, \
              volume_usd_24, market_cap_usd, available_supply, \
              total_supply, max_supply, percent_change_1, \
              percent_change_24, percent_change_7, last_updated) \
              VALUES\
              ($mc_id, $name, $symbol, $rank, $price_usd, $price_btc, \
              $volume_usd_24, $market_cap_usd, $available_supply, \
              $total_supply, $max_supply, $percent_change_1, \
              $percent_change_24, $percent_change_7, $last_updated) ON CONFLICT DO NOTHING"

let last_voting_period () =
  match PGSQL(dbh) "SELECT MAX(voting_period) from quorum" with
  | [ Some voting_period ] -> Int32.to_int voting_period
  | _ -> -1

let register_voting_rolls voting_period rolls =
  let voting_period = Int32.of_int voting_period in
  List.iter (fun {vroll_pkh; vroll_count} ->
      let vroll_count = Int32.of_int vroll_count in
      PGSQL(dbh)
        "INSERT INTO snapshot_voting_rolls (voting_period, delegate, rolls) \
         VALUES($voting_period, $vroll_pkh, $vroll_count) \
         ON CONFLICT DO NOTHING"
    ) rolls;
  PGSQL(dbh) "UPDATE snapshot_voting_rolls SET ready = true \
              WHERE voting_period = $voting_period"

let register_quorum voting_period quorum =
  let voting_period32 = Int32.of_int voting_period in
  let voting_period64 = Int64.of_int voting_period in
  PGSQL(dbh)
    "INSERT INTO quorum (voting_period, value, voting_period_kind) \
     (SELECT $voting_period32, $quorum, voting_period_kind FROM block WHERE \
     voting_period = $voting_period64 LIMIT 1) \
     ON CONFLICT DO NOTHING"

let constants ?limit () =
  let constants = match limit with
    | None ->
      PGSQL(dbh) "SELECT * FROM protocol_constants WHERE distance_level = 0 \
                  ORDER BY level_start"
    | Some limit ->
      let limit = Int64.of_int limit in
      PGSQL(dbh) "SELECT * FROM protocol_constants WHERE distance_level = 0 \
                  ORDER BY level_start DESC LIMIT $limit" in
  List.map Pg_helper.constants_from_db constants

let has_level_rights level =
  let level = Int64.of_int level in
  PGSQL(dbh) "SELECT level FROM level_rights WHERE level = $level AND ready" <> []



(* pending  operations *)

let pending_seed_nonce_revelation ?errors hash branch status tsp snr =
  let level = Int64.of_int snr.node_seed_level in
  let nonce = snr.node_seed_nonce in
  PGSQL(dbh)
    "INSERT INTO seed_nonce_revelation_pending \
     (hash, branch, status, timestamp_op, errors, level, nonce) \
     VALUES($hash, $branch, $status, $tsp, $?errors, $level, $nonce)"

let pending_activation ?errors hash branch status tsp act =
  let pkh = act.node_act_pkh in
  let secret = act.node_act_secret in
  register_dune_user pkh;
  PGSQL(dbh)
    "INSERT INTO activation_pending \
     (hash, branch, status, timestamp_op, errors, pkh, secret) \
     VALUES($hash, $branch, $status, $tsp, $?errors, $pkh, $secret)"

let pending_endorsement ?errors hash branch status tsp endo =
  let level = Int64.of_int endo.node_endorse_block_level in
  let priority =
    match PGSQL(dbh) "SELECT priority FROM block WHERE hash = $branch" with
    | [ p ] -> Some p
    | _ -> None in
  PGSQL(dbh)
    "INSERT INTO endorsement_pending \
     (hash, branch, status, timestamp_op, errors, block_level, priority) \
     VALUES($hash, $branch, $status, $tsp, $?errors, $level, $?priority)"

let pending_delegation ?errors hash branch status tsp dlg =
  let source = dlg.node_del_src in
  let counter = Z.to_int64 dlg.node_del_counter in
  let fee = dlg.node_del_fee in
  let gas_limit = Z.to_int64 dlg.node_del_gas_limit in
  let storage_limit = Z.to_int64 dlg.node_del_storage_limit in
  register_dune_user source;
  let delegate = dlg.node_del_delegate in
  register_dune_user delegate;
  PGSQL(dbh)
    "INSERT INTO delegation_pending \
     (hash, branch, status, timestamp_op, errors, source, fee, counter, \
     delegate, gas_limit, storage_limit) \
     VALUES($hash, $branch, $status, $tsp, $?errors, $source, $fee, $counter, \
     $delegate, $gas_limit, $storage_limit)"

let pending_reveal ?errors hash branch status tsp rvl =
  let source = rvl.node_rvl_src in
  let counter = Z.to_int64 rvl.node_rvl_counter in
  let fee = rvl.node_rvl_fee in
  let gas_limit = Z.to_int64 rvl.node_rvl_gas_limit in
  let storage_limit = Z.to_int64 rvl.node_rvl_storage_limit in
  register_dune_user source;
  let pubkey = rvl.node_rvl_pubkey in
  register_dune_user pubkey;
  PGSQL(dbh)
    "INSERT INTO reveal_pending \
     (hash, branch, status, timestamp_op, errors, source, fee, counter, pubkey, \
     gas_limit, storage_limit) \
     VALUES($hash, $branch, $status, $tsp, $?errors, $source, $fee, $counter, $pubkey, \
     $gas_limit, $storage_limit)"

let pending_transaction ?errors hash branch status tsp tr =
  let source = tr.node_tr_src in
  let counter = Z.to_int64 tr.node_tr_counter in
  let fee = tr.node_tr_fee in
  let gas_limit = Z.to_int64 tr.node_tr_gas_limit in
  let storage_limit = Z.to_int64 tr.node_tr_storage_limit in
  register_dune_user source;
  let dst = tr.node_tr_dst in
  register_dune_user dst;
  let amount = tr.node_tr_amount in
  let parameters = tr.node_tr_parameters in
  let collect_fee_gas = Misc.convopt Z.to_int64 tr.node_tr_collect_fee_gas in
  let collect_pk = tr.node_tr_collect_pk in
  PGSQL(dbh)
    "INSERT INTO transaction_pending \
     (hash, branch, status, timestamp_op, errors, source, destination, fee, \
     counter, amount, parameters, gas_limit, storage_limit, \
     collect_fee_gas, collect_pk) \
     VALUES \
     ($hash, $branch, $status, $tsp, $?errors, $source, $dst, $fee, $counter, \
     $amount, $?parameters, $gas_limit, $storage_limit, \
     $?collect_fee_gas, $?collect_pk)"

let pending_origination ?errors hash branch status tsp ori =
  let source = ori.node_or_src in
  let counter = Z.to_int64 ori.node_or_counter in
  let fee = ori.node_or_fee in
  let gas_limit = Z.to_int64 ori.node_or_gas_limit in
  let storage_limit = Z.to_int64 ori.node_or_storage_limit in
  register_dune_user source;
  let manager = ori.node_or_manager in
  register_dune_user manager;
  let delegate = match ori.node_or_delegate with
    | None -> "" | Some delegate -> delegate in
  register_dune_user delegate ;
  let balance = ori.node_or_balance in
  let spendable = match ori.node_or_spendable with
    | Some spendable -> spendable
    | None -> match ori.node_or_script with
      | Some _ -> false
      | _ -> true in
  let sc_code, sc_storage, sc_code_hash = match ori.node_or_script with
    | None -> None, None, None
    | Some sc -> sc.sc_code, Some sc.sc_storage, sc.sc_code_hash in
  let kt1 = Crypto.op_to_KT1 hash in
  let delegatable = match ori.node_or_delegatable with
    | Some delegatable -> delegatable
    | None -> true in
  register_dune_user kt1;
  update_originated_contract ~origination:hash kt1
    ori.node_or_delegate manager spendable delegatable;
  PGSQL(dbh)
    "INSERT INTO origination_pending \
     (hash, branch, status, timestamp_op, errors, source, kt1, fee, counter, \
     manager, delegate, script_code, script_storage_type, script_code_hash, \
     spendable, delegatable, balance, gas_limit, storage_limit) \
     VALUES($hash, $branch, $status, $tsp, $?errors, $source, $kt1, $fee, $counter, \
     $manager, $delegate, $?sc_code, $?sc_storage, $?sc_code_hash, $spendable, \
     $delegatable, $balance, $gas_limit, $storage_limit)"

let get_pending_timestamp hash branch tsp op =
  let aux = function [ tsp ] -> tsp | _ -> tsp in
  match op with
  | NSeed_nonce_revelation _ ->
    aux (PGSQL(dbh) "SELECT timestamp_op FROM seed_nonce_revelation_pending \
                     WHERE hash = $hash AND branch = $branch")
  | NActivation _ ->
    aux (PGSQL(dbh) "SELECT timestamp_op FROM activation_pending \
                     WHERE hash = $hash AND branch = $branch")
  | NEndorsement _ ->
    aux (PGSQL(dbh) "SELECT timestamp_op FROM endorsement_pending \
                     WHERE hash = $hash AND branch = $branch")
  | NDelegation _ ->
    aux (PGSQL(dbh) "SELECT timestamp_op FROM delegation_pending \
                     WHERE hash = $hash AND branch = $branch")
  | NReveal _ ->
    aux (PGSQL(dbh) "SELECT timestamp_op FROM reveal_pending \
                     WHERE hash = $hash AND branch = $branch")
  | NTransaction _ ->
    aux (PGSQL(dbh) "SELECT timestamp_op FROM transaction_pending \
                     WHERE hash = $hash AND branch = $branch")
  | NOrigination _ ->
    aux (PGSQL(dbh) "SELECT timestamp_op FROM origination_pending \
                     WHERE hash = $hash AND branch = $branch")
  | _ -> tsp

let clear_pending () =
  PGSQL(dbh) "DELETE FROM seed_nonce_revelation_pending";
  PGSQL(dbh) "DELETE FROM activation_pending";
  PGSQL(dbh) "DELETE FROM endorsement_pending";
  PGSQL(dbh) "DELETE FROM delegation_pending";
  PGSQL(dbh) "DELETE FROM reveal_pending";
  PGSQL(dbh) "DELETE FROM transaction_pending";
  PGSQL(dbh) "DELETE FROM origination_pending"


let register_pending tsp mempool =
  let get_tsp l =
    List.map (fun {pending_hash; pending_branch; pending_contents; pending_errors; _} ->
        match pending_contents with
        | [] -> pending_hash, pending_branch, [], pending_errors, tsp
        | op :: _ ->
          pending_hash, pending_branch, pending_contents, pending_errors,
          get_pending_timestamp pending_hash pending_branch tsp op) l in
  let applied = get_tsp mempool.applied in
  let refused = get_tsp mempool.refused in
  let branch_refused = get_tsp mempool.branch_refused in
  let branch_delayed = get_tsp mempool.branch_delayed in
  let unprocessed = get_tsp mempool.unprocessed in
  clear_pending ();
  let register status l = List.iter
      (fun (hash, branch, ops, errors, tsp) ->
         let errors = Misc.convopt format_errors errors in
         List.iter (function
             | NSeed_nonce_revelation snr ->
               pending_seed_nonce_revelation ?errors hash branch status tsp snr
             | NActivation act ->
               pending_activation ?errors hash branch status tsp act
             | NEndorsement endo ->
               pending_endorsement ?errors hash branch status tsp endo
             | NDelegation dlg ->
               pending_delegation ?errors hash branch status tsp dlg
             | NReveal rvl ->
               pending_reveal ?errors hash branch status tsp rvl
             | NTransaction tr ->
               pending_transaction ?errors hash branch status tsp tr
             | NOrigination ori ->
               pending_origination ?errors hash branch status tsp ori
             | _ -> ())
           ops)
      l in
  register "applied" applied;
  register "refused" refused;
  register "branch_refused" branch_refused;
  register "branch_delayed" branch_delayed;
  register "unprocessed" unprocessed
